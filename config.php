<?php
// HTTP
define('HTTP_SERVER', 'http://sealsdirect.edirectdev.co.uk/');

// HTTPS
define('HTTPS_SERVER', 'http://sealsdirect.edirectdev.co.uk/');

// DIR
define('DIR_APPLICATION', '/home/sealsdirect/public_html/catalog/');
define('DIR_SYSTEM', '/home/sealsdirect/public_html/system/');
define('DIR_IMAGE', '/home/sealsdirect/public_html/image/');
define('DIR_STORAGE', '/home/sealsdirect/public_html/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'sealsdirect_sealsd');
define('DB_PASSWORD', 'satellite4164S$!');
define('DB_DATABASE', 'sealsdirect_sealsd');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// HEADERS
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");


//error_reporting(E_ALL);
//ini_set('display_errors',2);