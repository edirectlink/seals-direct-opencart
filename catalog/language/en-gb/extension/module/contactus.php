<?php
// Heading
$_['heading_title']             = 'Contact Us';

// Root
$_['root']                       = $_SERVER['DOCUMENT_ROOT'];

// Label
$_['label_reason']              = 'Reason for Contacting Us';

// Entry
$_['entry_address']             = 'Address (Optional)';
$_['entry_telephone']           = 'Telephone (Optional)';
$_['entry_fax']                 = 'Fax (Optional)';
$_['entry_file']                = 'Attach a file?';
$_['entry_name']                = 'Your Name';
$_['entry_company']                = 'Company Name';
$_['entry_email']               = 'E-Mail address';
$_['entry_enquiry']             = 'Your Message';

// Button
$_['button_map']                = 'View Google Map';
$_['button_submit']             = 'Submit';
$_['button_continue']           = 'Continue';

// Error
$_['errors']                    = '<b>ERROR</b> Please correct errors and try again.!';
$_['error_reason']              = 'Please select one of the Reasons using the dropdown field!';
$_['error_invalid']             = 'File appears to be invalid!';
$_['error_invalid1']            = 'File 1 appears to be invalid!';
$_['error_invalid2']            = 'File 2 appears to be invalid!';
$_['error_invalid3']            = 'File 3 appears to be invalid!';
$_['error_size']                = 'File too large!';
$_['error_size1']               = 'File 1 - too large!';
$_['error_size2']               = 'File 2 - too large!';
$_['error_size3']               = 'File 3 - too large!';
$_['error_enquiry']             = 'You haven\'t entered a Message text! (You\'ll need to re-enter the fields above)';

// Success
$_['text_success']              = '<p>Your message has been successfully sent to the store owner!</p>';
$_['text_success_email']        = '<p><b>We\'ve sent an email to you to acknowledge receipt</b>. If you don\'t receive it shortly it\'s possible<br />that you submitted an invalid email address - please try again.</p>';

// Email
$_['subject']                   = '- New Message Received';
$_['text_greeting']             = 'Dear Admin,';
$_['text_heading']              = 'New Message Posted to ';
$_['text_details']              = 'Details:';
$_['text_title']                = 'Admin,';
$_['text_table_head']           = '<table style="width: 80%; border: 1px solid #333333; margin-bottom: 20px;">
<thead><tr><td colspan="2" style="font-size: 12px; background-color: #EFEFEF; font-weight: bold; text-align: left; padding: 7px; color: #222222;">';
$_['text_table_endhead']        = '</td></tr></thead><tbody>';
$_['text_table_foot']           = '</tbody></table>';
$_['text_table_td1']            = '<td width="30%" style="font-size: 12px; background-color: #EFEFEF; text-align: left; padding: 7px; color: #222222;">';
$_['text_table_td2']            = '<td width="70%" style="font-size: 12px; background-color: #FFFFFF; text-align: left; padding: 7px; color: #222222;">';
$_['text_table_tr']             = '<tr>';
$_['text_table_endtd']          = '</td>';
$_['text_table_endtr']          = '</tr>';
$_['text_upload']               = 'The User has uploaded files:<br /><br />';
$_['text_view_uploads']         = 'You can view and manage the upload(s) by visiting the \'Contact Us\' control panel within your Admin area.<br /><br />';
$_['text_browse']               = 'If you want to include an attachment with your message, click on \'browse...\', locate your file within the window and click on \'open\'. <b>Warning:</b> If you file is too large you may experience errors.';
$_['customer_subject']          = ' - Your Message';
$_['text_customer']             = 'Dear ';
$_['text_response']             = '<br />Thank you for your message. We\'re happy to answer any questions you may have and aim to respond ';
$_['text_regards']              = 'Kind regards,';

// Responses - remove '//' for live version, comment out all others
//$_['response']                  = 'as soon as possible.';
//$_['response']                  = 'today.';
//$_['response']                  = 'tomorrow.';
$_['response']                  = 'within 2 to 3 days.';
//$_['response']                  = 'within 3 to 4 days.';
//$_['response']                  = 'within 4 to 5 days.';
//$_['response']                  = 'within 5 to 6 days.';
//$_['response']                  = 'within a week.';
//$_['response']                  = 'within 2 weeks.';
//$_['response']                  = 'within 3 weeks.';
//$_['response']                  = 'within a month.';
