<?php
/**
 * UK Postcode Lookup by Crafty Clicks
 *
 * @author		Crafty Clicks Limited
 * @link		https://craftyclicks.co.uk
 * @copyright	Copyright (c) 2016, Crafty Clicks Limited
 * @license		Licensed under the terms of the MIT license.
 * @version		3.0.0
**/
