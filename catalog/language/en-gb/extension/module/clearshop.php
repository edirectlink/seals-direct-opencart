<?php
$_['text_welcome']                 = 'Welcome';
$_['text_onsale']                  = 'Sale';

$_['text_buy_now']                 = 'Buy Now';

$_['text_prev']                    = 'previous';
$_['text_next']                    = 'next';

$_['text_in_stock']                = 'In Stock';
$_['text_out_stock']               = 'Out of Stock';
$_['text_zoom']                    = 'Zoom';
$_['text_quickview']               = 'Quick View';
$_['text_more_details']            = 'View details';

$_['text_menu_categories']         = 'Categories';
$_['text_menu_brands']             = 'Brands';
$_['text_menu_information']        = 'Information';

$_['text_email_friend']            = 'Email a friend';
$_['text_get_link']                = 'Get link';

$_['text_browse']         	        = 'Browse';
$_['text_or']         	            = 'or';

$_['text_taptozoom']         	     = 'Tap to zoom';
$_['text_closezoom']         	     = 'Close &times;';

$_['text_subcategories']         	 = 'Subcategories';

$_['text_years']                   = "Years";
$_['text_months']                  = "Months";
$_['text_weeks']                   = "Weeks";
$_['text_days']                    = "Days";
$_['text_hours']                   = "Hours";
$_['text_minutes']                 = "Min";
$_['text_seconds']                 = "Secs";

?>