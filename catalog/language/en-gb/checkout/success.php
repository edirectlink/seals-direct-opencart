<?php
// Heading
$_['heading_title']        = 'Your order has been placed!';

// Text
$_['text_basket']          = 'Shopping Cart';
$_['text_checkout']        = 'Checkout';
$_['text_success']         = 'Success';
$_['text_customer']        = '<p>Your order has been successfully processed!</p><p>You can view your orders by going to your <a href="/index.php?route=account/order">order history</a> page</p><p>Thanks for shopping with us!</p>';
$_['text_guest']           = '<p>Your order has been successfully processed!</p><p>Thanks for shopping with us!</p>';