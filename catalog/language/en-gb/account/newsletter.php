<?php
// Heading
$_['heading_title']    = 'Newsletter Subscription';
$_['heading_title']    = 'Newsletter Subscription';

// Text
$_['text_account']     = 'Newsletter Subscription';
$_['text_newsletter']      = 'Other';
$_['text_success']     = 'Success: Your newsletter subscription has been successfully updated!';

// Entry
$_['entry_newsletter'] = 'Subscribe';