<?php
// Text
$_['heading_title']        = 'Your order';
$_['text_title']           = 'Pay by Invoice / Purchase Order';
$_['text_purchase_order']  = 'Purchase Order Details';
$_['entry_purchase_order'] = 'Purchase order number:';
$_['purchase_order_required'] = 'Purchase order number is required';