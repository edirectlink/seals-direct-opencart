<?php
class ControllerInformationInformation extends Controller {
	public function index() {
		
		
		$this->load->language('information/information');
		
		$this->load->model('catalog/information');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$information_info = $this->model_catalog_information->getInformation($information_id);
		
		

		if ($information_info) {
			$this->document->setTitle($information_info['meta_title']);
			$this->document->setDescription($information_info['meta_description']);
			$this->document->setKeywords($information_info['meta_keyword']);

			$data['breadcrumbs'][] = array(
				'text' => $information_info['title'],
				'href' => $this->url->link('information/information', 'information_id=' .  $information_id)
			);

			$data['heading_title'] = $information_info['title'];
			$data['meta_id'] = $information_info['information_id'];
			
			$data['logged'] = $this->customer->isLogged();

			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			
			$prods = $this->model_catalog_product->getProducts();
			$data['prods']  = $prods;
			
			$cats = $this->model_catalog_category->getAllCategories();
			$data['cats']  = $cats;
			
			
			$top_categories = $this->db->query("SELECT `oc_category`.`category_id`,`oc_category_description`.`name` FROM `oc_category` LEFT JOIN `oc_category_description` on `oc_category`.`category_id` = `oc_category_description`.`category_id` WHERE `oc_category`.`parent_id` = 71 ORDER by `oc_category`.`sort_order`")->rows;
			
			$data['top_categories'][0]['id'] = 0;
			$data['top_categories'][0]['name'] = "Seals Direct Catalogue";
			$data['top_categories'][0]['samples'] = $this->db->query("SELECT * FROM `oc_product_option_value` LEFT JOIN `oc_product` on `oc_product`.`product_id` = `oc_product_option_value`.`product_id` WHERE `points_prefix` = 'Catalogue'")->rows;
			$i = 0;
			foreach ($data['top_categories'][0]['samples'] as $s){
				$data['top_categories'][0]['samples'][$i]['image50'] = $this->model_tool_image->resize($s['image'], 50, 30);
				$i++;
			}
			
			foreach ($top_categories as $cat){
				$data['top_categories'][$cat['category_id']]['id'] = $cat['category_id'];
				$data['top_categories'][$cat['category_id']]['name'] = $cat['name'];
				$data['top_categories'][$cat['category_id']]['samples'] = $this->db->query("SELECT * FROM `oc_product_option_value` LEFT JOIN `oc_product` on `oc_product`.`product_id` = `oc_product_option_value`.`product_id` WHERE (`points_prefix` = 'SAMPLE' OR `points_prefix` = 'Catalogue') AND `oc_product`.`product_id` in (SELECT `product_id` FROM `oc_product_to_category` WHERE `category_id` = ".$cat['category_id'].") ORDER BY points_prefix ASC, sort_order ASC")->rows;
				
				$i = 0;
				foreach ($data['top_categories'][$cat['category_id']]['samples'] as $s){
					$data['top_categories'][$cat['category_id']]['samples'][$i]['image50'] = $this->model_tool_image->resize($s['image'], 50, 30);
					$i++;
				}
			}
			
			
			
			//var_dump($cats);
			
			
			$this->response->setOutput($this->load->view('information/information', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');
			

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['information_id'])) {
			$information_id = (int)$this->request->get['information_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}