<?php
class ControllerExtensionModuleFeaturedCategories extends Controller {
	public function index($setting) {

				$this->load->language('extension/module/clearshop');
		
				$data['direction'] = $this->language->get('direction');
				$data['clearshop_featured_carousel'] = $this->config->get('clearshop_featured_carousel');
				$data['clearshop_latest_carousel'] = $this->config->get('clearshop_latest_carousel');
				$data['clearshop_special_carousel'] = $this->config->get('clearshop_special_carousel');
				$data['clearshop_bestseller_carousel'] = $this->config->get('clearshop_bestseller_carousel');
				$data['clearshop_carousel_autoplay'] = $this->config->get('clearshop_carousel_autoplay');
				$data['clearshop_rollover_images'] = $this->config->get('clearshop_rollover_images');
			
		static $module = 0;
		$this->load->language('extension/module/featured_categories');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$this->document->addStyle('catalog/view/javascript/jquery/owl-carousel/owl.carousel.css');
		$this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/category');

		$this->load->model('tool/image');

		$data['categories'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}
     
		if (!empty($setting['category'])) {
			$categories = array_slice($setting['category'], 0, (int)$setting['limit']);
		    	
			foreach ($categories as $category_id) {
				$category_info = $this->model_catalog_category->getCategory($category_id);

				if ($category_info) {
					if ($category_info['image']) {
						$image = $this->model_tool_image->resize($category_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}
                    
				
					$data['categories'][] = array(
						'category_id'  => $category_info['category_id'],
						'thumb'       => $image,
						'name'        => $category_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8')), 0, $setting['description_length']),
						'display_description_status'=>$setting['description_status'],
						'href'        => $this->url->link('product/category', 'path='. $category_info['category_id'])
					);
					
				}
			}
		}
$data['module'] = $module++;
		if ($data['categories']) {
			return $this->load->view('extension/module/featured_categories', $data);
		}
	}
}