<?php
/**
 * UK Postcode Lookup by Crafty Clicks
 *
 * @author      Crafty Clicks Limited
 * @link		https://craftyclicks.co.uk
 * @copyright	Copyright (c) 2016, Crafty Clicks Limited
 * @license     Licensed under the terms of the MIT license.
 * @version		3.0.0
**/

class ControllerExtensionModuleCraftyclicks extends Controller {
	public function index() {
		$isSSL = $this->config->get('config_secure'); //Get the value whether website uses SSL
		if(isset($isSSL)){
			$data['absolutePath'] = $this->config->get('config_ssl');
		}
		else {
			$data['absolutePath'] = $this->config->get('config_url');
		}
		$this->load->language('extension/module/craftyclicks');
		$configValues= array(
			'access_token',
			'status'
		);
		$data['craftyclicksConfig']= array();
		foreach ($configValues as $elem) {
			$data['craftyclicksConfig'][$elem] = html_entity_decode($this->config->get('module_craftyclicks_'.$elem));
		}
		$data['craftyclicks_access_token'] = html_entity_decode($this->config->get('craftyclicks_access_token'));
		$data['craftyclicks_status'] = html_entity_decode($this->config->get('craftyclicks_status'));

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/extension/module/craftyclicks')) {
			return $this->load->view($this->config->get('config_template') . '/template/extension/module/craftyclicks', $data);
		} else {
			return $this->load->view('extension/module/craftyclicks', $data);
		}
	}
}
