<?php
class ControllerExtensionModuleContactUs extends Controller {
	private $error = array();

	public function index() {
	    $this->load->language('english/english');
	    $this->load->language('information/contact');
		$this->load->language('extension/module/contactus');
		$this->load->model('extension/module/contactus');


		$this->document->setTitle($this->language->get('heading_title'));
		
		$results = $this->model_extension_module_contactus->getParams();
		foreach ($results as $result) {
		    $data['number']              = $result['number'];
		    $data['upload']              = $result['upload'];
			$folder                      = $result['folder'];
			$folderclean                 = $result['folder'];
			$number                      = $result['number'];
			$adminemail                  = $result['adminemail'];
			$acknemail                   = $result['acknemail'];
			$copyemail                   = $result['copyemail'];
		}

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
          if (!isset($this->error['size'])) {
		     
			 $root = $this->language->get('root');
			 $folder = $root.$folder;

			 $upload = '';
			 if (isset($this->request->post['1']) && $_FILES['1']['name']) {
			    if (!is_dir($folder)) {
                    if (!mkdir($folder, 0755, true)) {
	                    die('Failed to create folder...');
	                }
                }
                  $random = rand('1000000000', '9999999999');
                  while(file_exists($folder.'/'.$random. '-'.$_FILES['1']['name']))
                  {
                    $random = rand('1000000000', '9999999999');
                  }
                  $filepath = $folder.'/'.$random. '-'.$_FILES['1']['name'];
				  $filename = $folderclean.'/'.$random. '-'.$_FILES['1']['name'];
				  $moved = move_uploaded_file($_FILES['1']['tmp_name'], $filepath);
                  if( $moved ) {
					$upload .= $filename."<br />";
                  } else {
                    $upload .= $filename . " - Problem uploading this file!<br />";
                  }
             }
			 if (isset($this->request->post['2']) && $_FILES['2']['name']) {
                  $random = rand('1000000000', '9999999999');
                  while(file_exists($folder.'/'.$random. '-'.$_FILES['2']['name']))
                  {
                    $random = rand('1000000000', '9999999999');
                  }
                  $filepath = $folder.'/'.$random. '-'.$_FILES['2']['name'];
				  $filename = $folderclean.'/'.$random. '-'.$_FILES['2']['name'];
				  $moved = move_uploaded_file($_FILES['2']['tmp_name'], $filepath);
	              if( $moved ) {
					$upload .= $filename."<br />";
                  } else {
                    $upload .= $filename . " - Problem uploading this file!<br />";
                  }
             }
			 if (isset($this->request->post['3']) && $_FILES['3']['name']) {
                  $random = rand('1000000000', '9999999999');
                  while(file_exists($folder.'/'.$random. '-'.$_FILES['3']['name']))
                  {
                    $random = rand('1000000000', '9999999999');
                  }
                  $filepath = $folder.'/'.$random. '-'.$_FILES['3']['name'];
				  $filename = $folderclean.'/'.$random. '-'.$_FILES['3']['name'];
				  $moved = move_uploaded_file($_FILES['3']['tmp_name'], $filepath);
	              if( $moved ) {
					$upload .= $filename."<br />";
                  } else {
                    $upload .= $filename . " - Problem uploading this file!<br />";
                  }
             }
			 
			 if (isset($this->request->post['4']) && $_FILES['4']['name']) {
                  $random = rand('1000000000', '9999999999');
                  while(file_exists($folder.'/'.$random. '-'.$_FILES['4']['name']))
                  {
                    $random = rand('1000000000', '9999999999');
                  }
                  $filepath = $folder.'/'.$random. '-'.$_FILES['4']['name'];
				  $filename = $folderclean.'/'.$random. '-'.$_FILES['4']['name'];
				  $moved = move_uploaded_file($_FILES['4']['tmp_name'], $filepath);
	              if( $moved ) {
					$upload .= $filename."<br />";
                  } else {
                    $upload .= $filename . " - Problem uploading this file!<br />";
                  }
             }
			 
			 			
			 // Admin Email

			 $this->load->language('extension/module/contactusemail.php');
			
			 $text_greeting          = $this->language->get('text_greeting');
			 $text_heading           = $this->language->get('text_heading');
			 $text_details           = $this->language->get('text_details');
			 $text_title             = $this->language->get('text_title');
			 $text_table_head        = $this->language->get('text_table_head');
			 $text_table_endhead     = $this->language->get('text_table_endhead');
			 $text_table_tr          = $this->language->get('text_table_tr');
			 $text_table_td1         = $this->language->get('text_table_td1');
			 $text_table_td2         = $this->language->get('text_table_td2');
			 $text_table_endtr       = $this->language->get('text_table_endtr');
			 $text_table_endtd       = $this->language->get('text_table_endtd');
			 $text_table_foot        = $this->language->get('text_table_foot');
			 $text_upload            = $this->language->get('text_upload');
			 $text_view_uploads      = $this->language->get('text_view_uploads');
			
			 $store_name             = $this->config->get('config_name');
             $store_url              = $this->config->get('config_url');
			 $logo                   = $store_url . 'image/' . $this->config->get('config_logo');

			 if (!$adminemail) {
		        $adminemail = $this->config->get('config_email');
		     }
		
			 $to                     = "$adminemail";
			 $subject                = $this->language->get('subject');
             $subject                = $store_name . $subject;

// Format the Admin Email			
$message = "<html><head><title>$subject</title></head>";
$message .= "<body style='font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;'><br />";
$message .= "<img src='$logo'><br /><br />";
$message .= "<br />". $text_greeting  ."<br /><br /><u><b>" . $text_heading . $store_name . "</b></u><br /><br />"
. $text_details . "<br /><br />" . $text_table_head . $this->request->post['contact_reason'] . $text_table_endhead .
$text_table_tr . $text_table_td1 . "
From:" 
. $text_table_endtd . $text_table_td2 . 
$this->request->post['name']
. $text_table_endtd . $text_table_endtr .

$text_table_tr . $text_table_td1 . "
Email:"
. $text_table_endtd . $text_table_td2 . 
$this->request->post['email']
. $text_table_endtd . $text_table_endtr
. $text_table_tr . $text_table_td1 . "
Telephone:"
. $text_table_endtd . $text_table_td2 . 
$this->request->post['telephone']
. $text_table_endtd . $text_table_endtr
. $text_table_tr . $text_table_td1 . "
Address:"
. $text_table_endtd . $text_table_td2 . 
nl2br(htmlspecialchars($this->request->post['address']))
. $text_table_endtd . $text_table_endtr
. $text_table_tr . $text_table_td1 . "
Fax:"
. $text_table_endtd . $text_table_td2 . 
$this->request->post['fax']
. $text_table_endtd . $text_table_endtr
. $text_table_tr . $text_table_td1 . "
IP Address:"
. $text_table_endtd . $text_table_td2 . 
$this->request->server['REMOTE_ADDR']
. $text_table_endtd . $text_table_endtr
. $text_table_tr . $text_table_td1 . "
Enquiry:"
. $text_table_endtd . $text_table_td2 . 
nl2br(htmlspecialchars($this->request->post['enquiry']))
. $text_table_endtd . $text_table_endtr
;
if ($upload) {
$message .=
$text_table_tr . $text_table_td1 . "
Uploaded File(s):"
. $text_table_endtd . $text_table_td2 . 
$text_upload . $upload . "<br />" . $text_view_uploads
. $text_table_endtd . $text_table_endtr;
}
$message .= $text_table_foot . $text_title . "<br />" . $store_url . "<br /><br /><br /><br /><br /><br /><br />";
$message .= "</body></html>";

             $mail = new Mail();
			 $mail->protocol = $this->config->get('config_mail_protocol');
			 $mail->parameter = $this->config->get('config_mail_parameter');
			 $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			 $mail->smtp_username = $this->config->get('config_mail_smtp_username');
			 $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			 $mail->smtp_port = $this->config->get('config_mail_smtp_port');
			 $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			 $mail->setTo($adminemail);
			 $mail->setFrom($this->request->post['email']);
			 $mail->setSender(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'));
			 $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			 $mail->setText($this->request->post['contact_reason']."\n\n".$this->request->post['enquiry'].$upload);
			 $mail->setHTML($message);
			 $mail->send();

			 if ($acknemail) {
// Format the Acknowledgement Email
		         
				 $text_customer          = $this->language->get('text_customer');
				 $text_response          = $this->language->get('text_response');
				 $response               = $this->language->get('response');
				 $text_regards           = $this->language->get('text_regards');
			     $subject                = $this->language->get('customer_subject');
                 $subject                = $store_name . $subject;
$message = "<html><head><title>$subject</title></head>";
$message .= "<body style='font-family:Verdana, Verdana, Geneva, sans-serif; font-size:12px; color:#666666;'><br />";
$message .= "<img src='$logo'><br /><br />";
$message .= "<br />". $text_customer . $this->request->post['name'] . ",<br /><br /><u><b>" . $this->request->post['contact_reason'] . " - " . $store_name . "</b></u><br /><br />";
$message .= $text_response . $response . "<br /><br /><br />" . $text_regards . "<br /><br /><br />";
$message .= $text_title . "<br />" . $store_url . "<br /><br /><br />"; 
$message .= "</body></html>";

                 $mail = new Mail();
				 
				 if ($copyemail) {
			         $mail->setTo($this->request->post['email'].','.$adminemail);
			     } else {
				     $mail->setTo($this->request->post['email']);
				 }
				 $mail->setFrom($adminemail);
			     $mail->setSender(html_entity_decode($text_title, ENT_QUOTES, 'UTF-8'));
			     $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
			     $mail->setText($this->request->post['contact_reason']);
			     $mail->setHTML($message);
			     $mail->send();
			 }

			 $this->response->redirect($this->url->link('extension/module/contactus/success'));
		  }
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/contactus')
		);

		
		$data['heading_title']       = $this->language->get('heading_title');

		$data['text_location']       = $this->language->get('text_location');
		$data['text_store']          = $this->language->get('text_store');
		$data['text_contact']        = $this->language->get('text_contact');
		$data['text_address']        = $this->language->get('text_address');
		$data['text_telephone']      = $this->language->get('text_telephone');
		$data['text_fax']            = $this->language->get('text_fax');
		$data['text_open']           = $this->language->get('text_open');
		$data['text_comment']        = $this->language->get('text_comment');
		$data['text_browse']         = $this->language->get('text_browse');
		
		$data['label_reason']        = $this->language->get('label_reason');
		
        $data['entry_reason']        = $this->language->get('entry_reason');
		$data['entry_name']          = $this->language->get('entry_name');
		$data['entry_address']       = $this->language->get('entry_address');
		$data['entry_telephone']     = $this->language->get('entry_telephone');
		$data['entry_fax']           = $this->language->get('entry_fax');
		$data['entry_email']         = $this->language->get('entry_email');
		$data['entry_file']          = $this->language->get('entry_file');
		$data['entry_enquiry']       = $this->language->get('entry_enquiry');

		$data['button_map']          = $this->language->get('button_map');

		$results = $this->model_extension_module_contactus->getReasons();
		$data['reasons']            = array();
		foreach ($results as $result) {
			$data['contact_reasons'][]      = array(
			      'order'           => $result['order'],
				  'option'          => $result['option']
			);
		}
		
		if (isset($this->error['errors'])) {
			$data['errors']         = $this->error['errors'];
		} else {
			$data['errors']         = '';
		}
		
		if (isset($this->error['reason'])) {
			$data['error_reason']   = $this->error['reason'];
		} else {
			$data['error_reason']   = '';
		}
		
		if (isset($this->error['name'])) {
			$data['error_name']     = $this->error['name'];
		} else {
			$data['error_name']     = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email']    = $this->error['email'];
		} else {
			$data['error_email']    = '';
		}

		if (isset($this->error['enquiry'])) {
			$data['error_enquiry']  = $this->error['enquiry'];
		} else {
			$data['error_enquiry']  = '';
		}

		if (isset($this->error['captcha'])) {
			$data['error_captcha']  = $this->error['captcha'];
		} else {
			$data['error_captcha']  = '';
		}
		
		if (isset($this->error['size1'])) {
			$data['error_size1']    = $this->error['size1'];
		} else {
			$data['error_size1']     = '';
		}
		if (isset($this->error['size2'])) {
			$data['error_size2']    = $this->error['size2'];
		} else {
			$data['error_size2']     = '';
		}
		if (isset($this->error['size3'])) {
			$data['error_size3']    = $this->error['size3'];
		} else {
			$data['error_size3']     = '';
		}
	
		$data['button_submit'] = $this->language->get('button_submit');

		$data['action'] = $this->url->link('extension/module/contactus');

		$this->load->model('tool/image');

		if ($this->config->get('config_image')) {
			$data['image'] = $this->model_tool_image->resize($this->config->get('config_image'), $this->config->get('config_image_location_width'), $this->config->get('config_image_location_height'));
		} else {
			$data['image'] = false;
		}

		$data['store'] = $this->config->get('config_name');
		$data['address'] = nl2br($this->config->get('config_address'));
		$data['geocode'] = $this->config->get('config_geocode');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['fax'] = $this->config->get('config_fax');
		$data['open'] = nl2br($this->config->get('config_open'));
		$data['comment'] = $this->config->get('config_comment');

		$data['locations'] = array();

		$this->load->model('localisation/location');

		foreach((array)$this->config->get('config_location') as $location_id) {
			$location_info = $this->model_localisation_location->getLocation($location_id);

			if ($location_info) {
				if ($location_info['image']) {
					$image = $this->model_tool_image->resize($location_info['image'], $this->config->get('config_image_location_width'), $this->config->get('config_image_location_height'));
				} else {
					$image = false;
				}

				$data['locations'][] = array(
					'location_id' => $location_info['location_id'],
					'name'        => $location_info['name'],
					'address'     => nl2br($location_info['address']),
					'geocode'     => $location_info['geocode'],
					'telephone'   => $location_info['telephone'],
					'fax'         => $location_info['fax'],
					'image'       => $image,
					'open'        => nl2br($location_info['open']),
					'comment'     => $location_info['comment']
				);
			}
		}
		
		if (isset($this->request->post['name'])) {
			$data['name'] = $this->request->post['name'];
		} else {
			$data['name'] = $this->customer->getFirstName()." ".$this->customer->getLastName();
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = $this->customer->getEmail();
		}

		if (isset($this->request->post['enquiry'])) {
			$data['enquiry'] = $this->request->post['enquiry'];
		} else {
			$data['enquiry'] = '';
		}

		if ($this->config->get('config_google_captcha_status')) {
			$this->document->addScript('https://www.google.com/recaptcha/api.js');

			$data['site_key'] = $this->config->get('config_google_captcha_public');
		} else {
			$data['site_key'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/extension/module/contactus')) {
			$this->response->setOutput($this->load->view($this->config->get('config_template') . '/extension/module/contactus', $data));
		} else {
			$this->response->setOutput($this->load->view('/extension/module/contactus', $data));
		}
	}

	public function success() {
		$this->load->language('extension/module/contactus');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/contactus')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_message'] = $this->language->get('text_success');
		$data['text_message_email'] = $this->language->get('text_success_email');

		$data['button_continue'] = $this->language->get('button_continue');

		$data['continue'] = $this->url->link('common/home');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('/extension/module/success', $data));

	}

	protected function validate() {

        $results = $this->model_extension_module_contactus->getParams();
		foreach ($results as $result) {
			$number                 = $result['number'];
			$maxsize                = $result['size'];
		}

        if (isset($this->request->post['1'])) {
            if($_FILES['1']['size'] > $maxsize) {
                $this->error['size1']    = $this->language->get('error_size1');
            }
		}
		if (isset($this->request->post['2'])) {
            if($_FILES['2']['size'] > $maxsize) {
                $this->error['size2']    = $this->language->get('error_size2');
            }
		}
		if (isset($this->request->post['3'])) {
            if($_FILES['3']['size'] > $maxsize) {
                $this->error['size3']    = $this->language->get('error_size3');
            }
		}
		if (isset($this->request->post['4'])) {
            if($_FILES['4']['size'] > $maxsize) {
                $this->error['size4']    = $this->language->get('error_size4');
            }
		}

	    if (!isset($this->request->post['contact_reason']) || $this->request->post['contact_reason'] == '0') {
		    $this->error['reason']      = $this->language->get('error_reason');
		}

		if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 32)) {
			$this->error['name']        = $this->language->get('error_name');
		}

		if (!filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
//		if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $this->request->post['email'])) {
			$this->error['email']       = $this->language->get('error_email');
		}

		if ((utf8_strlen($this->request->post['enquiry']) < 1) || (utf8_strlen($this->request->post['enquiry']) > 3000)) {
			$this->error['enquiry']     = $this->language->get('error_enquiry');
		}

		if ($this->config->get('config_google_captcha_status')) {
			$recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->config->get('config_google_captcha_secret')) . '&response=' . $this->request->post['g-recaptcha-response'] . '&remoteip=' . $this->request->server['REMOTE_ADDR']);

			$recaptcha = json_decode($recaptcha, true);

			if (!$recaptcha['success']) {
				$this->error['captcha'] = $this->language->get('error_captcha');
			}
		}
        
		if ($this->error) {
		    $this->error['errors']  = $this->language->get('errors');
		}
		
		return !$this->error;
	}
}