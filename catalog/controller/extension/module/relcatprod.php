<?php
class ControllerExtensionModulerelcatprod extends Controller {	
	private $error = array();
	private $modpath = 'module/relcatprod'; 
	private $modtpl = 'default/template/module/relcatprod.tpl'; 
	private $modname = 'relcatprod';
	private $modtext = 'Related Categories At Product';
	private $modid = '33973';
	private $modssl = 'SSL';
	private $modemail = 'opencarttools@gmail.com'; 
	private $langid = 0;
	
	public function __construct($registry) {
		parent::__construct($registry);
		
		$this->langid = (int)$this->config->get('config_language_id');
 		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3') { 
			$this->modtpl = 'extension/module/relcatprod';
			$this->modpath = 'extension/module/relcatprod';
		} else if(substr(VERSION,0,3)=='2.2') {
			$this->modtpl = 'module/relcatprod';
		} 
		
		if(substr(VERSION,0,3)>='3.0') { 
			$this->modname = 'module_relcatprod';
		} 
		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
			$this->modssl = true;
		} 
 	} 
	
	public function index() {
		$data['relcatprod_status'] = $this->setvalue($this->modname.'_status');
		$data['relcatprod_img_w'] = $this->setvalue($this->modname.'_img_w');
		$data['relcatprod_img_h'] = $this->setvalue($this->modname.'_img_h');
		$data['relcatprod_desc_len'] = $this->setvalue($this->modname.'_desc_len');
		$data['relcatprod_newwin'] = $this->setvalue($this->modname.'_newwin'); 
		
		if ($data['relcatprod_status'] && isset($this->request->get['product_id']) && $this->request->get['product_id']) {
			
			$this->load->model('tool/image');
			$this->load->model('catalog/product');
			$this->load->model('catalog/category');
 			 
			$relcatprod_data = $this->getrelcatproddata($this->request->get['product_id']);
 			
			$data['relcatprod_title'] = json_decode($relcatprod_data['relcatprod_title'], true);
 			$data['relcatprod_title'] = $data['relcatprod_title'][$this->langid];
			
			$categories = ($relcatprod_data['relcatprod_category_str_id']) ? explode(",", $relcatprod_data['relcatprod_category_str_id']) : array();
			
			$data['category_str_ids'] = array();
			 			
			if($categories) { 
				foreach ($categories as $category_id) {
					$related_info = $this->model_catalog_category->getCategory($category_id);
		
					if ($related_info) {
						if (is_file(DIR_IMAGE . $related_info['image'])) {
							$image = $this->model_tool_image->resize($related_info['image'], (int)$data['relcatprod_img_w'], (int)$data['relcatprod_img_h']);
						} else {
							$image = $this->model_tool_image->resize('no_image.png', (int)$data['relcatprod_img_w'], (int)$data['relcatprod_img_h']);
						} 
	
						$data['category_str_ids'][] = array(
							'thumb' => $image,
							'category_id' => $related_info['category_id'],
							'name' => $related_info['name'],
							'description' => utf8_substr(strip_tags(html_entity_decode($related_info['description'], ENT_QUOTES, 'UTF-8')), 0, (int)$data['relcatprod_desc_len']) . '..',						
							'href' => $this->url->link('product/category', 'path=' . $related_info['category_id']),
						); 
					}
				}
			}
			
			return $this->load->view($this->modtpl, $data);
 		} 
	}
	
	private function setvalue($postfield) {
		return $this->config->get($postfield);
	}
	
	public function getrelcatproddata($product_id) { 
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "relcatprod WHERE product_id = '" . (int)$product_id . "' limit 1");
		if($query->num_rows){
			return $query->row;
		} 
	}
}