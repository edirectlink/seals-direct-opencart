<?php
class ControllerCommonMenu extends Controller {
	public function index() {
		$this->load->language('common/menu');

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

			$this->load->language('extension/module/clearshop');
			$this->load->model('catalog/information');
			$data['informations'] = array();

			foreach ($this->model_catalog_information->getInformations() as $result) {
				if ($result['bottom']) {
					$data['informations'][] = array(
						'title' => $result['title'],
						'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
					);
				}
	    }

	    $this->load->model('catalog/manufacturer');
	    $this->load->model('tool/image');
	    $results = $this->model_catalog_manufacturer->getManufacturers();
	    foreach ($results as $result) {	
	    	if ($result['image']) {
					$image = $result['image'];
				} else {
					$image = 'no_image.jpg';
				}
	    	$data['manufacturers'][] = array(
	    		'name' => $result['name'],
	    		'image' => $this->model_tool_image->resize($image, 100, 50),
	    		'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id'])
	    	);
	    }

	    $data['language_id'] = $this->config->get('config_language_id');
	    $data['direction'] = $this->language->get('direction');

	    $data['clearshop_homepage_link_style'] = $this->config->get('clearshop_homepage_link_style');
	    $data['clearshop_menu_category_icons'] = $this->config->get('clearshop_menu_category_icons');
	    $data['clearshop_3rd_level_cat'] = $this->config->get('clearshop_3rd_level_cat');
	    $data['clearshop_menu_categories'] = $this->config->get('clearshop_menu_categories');
	    $data['clearshop_menu_categories_visibility'] = $this->config->get('clearshop_menu_categories_visibility');
	    $data['clearshop_categories_top_title'] = $this->config->get('clearshop_categories_top_title');
	    $data['clearshop_categories_tag'] = $this->config->get('clearshop_categories_tag');
	    $data['clearshop_categories_tag_color'] = $this->config->get('clearshop_categories_tag_color');
	    $data['clearshop_menu_categories_x_row'] = $this->config->get('clearshop_menu_categories_x_row');

	    $data['clearshop_menu_brands'] = $this->config->get('clearshop_menu_brands');
	    $data['clearshop_brands_tag'] = $this->config->get('clearshop_brands_tag');
	    $data['clearshop_brands_top_title'] = $this->config->get('clearshop_brands_top_title');
	    $data['clearshop_brands_tag_color'] = $this->config->get('clearshop_brands_tag_color');
	    $data['clearshop_menu_brands_x_row'] = $this->config->get('clearshop_menu_brands_x_row');

	    $menu_blocks = $this->config->get('clearshop_menu_blocks');

	    if ($menu_blocks) {
				foreach ($menu_blocks as $menu_block) {
					$clearshop_menu_blocks[] = array(
						'id'         => $menu_block['id'],
						'status'     => $menu_block['status'],
						'visibility' => $menu_block['visibility'],
						'title'      => $menu_block['title'][$data['language_id']],
						'content'    => html_entity_decode($menu_block['content'][$data['language_id']], ENT_QUOTES, 'UTF-8'),
						'tag'        => $menu_block['tag'][$data['language_id']],
						'tagcolor'   => $menu_block['tagcolor'][$data['language_id']],
					);
					$data['clearshop_menu_blocks'] = $clearshop_menu_blocks;
				}
			}

	    $data['clearshop_menu_infopages'] = $this->config->get('clearshop_menu_infopages');
	    $data['clearshop_infopages_top_title'] = $this->config->get('clearshop_infopages_top_title');
	    $data['clearshop_infopages_tag'] = $this->config->get('clearshop_infopages_tag');
	    $data['clearshop_infopages_tag_color'] = $this->config->get('clearshop_infopages_tag_color');

	    $data['clearshop_menu_link'] = $this->config->get('clearshop_menu_link');
	    $data['clearshop_menu_dropdown_status'] = $this->config->get('clearshop_menu_dropdown_status');
	    $data['clearshop_menu_dropdown_title'] = $this->config->get('clearshop_menu_dropdown_title');
	    $data['clearshop_menu_dropdown'] = $this->config->get('clearshop_menu_dropdown');
	    $data['clearshop_menu_dropdown_tag'] = $this->config->get('clearshop_menu_dropdown_tag');
	    $data['clearshop_menu_dropdown_tag_color'] = $this->config->get('clearshop_menu_dropdown_tag_color');

	    $data['clearshop_fullwidth_dropdown_menu'] = $this->config->get('clearshop_fullwidth_dropdown_menu');
	    $data['clearshop_menu_style'] = $this->config->get('clearshop_menu_style');

		  

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);


		  	$children_level_2 = $this->model_catalog_category->getCategories($child['category_id']);
			$children_data_level_2 = array();
			foreach ($children_level_2 as $child_level_2) {
				$data_level_2 = array(
					'filter_category_id'  => $child_level_2['category_id'],
					'filter_sub_category' => true
				);
				$product_total_level_2 = '';
				if ($this->config->get('config_product_count')) {
					$product_total_level_2 = ' (' . $this->model_catalog_product->getTotalProducts($data_level_2) . ')';
				}
				$children_data_level_2[] = array(
					'name'  =>  $child_level_2['name'],
					'href'  => $this->url->link('product/category', 'path=' . $child['category_id'] . '_' . $child_level_2['category_id']),
					'id' => $category['category_id']. '_' . $child['category_id']. '_' . $child_level_2['category_id']
				);
			}
		  
					$children_data[] = array(
						
			'name'  => $child['name'],
			'id' => $category['category_id']. '_' . $child['category_id'],
			'children_level_2' => $children_data_level_2,
		  
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				
			$this->load->model('tool/image');
	    $image = empty($category['image']) ? 'no_image.jpg' : $category['image'];
	    $thumb = $this->model_tool_image->resize($image, 100, 100);
	    $data['categories'][] = array(
	    'thumb'    => $thumb,
		  
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		return $this->load->view('common/menu', $data);
	}
}
