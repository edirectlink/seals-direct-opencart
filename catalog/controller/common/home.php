<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->load->language('information/contact');
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}


				$data['clearshop_main_layout'] = $this->config->get('clearshop_main_layout');
			
			

			$data['home_top'] = $this->load->controller('common/home_top');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		
		$data['name'] = $this->customer->getFirstName()." ".$this->customer->getLastName();
		$data['email'] = $this->customer->getEmail();
		
		// Captcha
		if ($this->config->get('captcha_' . $this->config->get('config_captcha') . '_status') && in_array('contact', (array)$this->config->get('config_captcha_page'))) {
			$data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'), $this->error);
		} else {
			$data['captcha'] = '';
		}

		$this->response->setOutput($this->load->view('common/home', $data));
		
	}
}
