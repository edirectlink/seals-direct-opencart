<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('setting/extension');

		$data['analytics'] = array();

		$analytics = $this->model_setting_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts('header');
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));
		
		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['logged_name'] = $this->customer->getFirstName()." ".$this->customer->getLastName();
		$data['account'] = $this->url->link('account/edit', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

				$data['language_id'] = $this->config->get('config_language_id');
				$data['direction'] = $this->language->get('direction');
				$data['config_url'] = $this->config->get('config_url');
				$data['config_ssl'] = $this->config->get('config_ssl');

				$data['clearshop_responsive_layout'] = $this->config->get('clearshop_responsive_layout');
				$data['aslider_general_status'] = $this->config->get('aslider_general_status');
				$data['clearshop_full_background'] = $this->config->get('clearshop_full_background');
				$data['clearshop_custom_image'] = $this->config->get('clearshop_custom_image');
				$data['clearshop_social_footer'] = $this->config->get('clearshop_social_footer');
				$data['clearshop_social'] = $this->config->get('clearshop_social');
				$data['clearshop_custom_icon'] = $this->config->get('clearshop_custom_icon');
				$data['clearshop_logo_position'] = $this->config->get('clearshop_logo_position');
				$data['clearshop_searchbox'] = $this->config->get('clearshop_searchbox');
				$data['clearshop_searchbox_position'] = $this->config->get('clearshop_searchbox_position');
				$header_info = $this->config->get('clearshop_header_info_text');
				$data['clearshop_header_info_text'] = html_entity_decode($header_info[$data['language_id']], ENT_QUOTES, 'UTF-8');
				$data['clearshop_sticky_menu'] = $this->config->get('clearshop_sticky_menu');

				$data['topbar_bg'] = $this->config->get('clearshop_topbar_background');
				$data['header_bg'] = $this->config->get('clearshop_header_background_color');
				$data['topbar_links'] = $this->config->get('clearshop_topbar_links');
				$data['header_text'] = $this->config->get('clearshop_header_text_color');
				$data['cart_total'] = $this->config->get('clearshop_cart_total_color');
				$data['cart_count'] = $this->config->get('clearshop_cart_count_color');

				$data['menu_color'] = $this->config->get('clearshop_menu_color');
				$data['menu_hover'] = $this->config->get('clearshop_menu_hover');
				$data['dropdown_color'] = $this->config->get('clearshop_dropdown_color');
				$data['dropdown_hover'] = $this->config->get('clearshop_dropdown_hover');
				$data['footer_title'] = $this->config->get('clearshop_footer_title_color');
				$data['footer_text'] = $this->config->get('clearshop_footer_text_color');
				$data['footer_links'] = $this->config->get('clearshop_footer_links_color');
				$data['title_color'] = $this->config->get('clearshop_title_color');
				$data['content_links'] = $this->config->get('clearshop_content_links_color');
				$data['bodybg_color'] = $this->config->get('clearshop_background_color'); #
				$data['bodytext_color'] = $this->config->get('clearshop_bodytext_color');
				$data['lighttext_color'] = $this->config->get('clearshop_lighttext_color');
				$data['btncart_normal'] = $this->config->get('clearshop_button_normal_bg');
				$data['btncart_hover'] = $this->config->get('clearshop_button_hover_bg');
				$data['btncart_text'] = $this->config->get('clearshop_button_text_color');
				$data['btninver_normal'] = $this->config->get('clearshop_2button_normal_bg');
				$data['btninver_hover'] = $this->config->get('clearshop_2button_hover_bg');
				$data['btninver_text'] = $this->config->get('clearshop_2button_text_color');
				$data['onsale_bg'] = $this->config->get('clearshop_onsale_background_color');
				$data['onsale_text'] = $this->config->get('clearshop_onsale_text_color');
				$data['product_name_color'] = $this->config->get('clearshop_product_name_color');
				$data['normal_price'] = $this->config->get('clearshop_normal_price_color');
				$data['old_price'] = $this->config->get('clearshop_old_price_color');
				$data['new_price'] = $this->config->get('clearshop_new_price_color');

				$data['clearshop_skins'] = $this->config->get('clearshop_skins');
				$data['clearshop_main_layout'] = $this->config->get('clearshop_main_layout');

				// Fonts
				
				$sitefonts = array_unique(array($this->config->get('clearshop_title_font'), $this->config->get('clearshop_body_font'), $this->config->get('clearshop_small_font')));

				$regfonts = array('Arial', 'Verdana', 'Helvetica', 'Lucida+Grande', 'Trebuchet+MS', 'Times+New+Roman', 'Tahoma', 'Georgia' );

				$data['googlefonts'] = array_diff($sitefonts, $regfonts);

				$data['clearshop_title_font'] = $this->config->get('clearshop_title_font');
				$data['clearshop_title_font_size'] = $this->config->get('clearshop_title_font_size');
				$data['clearshop_body_font'] = $this->config->get('clearshop_body_font');
				$data['clearshop_body_font_size'] = $this->config->get('clearshop_body_font_size');
				$data['clearshop_small_font'] = $this->config->get('clearshop_small_font');
				$data['clearshop_small_font_size'] = $this->config->get('clearshop_small_font_size');

				if ($this->config->get('clearshop_cyrillic_fonts') == 1) {
					$data['cyrillic'] = '&amp;subset=latin,cyrillic';
				} else {
					$data['cyrillic'] = '';
				}

				$data['clearshop_custom_image'] = $this->config->get('clearshop_custom_image');
				$data['clearshop_custom_pattern'] = $this->config->get('clearshop_custom_pattern');
				$data['clearshop_pattern_overlay'] = $this->config->get('clearshop_pattern_overlay');
				$data['clearshop_custom_colors'] = $this->config->get('clearshop_custom_colors');

				$data['clearshop_zoom_position'] = $this->config->get('clearshop_zoom_position');
				$data['clearshop_quickview_modules'] = $this->config->get('clearshop_quickview_modules');
				$data['clearshop_quickview_mobile'] = $this->config->get('clearshop_quickview_mobile');
				$data['clearshop_quickview_categories'] = $this->config->get('clearshop_quickview_categories');
				$data['clearshop_display_cart_button'] = $this->config->get('clearshop_display_cart_button');
				$data['clearshop_show_wishlist'] = $this->config->get('clearshop_show_wishlist');
				$data['clearshop_show_compare'] = $this->config->get('clearshop_show_compare');
				$data['clearshop_show_sale_bubble'] = $this->config->get('clearshop_show_sale_bubble');
				$data['clearshop_links_section'] = $this->config->get('clearshop_links_section');

				$data['clearshop_custom_css_status'] = $this->config->get('clearshop_custom_css_status');
				$data['clearshop_custom_css'] = htmlspecialchars_decode( $this->config->get('clearshop_custom_css'), ENT_QUOTES );
				$data['clearshop_custom_stylesheet'] = $this->config->get('clearshop_custom_stylesheet');

				$data['clearshop_fullwidth_dropdown_menu'] = $this->config->get('clearshop_fullwidth_dropdown_menu');
				$data['clearshop_homepage_link_style'] = $this->config->get('clearshop_homepage_link_style');
				$data['clearshop_menu_style'] = $this->config->get('clearshop_menu_style');

				$data['customer_firstname'] = $this->customer->getFirstName();
				$data['customer_lastname'] = $this->customer->getLastName();
			
		
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');
		$data['menu'] = $this->load->controller('common/menu');

		return $this->load->view('common/header', $data);
	}
}
