<?php
class ControllerPaymentInvoicepay extends Controller {

  private $debug = false;
  private $OC_V2;
  
	public function __construct($registry) {
		$this->OC_V2 = substr(VERSION, 0, 1) == 2;
		parent::__construct($registry);
	}

	public function index() {
		$this->load->model('checkout/order');
		$this->language->load('payment/invoicepay');
		
		$data['_language'] = &$this->language;
		$data['_config'] = &$this->config;
		$data['_url'] = &$this->url;
		$data['OC_V2'] = $this->OC_V2;
    $data['text_loading'] = $this->language->get('text_loading');
    $data['continue'] = $this->url->link('checkout/success');
		$data['button_confirm'] = $this->language->get('button_confirm');
		$data['text_purchase_order'] = $this->language->get('text_purchase_order');
		$data['purchase_order_required'] = $this->language->get('purchase_order_required');
		$data['entry_purchase_order'] = $this->language->get('entry_purchase_order');
		
		if(isset($this->request->get['order_id'])){
			$this->session->data['order_id'] = (int) $this->request->get['order_id'];
		}

		// form parameters 
		$data['action'] = $this->url->link('payment/invoicepay/confirm');
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		$data['total'] = (int) $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']*100, false);
		
		$data['purchase_order'] = $this->config->get('invoicepay_po');
    
		$data['description'] = html_entity_decode($this->config->get('invoicepay_desc_' . $this->config->get('config_language_id')), ENT_QUOTES, 'UTF-8');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/invoicepay.tpl')) {
			$template = $this->config->get('config_template') . '/template/payment/invoicepay.tpl';
		} else {
			$template = 'default/template/payment/invoicepay.tpl';
		}	
		
    if (version_compare(VERSION, '2.3', '>=')) {
      if (version_compare(VERSION, '3', '>=')) {
        //$this->config->set('template_engine', 'template');
        $return = $this->load->view('payment/invoicepay', $data);
        //$this->config->set('template_engine', 'twig');
        return $return;
      }
			return $this->load->view('payment/invoicepay', $data);
		} else if (version_compare(VERSION, '2', '>=')) {
			return $this->load->view($template, $data);
		} else {
			$this->data = $data;
			$this->template = $template;
			$this->render();
		}
	}
	
	public function confirm() {
    if ($this->session->data['payment_method']['code'] == 'invoicepay') {
      $this->load->model('checkout/order');
      $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
      
      if (!empty($this->request->get['po_number'])) {
        $this->language->load('payment/invoicepay');

        if (!empty($order_info['comment'])) {
          $order_info['comment'] = $this->language->get('entry_purchase_order') . ' ' . $this->request->get['po_number'] . "\n\n" . $order_info['comment'];
        } else {
          $order_info['comment'] = $this->language->get('entry_purchase_order') . ' ' . $this->request->get['po_number'];
        }
        
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET po_number = '" . $this->db->escape($this->request->get['po_number']) . "' WHERE order_id = '" . (int)$this->session->data['order_id'] . "'");
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET comment = '" . $this->db->escape($order_info['comment']) . "' WHERE order_id = '" . (int)$this->session->data['order_id'] . "'");
      }
      
      $message = html_entity_decode($this->config->get('invoicepay_notify_message_'.$order_info['language_id']), ENT_QUOTES, 'UTF-8');
      
      if (method_exists($this->model_checkout_order, 'confirm')) {
        $this->model_checkout_order->confirm($this->session->data['order_id'], $this->config->get('invoicepay_paid_status_id'), $message, $this->config->get('invoicepay_order_notify'));
      } else {
        $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('invoicepay_paid_status_id'), $message, $this->config->get('invoicepay_order_notify'));
      }
    }
	}

}