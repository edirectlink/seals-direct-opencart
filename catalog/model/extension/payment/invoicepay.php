<?php
$_modpath = 'catalog/model/payment/invoicepay.php';
if (defined('JPATH_MIJOSHOP_OC')) {
  if(file_exists(JPATH_MIJOSHOP_OC.'/system/modification/'.$_modpath)) {
    require_once(JPATH_MIJOSHOP_OC.'/system/modification/'.$_modpath);
  } else {
    require_once(JPATH_MIJOSHOP_OC.'/'.$_modpath);
  }
} else if (isset($vqmod)) {
	require_once($vqmod->modCheck(DIR_SYSTEM.'../'.$_modpath));
} else if (class_exists('VQMod')) {
	require_once(VQMod::modCheck(DIR_SYSTEM.'../'.$_modpath));
} else {
	require_once(DIR_SYSTEM.'../'.$_modpath);
}

class ModelExtensionPaymentInvoicepay extends ModelPaymentInvoicepay {}