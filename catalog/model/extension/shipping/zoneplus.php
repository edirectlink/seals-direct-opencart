<?php
//-----------------------------------------
// Author: Qphoria@gmail.com
// Web: http://www.OpenCartGuru.com/
//-----------------------------------------
class ModelExtensionShippingZonePlus extends Model {
	private $name = '';

	public function getQuote($address) {
		$classname = str_replace('vq2-catalog_model_extension_shipping_', '', basename(__FILE__, '.php'));
        //$this->load->language('extension/shipping/' . $classname);

		$geozones = $this->db->query("SELECT * FROM " . DB_PREFIX . "geo_zone ORDER BY name");

		$quote_data = array();

		// Reset the debug log
		file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "");

		foreach ($geozones->rows as $result) {

			if ($this->config->get('zoneplus_status_' . $result['geo_zone_id'])) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$result['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

				if ($query->num_rows) {
       				$status = TRUE;
   				} else {
       				$status = FALSE;
					file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "\r\n-----\r\nA) STATUS FALSE FOR GEOZONE: " . $result['name'] . ". REASON: NOT IN GEOZONE", FILE_APPEND);
   				}
			} else {
				$status = FALSE;
				file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "\r\n-----\r\nA) STATUS FALSE FOR GEOZONE: " . $result['name'] . ". REASON: NOT ENABLED", FILE_APPEND);
			}

			# Address filter Check.
			if ($status) {

				$keyword = $this->config->get('zoneplus_filter_' . $result['geo_zone_id']);

				if ($keyword) {

					$break = false; // if true, skip the exclude check

					if ($this->config->get('zoneplus_include_' . $result['geo_zone_id'])) {
						$match = 0;
						$filters = explode(',', str_replace('~', '', $this->config->get('zoneplus_include_' . $result['geo_zone_id'])));
						foreach($filters as $filter) {
							if (stripos($address[$keyword], trim($filter)) === false) { //if the value doesn't match - status false
								$status = false;
								file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "\r\n-----\r\nB) STATUS FALSE FOR GEOZONE: " . $result['name'] . ". EASON: NOT IN INCLUDE FILTER", FILE_APPEND);
								$break = true; //If not in the include, don't check exlude as reverse logic will incorrectly activate it
							} elseif (stripos($address[$keyword], trim($filter)) > 0) { //if the value doesn't match at the first position - status false
								$status = false;
								file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "\r\n-----\r\nC) STATUS FALSE FOR GEOZONE: " . $result['name'] . ". REASON: NOT IN FIRST POSITION OF INCLUDE FILTER", FILE_APPEND);
							} else {
								$status = true;
								$break = false;
								break;
							}
						}
					}

					if (!$break && $this->config->get('zoneplus_exclude_' . $result['geo_zone_id'])) { // If "Geozone Description" has a ! in it, then exclude by keyword
						$match = 0;
						$filters = explode(',', str_replace('!', '', $this->config->get('zoneplus_exclude_' . $result['geo_zone_id'])));
						foreach($filters as $filter) {
							if (stripos($address[$keyword], trim($filter)) === false) { //if the value doesn't match - status true
								$status = true;
							} elseif (stripos($address[$keyword], trim($filter)) > 0) { //if the value doesn't match at the first position - status true
								$status = true;
							} else {
								$status = false;
								file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "\r\n-----\r\nD) STATUS FALSE FOR GEOZONE: " . $result['name'] . ". REASON: INCLUDED IN EXCLUDE FILTER", FILE_APPEND);
								break;
							}
						}
					}
				}
			}//

			if ($status) {

				$inRange = false;
				$cost = 0;

				// set the unit of calculation to $value (weight, subtotal, or itemcount)
				if ($this->config->get($classname . '_method_' . $result['geo_zone_id']) == 'itemcount'){
					$value = 0;
					foreach ($this->cart->getProducts() as $product) {
						if ($product['shipping']) {
							//$value++;
							$value += $product['quantity'];
						}
					}
					//$value = $this->cart->countProducts(); // this doesn't take into account products with shipping set to NO
				} elseif ($this->config->get($classname . '_method_' . $result['geo_zone_id']) == 'subtotal'){
					$value = 0;
					foreach ($this->cart->getProducts() as $product) {
						if ($product['shipping']) {
							$value += $product['total'];
						}
					}
				} elseif ($this->config->get($classname . '_method_' . $result['geo_zone_id']) == 'total'){
					$value = 0;
					foreach ($this->getProducts() as $product) {
						if ($product['shipping']) {
							$value += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
						}
					}
				} else { // default to weight-based
					$value = $this->cart->getWeight();
				}

				// set the operator based on whether its incremental or decremental
				if ($this->config->get($classname . '_calc_' . $result['geo_zone_id']) == 'dec') {
					$op = '>=';
					$sort_method = 'dec';
				} else { //default to inc
					$op = '<=';
					$sort_method = 'inc';
				}

				$rates = explode(',', $this->config->get($classname . '_cost_' . $result['geo_zone_id']));
				if ($sort_method == 'dec') { rsort($rates, SORT_NUMERIC); }
				foreach ($rates as $rate) {
					file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', print_r($rates,1) . "\r\nValue:" . $value . "\r\nOp:" . $op);
					$array = explode(':', $rate);
					if (!empty($array) && isset($array[0]) && isset($array[1]) && $array[0] != '' && @(string)$array[1] != '') {
						if (eval("return($value $op $array[0]);")) {
							$cost = $array[1];
							if (strpos($cost, '%')) {
								$cost = trim($cost, '%');
								$cost = $this->cart->getSubtotal() * ($cost/100);
							}
							if ($cost == '-') { // Don't show 0.00 for "-"
								$inRange = false;
								file_put_contents(DIR_LOGS . 'zone_plus_debug.txt', "\r\n-----\r\nE) STATUS FALSE FOR GEOZONE: " . $result['name'] . ". REASON: NOT IN RANGE:\r\n"  . print_r($rate,1) . " WITH OP: $op and SORT: $sort_method", FILE_APPEND);
							} else {
								$inRange = true;
							}
							break;
						}
					}
				}

				if ($inRange == true) {
					$text_cost = $this->currency->convert($cost, $this->config->get('config_currency'), $this->session->data['currency']);
					$quote_data[$classname . '_' . $result['geo_zone_id']] = array(
						'id'    		=> $classname . '.' . $classname . '_' . $result['geo_zone_id'], //v14x
						'code'    		=> $classname . '.' . $classname . '_' . $result['geo_zone_id'], //v15x
						'title' 		=> $result['name'],
						'cost'  		=> $cost,
						'tax_class_id' 	=> $this->config->get($classname . '_tax_class_id'),
						//'text'  		=> $this->currency->format($this->tax->calculate($cost,     $this->config->get($classname . '_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'])
						'text'  		=> $this->currency->format($this->tax->calculate($text_cost, $this->config->get($classname . '_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency'], '1.0000', true)
					);
				}
			}
		}


		$method_data = array();

		$title = ($this->config->get($classname . '_title_' . $this->config->get('config_language_id')) ? $this->config->get($classname . '_title_' . $this->config->get('config_language_id')) : ucwords(str_replace(array('-','_','.'), " ", $classname)));

		if ($quote_data) {
      		$method_data = array(
        		'id'         => $classname, //v14x
                'code'       => $classname, //v15x
        		'title'      => $title,
        		'quote'      => $quote_data,
				'sort_order' => $this->config->get($classname . '_sort_order'),
        		'error'      => FALSE
      		);
		}

		return $method_data;
  	}
}
?>