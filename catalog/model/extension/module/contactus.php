<?php
class ModelExtensionModuleContactUs extends Model {
	
	public function getReasons() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "contactus_select` ORDER BY `order` ASC");

		return $query->rows;
	}
	
	public function getParams() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "contactus_params`");

		return $query->rows;
	}
}

