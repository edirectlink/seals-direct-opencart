<div><?php echo $description; ?></div>

<?php if ($purchase_order) { ?>
  <?php if (version_compare(VERSION, '2', '>')) { ?>
  <fieldset class="form-horizontal">
    <legend><?php echo $_language->get('text_purchase_order'); ?></legend>
    <div class="po_alert alert alert-danger <?php if (version_compare(VERSION, '2', '<')) echo ' warning'; ?>" style="display:none"><i class="fa fa-exclamation-circle"></i> <?php echo $_language->get('purchase_order_required'); ?></div>
    <div id="po_form_group" class="form-group<?php if ($purchase_order == 'required') echo ' required'; ?>">
      <label class="col-sm-3 control-label" for="input-po"><?php echo $_language->get('entry_purchase_order'); ?></label>
      <div class="col-sm-5">
        <input name="purchase_order_number" value="" id="input-po" class="form-control" type="text">
      </div>
    </div>
  </fieldset>
  <?php } else { ?>
  <div class="po_alert alert alert-danger <?php if (version_compare(VERSION, '2', '<')) echo ' warning'; ?>" style="display:none"><i class="fa fa-exclamation-circle"></i> <?php echo $_language->get('purchase_order_required'); ?></div>
  <div class="<?php if ($purchase_order == 'required') echo 'required'; ?>" style="margin-bottom:40px;"><label><?php echo $_language->get('entry_purchase_order'); ?>  <input name="purchase_order_number" value="" type="text"></label></div>
  <?php } ?>
<?php } ?>

<div class="buttons">
  <div class="pull-right right">
    <input type="button" value="<?php echo $button_confirm; ?>" id="button-confirm" class="button btn btn-primary" data-loading-text="<?php echo $text_loading; ?>" />
  </div>
</div>
<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
  <?php if ($purchase_order == 'required') { ?>
    $('.po_alert').hide(0);
    if (!$('input[name=purchase_order_number]').val()) {
      //alert('<?php echo $_language->get('purchase_order_required'); ?>');
      $('.po_alert').fadeIn();
      $('#po_form_group').addClass('has-error');
      return;
    }
  <?php } ?>
	$.ajax({
		type: 'get',
		url: 'index.php?route=payment/invoicepay/confirm',
    <?php if ($purchase_order) { ?>
    data: {po_number: $('input[name=purchase_order_number]').val()},
    <?php } ?>
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function() {
			location = '<?php echo $continue; ?>';
		}
	});
});
//--></script>