/**
 * UK Postcode Lookup by Crafty Clicks
 *
 * @author		Crafty Clicks Limited
 * @link		https://craftyclicks.co.uk
 * @copyright  	Copyright (c) 2016, Crafty Clicks Limited
 * @license     Licensed under the terms of the MIT license.
 * @version		3.0.0
**/

// CONFIGURATION
var _cp_config = {
	selectFirstAddress:		0,
	enabledCountries:		['222', '254', '256', '257'],
	busy_img_url:			'catalog/view/javascript/craftyclicks/crafty_postcode_busy.gif',
	traditional_county:		1
};
// END OF CONFIGURATION

var prefixes = [
	'-payment',
	'-shipping',
	''
];

function cc_check(){
	if(	typeof jQuery == 'undefined' ||
		typeof CraftyPostcodeCreate == 'undefined' ||
		typeof craftyclicksConfig.status == 'undefined' ||
		craftyclicksConfig.status != 1 ||
		(!jQuery('#input-payment-country').length && !jQuery('#input-shipping-country').length && !jQuery('#input-country').closest('fieldset').length)){
			setTimeout(cc_check, 200);
		}
	else {
		setInterval(crafty_interval, 200);
	}
}

function crafty_interval(){
	for(var j=0; j<prefixes.length; j++){
		var country_elem = jQuery('#input'+prefixes[j]+'-country');
		if(country_elem.length){
			if(jQuery.inArray(country_elem.val(), _cp_config.enabledCountries) != -1 && jQuery(country_elem).attr('cc-applied') != '1'){
				jQuery(country_elem).attr('cc-applied', '1');
				addLookup(prefixes[j]);
			}
		}
	}
	var checkoutPage = jQuery('.checkout-checkout').length;
	if(	checkoutPage && prefixes.length > 1 ||
		!checkoutPage && prefixes.length > 2)
			setTimeout(function(){
				crafty_interval(prefixes);
			}, 200);
}
jQuery(document).ready(function(){
	cc_check();
});

function addLookup(prefix){

    console.log(prefix);
	var craftyFieldWidth = jQuery('#input'+prefix+'-postcode').css('width');
	UKArrangement(prefix);
	addCraftyButton(prefix);
	if(prefix == '-payment' && !jQuery('#payment-new').length){
		var craftyResult_1 = jQuery('<div class="form-group" id="crafty_postcode_result_display'+prefix+'"></div>');

	}
	else {
		var craftyResult_1 = jQuery('<div class="form-group">'+
										'<label class="col-sm-2 control-label"></label>'+
										'<div class="col-sm-10" id="crafty_postcode_result_display'+prefix+'"></div>'+
									'</div>');
	}
	var cp_obj = CraftyPostcodeCreate();
	cp_obj.set('access_token',			craftyclicksConfig.access_token);
	cp_obj.set('result_elem_id',		'crafty_postcode_result_display'+prefix);
	cp_obj.set('max_width',				craftyFieldWidth);
	cp_obj.set('traditional_county',	_cp_config.traditional_county);
	cp_obj.set('elem_company',			'input'+prefix+'-company');
	cp_obj.set('elem_street1',			'input'+prefix+'-address-1');
	cp_obj.set('elem_street2',			'input'+prefix+'-address-2');
	cp_obj.set('elem_town',				'input'+prefix+'-city');
	cp_obj.set('elem_postcode',			'input'+prefix+'-postcode');
	cp_obj.set('elem_county',			'input'+prefix+'-zone');
	cp_obj.set('on_result_ready',		function(){
		craftyResult_1.find('select').addClass('form-control');
		craftyResult_1.show();
	});
	cp_obj.set('res_autoselect',		_cp_config.selectFirstAddress);
	cp_obj.set('busy_img_url',			_cp_config.busy_img_url);
	cp_obj.set('on_result_selected',	function(){
		var country_element = jQuery('#input'+prefix+'-country');
		var county_element = jQuery('#input'+prefix+'-zone');



		var postcodeInitial = _cp_addr_data.postcode.substring(0,2);
		switch(postcodeInitial){
			case 'IM':
				country_element.val(254).trigger('change');
				break;
			case 'GY':
				country_element.val(256).trigger('change');
				break;
			case 'JE':
				country_element.val(257).trigger('change');
				break;
			default:
				country_element.val(222).trigger('change');
				break;
		}
		jQuery('#input'+prefix+'-zone option:first').attr('cc-tagged', 1);
		var countyChecker = setInterval(function(){
			if (jQuery('#input'+prefix+'-zone option:first').attr('cc-tagged') !== '1') {
				countyChange(jQuery('#input'+prefix+'-zone option'), jQuery('#input'+prefix+'-zone'));
				clearInterval(countyChecker);
			}
		},10);
		var cp_line1 = jQuery('#input'+prefix+'-address-1');
		if(cp_line1.val().length < 5){
			var cp_line2 = jQuery('#input'+prefix+'-address-2');
			cp_line1.val(cp_line1.val() + ', ' + cp_line2.val());
			cp_line2.val('');
		}
	});
	// Do the lookup
	jQuery('#cc-button'+prefix).on('click', function(){
		var pc_elem =	jQuery('#input'+prefix+'-postcode').closest('div.form-group');
		var disp_elem =	jQuery('#crafty_postcode_result_display'+prefix);
		if(!disp_elem.length){
			pc_elem.after(craftyResult_1);
		}
		cp_obj.doLookup();
	});

	// Country change triggers
	jQuery('#input'+prefix+'-country').on('change', function(){
		if(jQuery.inArray(jQuery(this).val(), _cp_config.enabledCountries) != -1){
			UKArrangement(prefix);
			addCraftyButton(prefix);
		}
		else {
			nonUKArrangement(prefix);
		}
	});
}

// Add lookup button
function addCraftyButton(prefix){
	var btn_elem = jQuery('#cc-button'+prefix);
	if(btn_elem.length) {
		btn_elem.closest('div.input-group-btn').show();
		return;
	}
	var craftyHtml = jQuery('<div class="input-group-btn">'+
								'<div id="cc-button'+prefix+'" class="btn btn-primary">'+
									'Find Address'+
								'</div>'+
							'</div>');
	var pc_elem = jQuery('#input'+prefix+'-postcode');
	pc_elem.wrap('<div id="cc-input-group'+prefix+'" class="input-group"></div>');
	pc_elem.after(craftyHtml);
	craftyHtml.find('btn').css({fontSize: '12px'});
}

// Form arrangement for UK
function UKArrangement(prefix){
	jQuery('#cc-input-group'+prefix).addClass('input-group');
	var company_elem = jQuery('#input'+prefix+'-company').closest('div.form-group');
	var pc_elem = jQuery('#input'+prefix+'-postcode').closest('div.form-group');
	company_elem.before(pc_elem);
	var display_elem = jQuery('#crafty_postcode_result_display'+prefix);
	if(display_elem.hasClass('form-group')){
		pc_elem.after(display_elem);
	}
	else {
		pc_elem.after(display_elem.closest('div.form-group'));
	}
}

// Form arrangement for non-UK
function nonUKArrangement(prefix){
	jQuery('#cc-input-group'+prefix).removeClass('input-group');
	jQuery('#cc-button'+prefix).closest('div.input-group-btn').hide();
	var display_elem = jQuery('#crafty_postcode_result_display'+prefix);
	if(display_elem.hasClass('form-group')){
		display_elem.hide();
	}
	else {
		display_elem.closest('div.form-group').hide();
	}
	var city_elem = jQuery('#input'+prefix+'-city').closest('div.form-group');
	var pc_elem = jQuery('#input'+prefix+'-postcode').closest('div.form-group');
	city_elem.after(pc_elem);
}

function countyChange(inputOptions, input) {
	var inList = false;
	for(var i=0; i<inputOptions.length; i++){
		if(jQuery(inputOptions[i]).html().toUpperCase() === _cp_addr_data.traditional_county){
				input.val(jQuery(inputOptions[i]).val());
				inList = true;
		}
	}
	if (!inList) {
		var countyData = [
			['ARGYLLSHIRE', 3517],
			['AYRSHIRE', 3574],
			['BANFFSHIRE', 3570],
			['BERWICKSHIRE', 3588],
			['BRECKNOCKSHIRE', 3584],
			['BUTESHIRE', 3517],
			['CAERNARFONSHIRE', 3555],
			['CAITHNESS', 3559],
			['CARDIGANSHIRE', 3528],
			['COUNTY DURHAM', 3539],
			['CUMBERLAND', 3955],
			['DUMFRIESSHIRE', 3537],
			['DUNBARTONSHIRE', 3541],
			['GLAMORGAN', 3526],
			['HUNTINGDONSHIRE', 3525],
			['INVERNESS-SHIRE', 3559],
			['ISLE OF LEWIS', 3609],
			['ISLE OF SKYE', 3609],
			['ISLES OF SCILLY', 3532],
			['KINCARDINESHIRE', 3514],
			['KINROSS-SHIRE', 3583],
			['KIRKCUDBRIGHTSHIRE', 3537],
			['LANARKSHIRE', 3551],
			['MERIONETH', 3555],
			['MIDDLESEX', 3553],
			['MONTGOMERYSHIRE', 3584],
			['MORAYSHIRE', 3570],
			['NAIRNSHIRE', 3559],
			['ORKNEY', 3580],
			['PEEBLESSHIRE', 3588],
			['PERTHSHIRE', 3583],
			['RADNORSHIRE', 3584],
			['ROSS-SHIRE', 3559],
			['ROXBURGHSHIRE', 3588],
			['SELKIRKSHIRE', 3588],
			['SHETLAND', 3589],
			['STIRLINGSHIRE', 3596],
			['SUSSEX', 3607],
			['SUTHERLAND', 3559],
			['WESTMORLAND', 3955],
			['WIGTOWNSHIRE', 3537],
			['YORKSHIRE', 3576]
		];

		for(j=0;j<countyData.length;j++) {
			var currentCounty = countyData[j][0];
			var currentValue = countyData[j][1];
			if (_cp_addr_data.traditional_county === currentCounty) {
				input.val(currentValue);
			}
		}
	}

}
