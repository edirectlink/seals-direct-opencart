<?php

/* clearshop/template/common/header.twig */
class __TwigTemplate_48108d9b3d402791e6a2270e9478226ad8b6874286a9c19d14be8ef2c479e85e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo " \" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo " \" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo " \" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo " \" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo " \" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo " \">
<!--<![endif]-->

<head>
\t<meta charset=\"UTF-8\" />
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">

\t<title>";
        // line 14
        echo (isset($context["title"]) ? $context["title"] : null);
        echo " </title>

\t<base href=\"";
        // line 16
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />

\t";
        // line 18
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 19
            echo "\t<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
\t";
        }
        // line 21
        echo "
\t";
        // line 22
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 23
            echo "\t<meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
\t";
        }
        // line 25
        echo "
\t";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 27
            echo "\t<link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "
\t";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 31
            echo "\t<link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
\t<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
\t<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
\t<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
\t<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />

\t<link href=\"catalog/view/theme/clearshop/stylesheet/jasny-bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
\t<script src=\"catalog/view/theme/clearshop/js/jasny-bootstrap.min.js\" type=\"text/javascript\"></script>

\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/clearshop/stylesheet/stylesheet.css\"/>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/clearshop/stylesheet/stylesheet-responsive.css\" media=\"screen\"/>

\t";
        // line 45
        if (((isset($context["direction"]) ? $context["direction"] : null) == "rtl")) {
            // line 46
            echo "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/clearshop/stylesheet/stylesheet.rtl.css\" media=\"screen\" />
\t";
        }
        // line 48
        echo "
\t<script src=\"https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js\"></script>

\t<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/jquery.magnific-popup.min.js\"></script>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/clearshop/stylesheet/magnific-popup.css\" media=\"screen\" />

\t<script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>

\t<script src=\"catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js\" type=\"text/javascript\"></script>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/javascript/jquery/owl-carousel/owl.carousel.css\" media=\"screen\"/>
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/javascript/jquery/owl-carousel/owl.transitions.css\" media=\"screen\"/>

\t";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 61
            echo "\t<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 63
        echo "
\t";
        // line 64
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 65
            echo "\t";
            echo $context["analytic"];
            echo "
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "
\t";
        // line 68
        $context["base_class"] = "";
        // line 69
        echo "
\t";
        // line 70
        if (((isset($context["clearshop_skins"]) ? $context["clearshop_skins"] : null) != "")) {
            // line 71
            echo "\t\t";
            $context["base_class"] = ((isset($context["clearshop_skins"]) ? $context["clearshop_skins"] : null) . "-skin");
            // line 72
            echo "\t";
        }
        // line 73
        echo "
\t";
        // line 74
        if ((isset($context["body_classes"]) ? $context["body_classes"] : null)) {
            // line 75
            echo "\t\t";
            $context["base_class"] = ((isset($context["base_class"]) ? $context["base_class"] : null) . (isset($context["body_classes"]) ? $context["body_classes"] : null));
            // line 76
            echo "\t";
        }
        // line 77
        echo "
\t";
        // line 78
        echo twig_include($this->env, $context, "clearshop/template/common/custom_styles.twig");
        echo "

</head>

";
        // line 82
        $context["lang"] = (isset($context["language"]) ? $context["language"] : null);
        echo " 

<body class=\"";
        // line 84
        echo (isset($context["base_class"]) ? $context["base_class"] : null);
        echo " \">

";
        // line 86
        echo twig_include($this->env, $context, "clearshop/template/common/menumob.twig");
        echo "

<div id=\"supercontainer\" class=\"topcanvas\">

\t<div id=\"thickbar\"></div>

\t<header id=\"header\">

\t\t<div class=\"container\">

\t\t\t<div class=\"row ";
        // line 96
        echo (isset($context["clearshop_logo_position"]) ? $context["clearshop_logo_position"] : null);
        echo "\">

\t\t\t\t<div id=\"logo\" class=\"col-sm-4 col-xs-6 ";
        // line 98
        if (((isset($context["clearshop_logo_position"]) ? $context["clearshop_logo_position"] : null) == "center")) {
            echo " col-md-4 col-sm-push-4 ";
        } else {
            echo " col-md-3 ";
        }
        echo "\">
\t\t\t\t\t";
        // line 99
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 100
            echo "\t\t\t\t\t\t<a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\" class=\"logo\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo " \" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo " \" class=\"img-responsive\" /></a>
\t\t\t\t\t";
        } else {
            // line 102
            echo "\t\t\t\t\t\t<a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\" class=\"logo\"><img src=\"catalog/view/theme/clearshop/image/logo.png\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" class=\"img-responsive\" /></a>
\t\t\t\t\t";
        }
        // line 104
        echo "\t\t\t\t</div> <!-- #logo -->

\t\t\t\t<div class=\"col-xs-3 leftbox ";
        // line 106
        if (((isset($context["clearshop_logo_position"]) ? $context["clearshop_logo_position"] : null) == "center")) {
            echo " col-sm-4 col-sm-pull-4 ";
        } else {
            echo " col-md-6 col-sm-5 ";
        }
        echo "\">

\t\t\t\t\t<div class=\"search-area\">
\t\t\t\t\t\t";
        // line 109
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "
\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 112
        if ((strip_tags((isset($context["clearshop_header_info_text"]) ? $context["clearshop_header_info_text"] : null)) != "")) {
            // line 113
            echo "\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t<div class=\"header-info owl-carousel ";
            // line 114
            if (((isset($context["clearshop_logo_position"]) ? $context["clearshop_logo_position"] : null) == "center")) {
                echo " hidden-sm ";
            }
            echo " hidden-xs\">
\t\t\t\t\t\t\t";
            // line 115
            echo (isset($context["clearshop_header_info_text"]) ? $context["clearshop_header_info_text"] : null);
            echo "
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<script type=\"text/javascript\">
\t\t\t\t\t\t\$('.header-info').owlCarousel({
\t\t\t\t\t\t\titems:6,
\t\t\t\t\t\t\tsingleItem: true,
\t\t\t\t\t\t\tloop: true,
\t\t\t\t\t\t\tautoPlay: 3000,
\t\t\t\t\t\t\tnavigation: false,
\t\t\t\t\t\t\tpagination: false,
\t\t\t\t\t\t\ttransitionStyle: \"fade\"
\t\t\t\t\t\t});
\t\t\t\t\t\t</script>

\t\t\t\t\t";
        }
        // line 131
        echo "
\t\t\t\t</div> <!-- .middlebox -->

\t\t\t\t<div class=\"col-xs-6 rightbox ";
        // line 134
        if (((isset($context["clearshop_logo_position"]) ? $context["clearshop_logo_position"] : null) == "center")) {
            echo " col-sm-4 ";
        } else {
            echo " col-sm-3 ";
        }
        echo "\">

\t\t\t\t\t<button type=\"button\" class=\"btn btn-menu visible-sm-inline visible-xs-inline\" onclick=\"\$('.topcanvas').addClass('canvas-slid');\" data-toggle=\"offcanvas\" data-target=\"#mobmenu\" data-disable-scrolling=\"true\" data-modal=\"true\" ><i class=\"fa fa-bars\"></i></button>

\t\t\t\t\t<button type=\"button\" class=\"search-trigger btn btn-link btn-header visible-xs-inline\">
\t\t\t\t\t\t<i class=\"fa fa-search\"></i>
\t\t\t\t\t</button>

\t\t\t\t\t<div id=\"top-links\" class=\"btn-group dropdown hidden-sm hidden-xs\">
\t\t\t\t\t\t<a href=\"";
        // line 143
        echo (isset($context["account"]) ? $context["account"] : null);
        echo " \" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo " \" class=\"btn btn-link dropdown-toggle myaccount\" data-toggle=\"dropdown\"><i class=\"fa fa-user-circle-o\"></i></a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\">
                            ";
        // line 145
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 146
            echo "\t\t\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo " \">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo " </a></li>
\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 147
            echo (isset($context["order"]) ? $context["order"] : null);
            echo " \">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo " </a></li>
\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 148
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo " \">";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo " </a></li>
\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 149
            echo (isset($context["download"]) ? $context["download"] : null);
            echo " \">";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo " </a></li>
\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 150
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo " \"><i class=\"fa fa-sign-out\"></i> ";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo " </a></li>
                            ";
        } else {
            // line 152
            echo "\t\t\t\t\t\t\t\t<li><a href=\"";
            echo (isset($context["register"]) ? $context["register"] : null);
            echo " \">";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo " </a></li>
\t\t\t\t\t\t\t\t<li><a href=\"";
            // line 153
            echo (isset($context["login"]) ? $context["login"] : null);
            echo " \">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo " </a></li>
                            ";
        }
        // line 155
        echo "\t\t\t\t\t\t\t<li class=\"divider wishlist\"></li>
\t\t\t\t\t\t\t<li><a href=\"";
        // line 156
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo " \" id=\"wishlist-total\" title=\"";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo " \" class=\"wishlist\"><i class=\"fa fa-heart-o\"></i> <span>";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo " </a></span></li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"btn-group dropdown site-settings hidden-sm hidden-xs\">
\t\t\t\t\t\t<a href=\"";
        // line 161
        echo (isset($context["account"]) ? $context["account"] : null);
        echo " \" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo " \" class=\"btn btn-link dropdown-toggle myaccount\" data-toggle=\"dropdown\"><i class=\"fa fa-ellipsis-v\"></i></a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t\t\t\t\t<li>";
        // line 163
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "</li>
\t\t\t\t\t\t\t<li>";
        // line 164
        echo (isset($context["currency"]) ? $context["currency"] : null);
        echo "</li>
\t\t\t\t\t\t</ul>
\t\t\t\t\t</div>

\t\t\t\t\t";
        // line 168
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t</div> <!-- .leftlogo -->

\t\t</div>

\t\t";
        // line 176
        echo twig_include($this->env, $context, "clearshop/template/common/menudesk.twig");
        echo "

\t\t";
        // line 178
        if (((isset($context["clearshop_sticky_menu"]) ? $context["clearshop_sticky_menu"] : null) == 1)) {
            // line 179
            echo "
\t\t<script type=\"text/javascript\">
\t\t\tvar startPosition = 101;
\t\t\tvar contentMargin =  28;

\t\t\t\$(window).scroll(function() {
\t\t\t\tif(\$(window).scrollTop() > startPosition) {
\t\t\t\t\twidth = \$('.container #menu').width();
\t\t\t\t\theight = \$('.container #menu').height();
\t\t\t\t\t\$('#menu .navbar').addClass('navbar-fixed-top')
\t\t\t\t\t\$('#menu .navcontainer').addClass('container')
\t\t\t\t\t\$('#cart').addClass('fixed')
\t\t\t\t} else {
\t\t\t\t\t\$('#menu .navbar').removeClass('navbar-fixed-top');
\t\t\t\t\t\$('#menu .navcontainer').removeClass('container')
\t\t\t\t\t\$('#cart').removeClass('fixed');
\t\t\t\t}
\t\t\t}); 
\t\t</script>

\t\t";
        }
        // line 200
        echo "
\t</header> <!-- #header -->

\t<div id=\"content-wrapper\">
\t\t\t";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  474 => 200,  451 => 179,  449 => 178,  444 => 176,  433 => 168,  426 => 164,  422 => 163,  415 => 161,  403 => 156,  400 => 155,  393 => 153,  386 => 152,  379 => 150,  373 => 149,  367 => 148,  361 => 147,  354 => 146,  352 => 145,  345 => 143,  329 => 134,  324 => 131,  305 => 115,  299 => 114,  296 => 113,  294 => 112,  288 => 109,  278 => 106,  274 => 104,  264 => 102,  254 => 100,  252 => 99,  244 => 98,  239 => 96,  226 => 86,  221 => 84,  216 => 82,  209 => 78,  206 => 77,  203 => 76,  200 => 75,  198 => 74,  195 => 73,  192 => 72,  189 => 71,  187 => 70,  184 => 69,  182 => 68,  179 => 67,  170 => 65,  166 => 64,  163 => 63,  154 => 61,  150 => 60,  136 => 48,  132 => 46,  130 => 45,  116 => 33,  105 => 31,  101 => 30,  98 => 29,  85 => 27,  81 => 26,  78 => 25,  72 => 23,  70 => 22,  67 => 21,  61 => 19,  59 => 18,  54 => 16,  49 => 14,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }} " lang="{{ lang }} " class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }} " lang="{{ lang }} " class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }} " lang="{{ lang }} ">*/
/* <!--<![endif]-->*/
/* */
/* <head>*/
/* 	<meta charset="UTF-8" />*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1">*/
/* 	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">*/
/* */
/* 	<title>{{ title }} </title>*/
/* */
/* 	<base href="{{ base }}" />*/
/* */
/* 	{% if description %}*/
/* 	<meta name="description" content="{{ description }}" />*/
/* 	{% endif %}*/
/* */
/* 	{% if keywords %}*/
/* 	<meta name="keywords" content="{{ keywords }}" />*/
/* 	{% endif %}*/
/* */
/* 	{% for style in styles %}*/
/* 	<link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* 	{% endfor %}*/
/* */
/* 	{% for link in links %}*/
/* 	<link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* 	{% endfor %}*/
/* */
/* 	<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>*/
/* 	<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />*/
/* 	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>*/
/* 	<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/* */
/* 	<link href="catalog/view/theme/clearshop/stylesheet/jasny-bootstrap.min.css" rel="stylesheet" media="screen" />*/
/* 	<script src="catalog/view/theme/clearshop/js/jasny-bootstrap.min.js" type="text/javascript"></script>*/
/* */
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/theme/clearshop/stylesheet/stylesheet.css"/>*/
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/theme/clearshop/stylesheet/stylesheet-responsive.css" media="screen"/>*/
/* */
/* 	{% if direction == 'rtl' %}*/
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/theme/clearshop/stylesheet/stylesheet.rtl.css" media="screen" />*/
/* 	{% endif %}*/
/* */
/* 	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>*/
/* */
/* 	<script type="text/javascript" src="catalog/view/theme/clearshop/js/jquery.magnific-popup.min.js"></script>*/
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/theme/clearshop/stylesheet/magnific-popup.css" media="screen" />*/
/* */
/* 	<script src="catalog/view/javascript/common.js" type="text/javascript"></script>*/
/* */
/* 	<script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>*/
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" media="screen"/>*/
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" media="screen"/>*/
/* */
/* 	{% for script in scripts %}*/
/* 	<script src="{{ script }}" type="text/javascript"></script>*/
/* 	{% endfor %}*/
/* */
/* 	{% for analytic in analytics %}*/
/* 	{{ analytic }}*/
/* 	{% endfor %}*/
/* */
/* 	{% set base_class = '' %}*/
/* */
/* 	{% if clearshop_skins != '' %}*/
/* 		{% set base_class = clearshop_skins ~ '-skin' %}*/
/* 	{% endif %}*/
/* */
/* 	{% if body_classes %}*/
/* 		{% set base_class = base_class ~ body_classes %}*/
/* 	{% endif %}*/
/* */
/* 	{{ include('clearshop/template/common/custom_styles.twig') }}*/
/* */
/* </head>*/
/* */
/* {% set lang = language %} */
/* */
/* <body class="{{ base_class }} ">*/
/* */
/* {{ include('clearshop/template/common/menumob.twig') }}*/
/* */
/* <div id="supercontainer" class="topcanvas">*/
/* */
/* 	<div id="thickbar"></div>*/
/* */
/* 	<header id="header">*/
/* */
/* 		<div class="container">*/
/* */
/* 			<div class="row {{ clearshop_logo_position }}">*/
/* */
/* 				<div id="logo" class="col-sm-4 col-xs-6 {% if clearshop_logo_position == 'center' %} col-md-4 col-sm-push-4 {% else %} col-md-3 {% endif %}">*/
/* 					{% if logo %}*/
/* 						<a href="{{ home }}" class="logo"><img src="{{ logo }} " alt="{{ name }} " class="img-responsive" /></a>*/
/* 					{% else %}*/
/* 						<a href="{{ home }}" class="logo"><img src="catalog/view/theme/clearshop/image/logo.png" title="{{ name }}" alt="{{ name }}" class="img-responsive" /></a>*/
/* 					{% endif %}*/
/* 				</div> <!-- #logo -->*/
/* */
/* 				<div class="col-xs-3 leftbox {% if clearshop_logo_position == 'center' %} col-sm-4 col-sm-pull-4 {% else %} col-md-6 col-sm-5 {% endif %}">*/
/* */
/* 					<div class="search-area">*/
/* 						{{ search }}*/
/* 					</div>*/
/* */
/* 					{% if clearshop_header_info_text|striptags != '' %}*/
/* 											*/
/* 						<div class="header-info owl-carousel {% if clearshop_logo_position == 'center' %} hidden-sm {% endif %} hidden-xs">*/
/* 							{{ clearshop_header_info_text }}*/
/* 						</div>*/
/* */
/* 						<script type="text/javascript">*/
/* 						$('.header-info').owlCarousel({*/
/* 							items:6,*/
/* 							singleItem: true,*/
/* 							loop: true,*/
/* 							autoPlay: 3000,*/
/* 							navigation: false,*/
/* 							pagination: false,*/
/* 							transitionStyle: "fade"*/
/* 						});*/
/* 						</script>*/
/* */
/* 					{% endif %}*/
/* */
/* 				</div> <!-- .middlebox -->*/
/* */
/* 				<div class="col-xs-6 rightbox {% if clearshop_logo_position == 'center' %} col-sm-4 {% else %} col-sm-3 {% endif %}">*/
/* */
/* 					<button type="button" class="btn btn-menu visible-sm-inline visible-xs-inline" onclick="$('.topcanvas').addClass('canvas-slid');" data-toggle="offcanvas" data-target="#mobmenu" data-disable-scrolling="true" data-modal="true" ><i class="fa fa-bars"></i></button>*/
/* */
/* 					<button type="button" class="search-trigger btn btn-link btn-header visible-xs-inline">*/
/* 						<i class="fa fa-search"></i>*/
/* 					</button>*/
/* */
/* 					<div id="top-links" class="btn-group dropdown hidden-sm hidden-xs">*/
/* 						<a href="{{ account }} " title="{{ text_account }} " class="btn btn-link dropdown-toggle myaccount" data-toggle="dropdown"><i class="fa fa-user-circle-o"></i></a>*/
/* 						<ul class="dropdown-menu dropdown-menu-right">*/
/*                             {% if logged %}*/
/* 								<li><a href="{{ account }} ">{{ text_account }} </a></li>*/
/* 								<li><a href="{{ order }} ">{{ text_order }} </a></li>*/
/* 								<li><a href="{{ transaction }} ">{{ text_transaction }} </a></li>*/
/* 								<li><a href="{{ download }} ">{{ text_download }} </a></li>*/
/* 								<li><a href="{{ logout }} "><i class="fa fa-sign-out"></i> {{ text_logout }} </a></li>*/
/*                             {% else %}*/
/* 								<li><a href="{{ register }} ">{{ text_register }} </a></li>*/
/* 								<li><a href="{{ login }} ">{{ text_login }} </a></li>*/
/*                             {% endif %}*/
/* 							<li class="divider wishlist"></li>*/
/* 							<li><a href="{{ wishlist }} " id="wishlist-total" title="{{ text_wishlist }} " class="wishlist"><i class="fa fa-heart-o"></i> <span>{{ text_wishlist }} </a></span></li>*/
/* 						</ul>*/
/* 					</div>*/
/* */
/* 					<div class="btn-group dropdown site-settings hidden-sm hidden-xs">*/
/* 						<a href="{{ account }} " title="{{ text_account }} " class="btn btn-link dropdown-toggle myaccount" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>*/
/* 						<ul class="dropdown-menu dropdown-menu-right">*/
/* 							<li>{{ lang }}</li>*/
/* 							<li>{{ currency }}</li>*/
/* 						</ul>*/
/* 					</div>*/
/* */
/* 					{{ cart }}*/
/* 					*/
/* 				</div>*/
/* 				*/
/* 			</div> <!-- .leftlogo -->*/
/* */
/* 		</div>*/
/* */
/* 		{{ include('clearshop/template/common/menudesk.twig') }}*/
/* */
/* 		{% if clearshop_sticky_menu == 1 %}*/
/* */
/* 		<script type="text/javascript">*/
/* 			var startPosition = 101;*/
/* 			var contentMargin =  28;*/
/* */
/* 			$(window).scroll(function() {*/
/* 				if($(window).scrollTop() > startPosition) {*/
/* 					width = $('.container #menu').width();*/
/* 					height = $('.container #menu').height();*/
/* 					$('#menu .navbar').addClass('navbar-fixed-top')*/
/* 					$('#menu .navcontainer').addClass('container')*/
/* 					$('#cart').addClass('fixed')*/
/* 				} else {*/
/* 					$('#menu .navbar').removeClass('navbar-fixed-top');*/
/* 					$('#menu .navcontainer').removeClass('container')*/
/* 					$('#cart').removeClass('fixed');*/
/* 				}*/
/* 			}); */
/* 		</script>*/
/* */
/* 		{% endif %}*/
/* */
/* 	</header> <!-- #header -->*/
/* */
/* 	<div id="content-wrapper">*/
/* 			*/
