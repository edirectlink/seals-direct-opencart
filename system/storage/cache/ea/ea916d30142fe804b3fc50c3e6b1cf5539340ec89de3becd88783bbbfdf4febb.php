<?php

/* clearshop/template/common/menu.twig */
class __TwigTemplate_f60f8ce418995c02fcbf087a49f9e2325c12817855267cfc3ff4c9b605f1a113 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!-- Category Links -->

";
        // line 4
        $context["linkidcount"] = 0;
        // line 5
        echo "
\t";
        // line 6
        if (((isset($context["clearshop_menu_categories"]) ? $context["clearshop_menu_categories"] : null) == "inline")) {
            // line 7
            echo "
\t\t";
            // line 8
            if ((isset($context["categories"]) ? $context["categories"] : null)) {
                // line 9
                echo "
\t\t\t";
                // line 10
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 11
                    echo "
\t\t\t\t";
                    // line 12
                    if ($this->getAttribute($context["category"], "children", array())) {
                        // line 13
                        echo "
\t\t\t\t\t<li id=\"m";
                        // line 14
                        echo (isset($context["linkidcount"]) ? $context["linkidcount"] : null);
                        echo "\" class=\"dropdown\" ><a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo " <span class=\"caret\"></span></a>
\t\t\t\t\t\t
\t\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t\t";
                        // line 17
                        if (((isset($context["clearshop_menu_category_icons"]) ? $context["clearshop_menu_category_icons"] : null) == 1)) {
                            // line 18
                            echo "\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                            // line 19
                            echo $this->getAttribute($context["category"], "href", array());
                            echo "\"><img src=\"";
                            echo $this->getAttribute($context["category"], "thumb", array());
                            echo "\" title=\"";
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "\" alt=\"";
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "\" /></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                        }
                        // line 22
                        echo "
\t\t\t\t\t\t\t<li>

\t\t\t\t\t\t\t\t";
                        // line 25
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), (twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array())) / twig_round($this->getAttribute($context["category"], "column", array()), 1, "ceil"))));
                        foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                            // line 26
                            echo "
\t\t\t\t\t\t\t  <ul class=\"list-unstyled column-menu\">
\t\t\t\t\t\t\t    
\t\t\t\t\t\t\t    ";
                            // line 29
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable($context["children"]);
                            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                // line 30
                                echo "\t\t\t\t\t\t\t    \t
\t\t\t\t\t\t\t    \t";
                                // line 31
                                if (($this->getAttribute($context["child"], "children_level_2", array()) && ((isset($context["clearshop_3rd_level_cat"]) ? $context["clearshop_3rd_level_cat"] : null) == 1))) {
                                    // line 32
                                    echo "\t\t\t\t\t\t\t    \t\t
\t\t\t\t\t\t\t    \t\t<li class=\"dropdown-submenu\"><a href=\"";
                                    // line 33
                                    echo $this->getAttribute($context["child"], "href", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["child"], "name", array());
                                    echo "</a>
\t\t\t\t\t\t\t    \t\t\t
\t\t\t\t\t\t\t    \t\t\t<ul class=\"dropdown-menu thirdmenu\">
\t\t\t\t\t\t\t    \t\t\t\t
\t\t\t\t\t\t\t    \t\t\t\t";
                                    // line 37
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                    foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                        // line 38
                                        echo "\t\t\t\t\t\t    \t\t\t\t  \t<li><a href=\"";
                                        echo $this->getAttribute($context["child2"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["child2"], "name", array());
                                        echo "</a></li>
\t\t\t\t\t\t    \t\t\t\t\t";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 40
                                    echo "
\t\t\t\t\t\t    \t\t\t  </ul>

\t\t\t\t\t\t    \t\t\t</li>

\t\t\t\t\t\t\t    \t";
                                } else {
                                    // line 46
                                    echo "\t\t\t\t\t\t\t    \t\t
\t\t\t\t\t\t\t    \t\t<li><a href=\"";
                                    // line 47
                                    echo $this->getAttribute($context["child"], "href", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["child"], "name", array());
                                    echo "</a></li>

\t\t\t\t\t\t\t    \t";
                                }
                                // line 50
                                echo "
\t\t\t\t\t\t\t    ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 52
                            echo "\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t  </ul>

\t\t\t\t\t\t\t  ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 56
                        echo "
\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t<li class=\"viewall\"><a href=\"";
                        // line 59
                        echo $this->getAttribute($context["category"], "href", array());
                        echo "\">";
                        echo (isset($context["text_all"]) ? $context["text_all"] : null);
                        echo " ";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "</a></li>

\t\t\t\t\t\t</ul>

\t\t\t\t\t</li>

\t\t\t\t";
                    } else {
                        // line 66
                        echo "\t\t\t\t
\t\t\t\t\t<li id=\"m";
                        // line 67
                        echo (isset($context["linkidcount"]) ? $context["linkidcount"] : null);
                        echo "\"><a href=\"";
                        echo $this->getAttribute($context["category"], "href", array());
                        echo "\">";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "</a></li>

\t\t\t\t";
                    }
                    // line 70
                    echo "
\t\t\t\t";
                    // line 71
                    $context["linkidcount"] = ((isset($context["linkidcount"]) ? $context["linkidcount"] : null) + 1);
                    // line 72
                    echo "
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 73
                echo " ";
                // line 74
                echo "
\t\t";
            }
            // line 76
            echo "
\t";
        } elseif ((        // line 77
(isset($context["clearshop_menu_categories"]) ? $context["clearshop_menu_categories"] : null) == "table")) {
            // line 78
            echo "
\t\t<li class=\"dropdown\">
\t\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">";
            // line 80
            echo (($this->getAttribute((isset($context["clearshop_categories_top_title"]) ? $context["clearshop_categories_top_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) ? ($this->getAttribute((isset($context["clearshop_categories_top_title"]) ? $context["clearshop_categories_top_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) : ((isset($context["text_menu_categories"]) ? $context["text_menu_categories"] : null)));
            echo " <span class=\"label\" style=\"background:";
            echo (isset($context["clearshop_categories_tag_color"]) ? $context["clearshop_categories_tag_color"] : null);
            echo "\">";
            echo (($this->getAttribute((isset($context["clearshop_categories_tag"]) ? $context["clearshop_categories_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) ? ($this->getAttribute((isset($context["clearshop_categories_tag"]) ? $context["clearshop_categories_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) : (""));
            echo "</span> <span class=\"caret\"></span></a>
\t\t\t<ul class=\"dropdown-menu cols-";
            // line 81
            echo (isset($context["clearshop_menu_categories_x_row"]) ? $context["clearshop_menu_categories_x_row"] : null);
            echo "\"> 

\t\t\t\t";
            // line 83
            if ((isset($context["categories"]) ? $context["categories"] : null)) {
                // line 84
                echo "
\t\t\t\t\t";
                // line 85
                $context["count"] = 0;
                // line 86
                echo "
\t\t\t\t\t";
                // line 87
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 88
                    echo "
\t\t\t\t\t\t";
                    // line 89
                    if ((((isset($context["count"]) ? $context["count"] : null) > 0) && (((isset($context["count"]) ? $context["count"] : null) % (isset($context["clearshop_menu_categories_x_row"]) ? $context["clearshop_menu_categories_x_row"] : null)) == 0))) {
                        // line 90
                        echo "\t\t\t\t\t\t\t<li class=\"clearfix\"></li>
\t\t\t\t\t\t";
                    }
                    // line 92
                    echo "
\t\t\t\t\t\t<li class=\"column-item\">

\t\t\t\t\t\t\t";
                    // line 95
                    if (((isset($context["clearshop_menu_category_icons"]) ? $context["clearshop_menu_category_icons"] : null) == 1)) {
                        // line 96
                        echo "\t\t\t\t\t\t\t\t<div class=\"image\">
\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 97
                        echo $this->getAttribute($context["category"], "href", array());
                        echo "\"><img src=\"";
                        echo $this->getAttribute($context["category"], "thumb", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "\" alt=\"";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "\" /></a>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
                    }
                    // line 100
                    echo "
\t\t\t\t\t\t\t<span class=\"maincat\"><a href=\"";
                    // line 101
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></span>

\t\t\t\t\t\t\t";
                    // line 103
                    if ($this->getAttribute($context["category"], "children", array())) {
                        // line 104
                        echo "\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t<ul>

\t\t\t\t\t\t\t\t\t<li class=\"column-menu\">

\t\t\t\t\t\t\t\t\t\t<ul class=\"list-unstyled\">

\t\t\t\t\t\t\t\t\t    ";
                        // line 111
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 112
                            echo "\t\t\t\t\t\t\t\t\t    \t
\t\t\t\t\t\t\t\t\t    \t";
                            // line 113
                            if (($this->getAttribute($context["child"], "children_level_2", array()) && ((isset($context["clearshop_3rd_level_cat"]) ? $context["clearshop_3rd_level_cat"] : null) == 1))) {
                                // line 114
                                echo "\t\t\t\t\t\t\t\t\t    \t\t
\t\t\t\t\t\t\t\t\t    \t\t<li class=\"dropdown-submenu\"><a href=\"";
                                // line 115
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "</a>
\t\t\t\t\t\t\t\t\t    \t\t\t
\t\t\t\t\t\t\t\t\t    \t\t\t<ul class=\"dropdown-menu thirdmenu\">
\t\t\t\t\t\t\t\t\t    \t\t\t\t
\t\t\t\t\t\t\t\t\t    \t\t\t\t";
                                // line 119
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                    // line 120
                                    echo "\t\t\t\t\t\t\t\t    \t\t\t\t  \t<li><a href=\"";
                                    echo $this->getAttribute($context["child2"], "href", array());
                                    echo "\">";
                                    echo $this->getAttribute($context["child2"], "name", array());
                                    echo "</a></li>
\t\t\t\t\t\t\t\t    \t\t\t\t\t";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 122
                                echo "
\t\t\t\t\t\t\t\t    \t\t\t  </ul>

\t\t\t\t\t\t\t\t    \t\t\t</li>

\t\t\t\t\t\t\t\t\t    \t";
                            } else {
                                // line 128
                                echo "\t\t\t\t\t\t\t\t\t    \t\t
\t\t\t\t\t\t\t\t\t    \t\t<li><a href=\"";
                                // line 129
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "</a></li>

\t\t\t\t\t\t\t\t\t    \t";
                            }
                            // line 132
                            echo "
\t\t\t\t\t\t\t\t\t    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 134
                        echo "
\t\t\t\t\t\t\t\t\t  </ul>

\t\t\t\t\t\t\t\t\t</li>

\t\t\t\t\t\t\t\t</ul>
\t
\t\t\t\t\t\t\t";
                    }
                    // line 142
                    echo "
\t\t\t\t\t\t</li>

\t\t\t\t\t\t";
                    // line 145
                    $context["linkidcount"] = ((isset($context["linkidcount"]) ? $context["linkidcount"] : null) + 1);
                    // line 146
                    echo "
\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 147
                echo " ";
                // line 148
                echo "
\t\t\t\t";
            }
            // line 150
            echo "
\t\t\t</ul>

\t\t</li>

\t";
        }
        // line 156
        echo "
<!-- Brands -->

";
        // line 159
        if (((isset($context["clearshop_menu_brands"]) ? $context["clearshop_menu_brands"] : null) != "hidden")) {
            // line 160
            echo "
\t<li class=\"dropdown brands\">
\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">";
            // line 162
            echo (($this->getAttribute((isset($context["clearshop_brands_top_title"]) ? $context["clearshop_brands_top_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) ? ($this->getAttribute((isset($context["clearshop_brands_top_title"]) ? $context["clearshop_brands_top_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) : ((isset($context["text_menu_brands"]) ? $context["text_menu_brands"] : null)));
            echo " <span class=\"label\" style=\"background:";
            echo (isset($context["clearshop_brands_tag_color"]) ? $context["clearshop_brands_tag_color"] : null);
            echo "\">";
            echo (($this->getAttribute((isset($context["clearshop_brands_tag"]) ? $context["clearshop_brands_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) ? ($this->getAttribute((isset($context["clearshop_brands_tag"]) ? $context["clearshop_brands_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) : (""));
            echo "</span> <span class=\"caret\"></span></a>

\t\t<ul class=\"dropdown-menu cols-";
            // line 164
            echo (isset($context["clearshop_menu_brands_x_row"]) ? $context["clearshop_menu_brands_x_row"] : null);
            echo "\"> 

\t\t\t";
            // line 166
            if (((isset($context["manufacturers"]) ? $context["manufacturers"] : null) && ((isset($context["manufacturers"]) ? $context["manufacturers"] : null) != ""))) {
                // line 167
                echo "\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["manufacturers"]) ? $context["manufacturers"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                    // line 168
                    echo "\t\t\t\t<li class=\"column-item ";
                    echo (isset($context["clearshop_menu_brands"]) ? $context["clearshop_menu_brands"] : null);
                    echo "\">
\t\t\t\t\t<a href=\"";
                    // line 169
                    echo $this->getAttribute($context["manufacturer"], "href", array());
                    echo "\">
\t\t\t\t\t<span class=\"image\"><img src=\"";
                    // line 170
                    echo $this->getAttribute($context["manufacturer"], "image", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["manufacturer"], "name", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["manufacturer"], "name", array());
                    echo "\" /></span>
\t\t\t\t\t<span class=\"name\">";
                    // line 171
                    echo $this->getAttribute($context["manufacturer"], "name", array());
                    echo "</span></a>
\t\t\t\t</li>
\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 174
                echo "\t\t\t";
            }
            // line 175
            echo "\t\t</ul>  
\t</li>

";
        }
        // line 179
        echo "
<!-- Menu block contents -->

";
        // line 182
        if ((isset($context["clearshop_menu_blocks"]) ? $context["clearshop_menu_blocks"] : null)) {
            // line 183
            echo "
\t";
            // line 184
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_menu_blocks"]) ? $context["clearshop_menu_blocks"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["menu_block"]) {
                // line 185
                echo "
\t\t";
                // line 186
                if ((($this->getAttribute($context["menu_block"], "status", array()) == 1) && ($this->getAttribute($context["menu_block"], "visibility", array()) != "desktop"))) {
                    // line 187
                    echo "
\t\t\t<li id=\"menu_block";
                    // line 188
                    echo $this->getAttribute($context["menu_block"], "id", array());
                    echo "\" class=\"menu_block dropdown\"><a class=\"dropdown-toggle\" ";
                    if ((($this->getAttribute($context["menu_block"], "content", array()) || ($this->getAttribute($context["menu_block"], "content", array()) == "")) && ($this->getAttribute($context["menu_block"], "url", array()) != ""))) {
                        echo " href=\"";
                        echo $this->getAttribute($context["menu_block"], "url", array());
                        echo "\" ";
                    } else {
                        echo " data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\" ";
                    }
                    echo ">";
                    echo (($this->getAttribute($context["menu_block"], "title", array())) ? ($this->getAttribute($context["menu_block"], "title", array())) : (""));
                    echo " ";
                    if (($this->getAttribute($context["menu_block"], "tag", array()) != "")) {
                        echo " <span class=\"label\" style=\"background:";
                        echo $this->getAttribute($context["menu_block"], "tagcolor", array());
                        echo "\">";
                        echo $this->getAttribute($context["menu_block"], "tag", array());
                        echo "</span>";
                    }
                    echo " <span class=\"caret\"></span></a>

\t\t\t\t";
                    // line 190
                    if (($this->getAttribute($context["menu_block"], "content", array()) && ($this->getAttribute($context["menu_block"], "content", array()) != ""))) {
                        // line 191
                        echo "
\t\t\t\t\t<ul class=\"dropdown-menu\">
\t\t\t\t\t\t<li>
\t\t\t\t\t\t
\t\t\t\t\t\t";
                        // line 195
                        if (($this->getAttribute($context["menu_block"], "url", array()) != "")) {
                            // line 196
                            echo "\t\t\t\t\t\t\t<p>Go to <a href=\"";
                            echo $this->getAttribute($context["menu_block"], "url", array());
                            echo "\">";
                            echo (($this->getAttribute($context["menu_block"], "title", array())) ? ($this->getAttribute($context["menu_block"], "title", array())) : (""));
                            echo "</a></p>
\t\t\t\t\t\t";
                        }
                        // line 198
                        echo "
\t\t\t\t\t\t";
                        // line 199
                        echo $this->getAttribute($context["menu_block"], "content", array());
                        echo "

\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>

\t\t\t\t";
                    }
                    // line 205
                    echo "
\t\t\t</li>

\t\t";
                }
                // line 209
                echo "
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_block'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 211
            echo "
";
        }
        // line 213
        echo "
<!-- Information pages -->

";
        // line 216
        if (((isset($context["clearshop_menu_infopages"]) ? $context["clearshop_menu_infopages"] : null) == "inline")) {
            // line 217
            echo "
\t";
            // line 218
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 219
                echo "\t\t<li><a href=\"";
                echo $this->getAttribute($context["information"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a></li>
\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 221
            echo "
";
        } elseif ((        // line 222
(isset($context["clearshop_menu_infopages"]) ? $context["clearshop_menu_infopages"] : null) == "vertical")) {
            // line 223
            echo "
\t<li class=\"dropdown information-pages\">
\t\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">";
            // line 225
            echo (($this->getAttribute((isset($context["clearshop_infopages_top_title"]) ? $context["clearshop_infopages_top_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) ? ($this->getAttribute((isset($context["clearshop_infopages_top_title"]) ? $context["clearshop_infopages_top_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) : ((isset($context["text_menu_information"]) ? $context["text_menu_information"] : null)));
            echo " <span class=\"label\" style=\"background: ";
            echo (isset($context["clearshop_infopages_tag_color"]) ? $context["clearshop_infopages_tag_color"] : null);
            echo "\">";
            echo (($this->getAttribute((isset($context["clearshop_infopages_tag"]) ? $context["clearshop_infopages_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) ? ($this->getAttribute((isset($context["clearshop_infopages_tag"]) ? $context["clearshop_infopages_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array")) : (""));
            echo "</span> <span class=\"caret\"></span></a>
\t\t<ul class=\"dropdown-menu\">
\t\t\t";
            // line 227
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                // line 228
                echo "\t\t\t\t<li><a href=\"";
                echo $this->getAttribute($context["information"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["information"], "title", array());
                echo "</a></li>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 230
            echo "\t\t</ul>
\t</li>

";
        }
        // line 234
        echo "
<!-- Custom links -->

";
        // line 237
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_menu_link"]) ? $context["clearshop_menu_link"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["menu_link"]) {
            // line 238
            echo "
\t";
            // line 239
            if ((($this->getAttribute($context["menu_link"], "status", array()) == 1) && ($this->getAttribute($this->getAttribute($context["menu_link"], (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array"), "url", array()) != ""))) {
                // line 240
                echo "\t\t<li>
\t\t\t<a href=\"";
                // line 241
                echo $this->getAttribute($this->getAttribute($context["menu_link"], (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array"), "url", array());
                echo "\" target=\"";
                echo $this->getAttribute($context["menu_link"], "target", array());
                echo "\">";
                echo $this->getAttribute($this->getAttribute($context["menu_link"], (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array"), "title", array());
                echo "</a>
\t\t</li>
\t";
            }
            // line 244
            echo "\t
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 246
        echo "
<!-- Custom dropdown links -->

";
        // line 249
        if (((isset($context["clearshop_menu_dropdown_status"]) ? $context["clearshop_menu_dropdown_status"] : null) == 1)) {
            // line 250
            echo "
\t<li class=\"dropdown\">
\t<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">";
            // line 252
            echo $this->getAttribute((isset($context["clearshop_menu_dropdown_title"]) ? $context["clearshop_menu_dropdown_title"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array");
            echo " <span class=\"label\" style=\"background:";
            echo (isset($context["clearshop_menu_dropdown_tag_color"]) ? $context["clearshop_menu_dropdown_tag_color"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["clearshop_menu_dropdown_tag"]) ? $context["clearshop_menu_dropdown_tag"] : null), (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array");
            echo "</span></a>
\t\t<ul class=\"dropdown-menu\">
\t\t\t";
            // line 254
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_menu_dropdown"]) ? $context["clearshop_menu_dropdown"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["menu_drop"]) {
                // line 255
                echo "\t\t\t\t";
                if ((($this->getAttribute($context["menu_drop"], "status", array()) == 1) && ($this->getAttribute($context["menu_drop"], "url", array()) != ""))) {
                    // line 256
                    echo "\t\t\t\t\t<li>
\t\t\t\t\t\t<a href=\"";
                    // line 257
                    echo $this->getAttribute($context["menu_drop"], "url", array());
                    echo "\" target=\"";
                    echo $this->getAttribute($context["menu_drop"], "target", array());
                    echo "\">";
                    echo $this->getAttribute($this->getAttribute($context["menu_drop"], (isset($context["language_id"]) ? $context["language_id"] : null), array(), "array"), "title", array());
                    echo "</a>
\t\t\t\t\t</li>
\t\t\t\t";
                }
                // line 260
                echo "\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu_drop'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 261
            echo "\t\t</ul>
\t</li>

";
        }
        // line 265
        echo "
";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/menu.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  708 => 265,  702 => 261,  696 => 260,  686 => 257,  683 => 256,  680 => 255,  676 => 254,  667 => 252,  663 => 250,  661 => 249,  656 => 246,  649 => 244,  639 => 241,  636 => 240,  634 => 239,  631 => 238,  627 => 237,  622 => 234,  616 => 230,  605 => 228,  601 => 227,  592 => 225,  588 => 223,  586 => 222,  583 => 221,  572 => 219,  568 => 218,  565 => 217,  563 => 216,  558 => 213,  554 => 211,  547 => 209,  541 => 205,  532 => 199,  529 => 198,  521 => 196,  519 => 195,  513 => 191,  511 => 190,  488 => 188,  485 => 187,  483 => 186,  480 => 185,  476 => 184,  473 => 183,  471 => 182,  466 => 179,  460 => 175,  457 => 174,  448 => 171,  440 => 170,  436 => 169,  431 => 168,  426 => 167,  424 => 166,  419 => 164,  410 => 162,  406 => 160,  404 => 159,  399 => 156,  391 => 150,  387 => 148,  385 => 147,  378 => 146,  376 => 145,  371 => 142,  361 => 134,  354 => 132,  346 => 129,  343 => 128,  335 => 122,  324 => 120,  320 => 119,  311 => 115,  308 => 114,  306 => 113,  303 => 112,  299 => 111,  290 => 104,  288 => 103,  281 => 101,  278 => 100,  266 => 97,  263 => 96,  261 => 95,  256 => 92,  252 => 90,  250 => 89,  247 => 88,  243 => 87,  240 => 86,  238 => 85,  235 => 84,  233 => 83,  228 => 81,  220 => 80,  216 => 78,  214 => 77,  211 => 76,  207 => 74,  205 => 73,  198 => 72,  196 => 71,  193 => 70,  183 => 67,  180 => 66,  166 => 59,  161 => 56,  152 => 52,  145 => 50,  137 => 47,  134 => 46,  126 => 40,  115 => 38,  111 => 37,  102 => 33,  99 => 32,  97 => 31,  94 => 30,  90 => 29,  85 => 26,  81 => 25,  76 => 22,  64 => 19,  61 => 18,  59 => 17,  51 => 14,  48 => 13,  46 => 12,  43 => 11,  39 => 10,  36 => 9,  34 => 8,  31 => 7,  29 => 6,  26 => 5,  24 => 4,  19 => 1,);
    }
}
/* */
/* <!-- Category Links -->*/
/* */
/* {% set linkidcount = 0 %}*/
/* */
/* 	{% if clearshop_menu_categories == 'inline' %}*/
/* */
/* 		{% if categories %}*/
/* */
/* 			{% for category in categories %}*/
/* */
/* 				{% if category.children %}*/
/* */
/* 					<li id="m{{ linkidcount }}" class="dropdown" ><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ category.name }} <span class="caret"></span></a>*/
/* 						*/
/* 						<ul class="dropdown-menu">*/
/* 							{% if clearshop_menu_category_icons == 1 %}*/
/* 								<div class="image">*/
/* 									<a href="{{ category.href }}"><img src="{{ category.thumb }}" title="{{ category.name }}" alt="{{ category.name }}" /></a>*/
/* 								</div>*/
/* 							{% endif %}*/
/* */
/* 							<li>*/
/* */
/* 								{% for children in category.children|batch(category.children|length / category.column|round(1, 'ceil')) %}*/
/* */
/* 							  <ul class="list-unstyled column-menu">*/
/* 							    */
/* 							    {% for child in children %}*/
/* 							    	*/
/* 							    	{% if child.children_level_2 and clearshop_3rd_level_cat == 1 %}*/
/* 							    		*/
/* 							    		<li class="dropdown-submenu"><a href="{{ child.href }}">{{ child.name }}</a>*/
/* 							    			*/
/* 							    			<ul class="dropdown-menu thirdmenu">*/
/* 							    				*/
/* 							    				{% for child2 in child.children_level_2 %}*/
/* 						    				  	<li><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/* 						    					{% endfor %}*/
/* */
/* 						    			  </ul>*/
/* */
/* 						    			</li>*/
/* */
/* 							    	{% else %}*/
/* 							    		*/
/* 							    		<li><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/* */
/* 							    	{% endif %}*/
/* */
/* 							    {% endfor %}*/
/* 							  */
/* 							  </ul>*/
/* */
/* 							  {% endfor %}*/
/* */
/* 							</li>*/
/* */
/* 							<li class="viewall"><a href="{{ category.href }}">{{ text_all }} {{ category.name }}</a></li>*/
/* */
/* 						</ul>*/
/* */
/* 					</li>*/
/* */
/* 				{% else %}*/
/* 				*/
/* 					<li id="m{{ linkidcount }}"><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/* */
/* 				{% endif %}*/
/* */
/* 				{% set linkidcount = linkidcount + 1 %}*/
/* */
/* 			{% endfor %} {# for category in categories #}*/
/* */
/* 		{% endif %}*/
/* */
/* 	{% elseif clearshop_menu_categories == 'table' %}*/
/* */
/* 		<li class="dropdown">*/
/* 			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ clearshop_categories_top_title[language_id] ? clearshop_categories_top_title[language_id] : text_menu_categories }} <span class="label" style="background:{{ clearshop_categories_tag_color }}">{{ clearshop_categories_tag[language_id] ? clearshop_categories_tag[language_id] : '' }}</span> <span class="caret"></span></a>*/
/* 			<ul class="dropdown-menu cols-{{ clearshop_menu_categories_x_row }}"> */
/* */
/* 				{% if categories %}*/
/* */
/* 					{% set count = 0 %}*/
/* */
/* 					{% for category in categories %}*/
/* */
/* 						{% if count > 0 and (count % clearshop_menu_categories_x_row == 0) %}*/
/* 							<li class="clearfix"></li>*/
/* 						{% endif %}*/
/* */
/* 						<li class="column-item">*/
/* */
/* 							{% if clearshop_menu_category_icons == 1 %}*/
/* 								<div class="image">*/
/* 									<a href="{{ category.href }}"><img src="{{ category.thumb }}" title="{{ category.name }}" alt="{{ category.name }}" /></a>*/
/* 								</div>*/
/* 							{% endif %}*/
/* */
/* 							<span class="maincat"><a href="{{ category.href }}">{{ category.name }}</a></span>*/
/* */
/* 							{% if category.children %}*/
/* 								*/
/* 								<ul>*/
/* */
/* 									<li class="column-menu">*/
/* */
/* 										<ul class="list-unstyled">*/
/* */
/* 									    {% for child in category.children %}*/
/* 									    	*/
/* 									    	{% if child.children_level_2 and clearshop_3rd_level_cat == 1 %}*/
/* 									    		*/
/* 									    		<li class="dropdown-submenu"><a href="{{ child.href }}">{{ child.name }}</a>*/
/* 									    			*/
/* 									    			<ul class="dropdown-menu thirdmenu">*/
/* 									    				*/
/* 									    				{% for child2 in child.children_level_2 %}*/
/* 								    				  	<li><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/* 								    					{% endfor %}*/
/* */
/* 								    			  </ul>*/
/* */
/* 								    			</li>*/
/* */
/* 									    	{% else %}*/
/* 									    		*/
/* 									    		<li><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/* */
/* 									    	{% endif %}*/
/* */
/* 									    {% endfor %}*/
/* */
/* 									  </ul>*/
/* */
/* 									</li>*/
/* */
/* 								</ul>*/
/* 	*/
/* 							{% endif %}*/
/* */
/* 						</li>*/
/* */
/* 						{% set linkidcount = linkidcount + 1 %}*/
/* */
/* 					{% endfor %} {# for category in categories #}*/
/* */
/* 				{% endif %}*/
/* */
/* 			</ul>*/
/* */
/* 		</li>*/
/* */
/* 	{% endif %}*/
/* */
/* <!-- Brands -->*/
/* */
/* {% if clearshop_menu_brands != 'hidden' %}*/
/* */
/* 	<li class="dropdown brands">*/
/* 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{clearshop_brands_top_title[language_id] ? clearshop_brands_top_title[language_id] : text_menu_brands }} <span class="label" style="background:{{ clearshop_brands_tag_color }}">{{ clearshop_brands_tag[language_id] ? clearshop_brands_tag[language_id] : '' }}</span> <span class="caret"></span></a>*/
/* */
/* 		<ul class="dropdown-menu cols-{{ clearshop_menu_brands_x_row }}"> */
/* */
/* 			{% if manufacturers and manufacturers != '' %}*/
/* 				{% for manufacturer in manufacturers %}*/
/* 				<li class="column-item {{ clearshop_menu_brands }}">*/
/* 					<a href="{{ manufacturer.href }}">*/
/* 					<span class="image"><img src="{{ manufacturer.image }}" title="{{ manufacturer.name }}" alt="{{ manufacturer.name }}" /></span>*/
/* 					<span class="name">{{ manufacturer.name }}</span></a>*/
/* 				</li>*/
/* 				{% endfor %}*/
/* 			{% endif %}*/
/* 		</ul>  */
/* 	</li>*/
/* */
/* {% endif %}*/
/* */
/* <!-- Menu block contents -->*/
/* */
/* {% if clearshop_menu_blocks %}*/
/* */
/* 	{% for menu_block in clearshop_menu_blocks %}*/
/* */
/* 		{% if menu_block.status == 1 and menu_block.visibility != 'desktop' %}*/
/* */
/* 			<li id="menu_block{{ menu_block.id }}" class="menu_block dropdown"><a class="dropdown-toggle" {% if (menu_block.content or menu_block.content =='') and menu_block.url != '' %} href="{{ menu_block.url }}" {% else %} data-toggle="dropdown" role="button" aria-expanded="false" {% endif %}>{{ menu_block.title ? menu_block.title }} {% if menu_block.tag != '' %} <span class="label" style="background:{{ menu_block.tagcolor }}">{{ menu_block.tag }}</span>{% endif %} <span class="caret"></span></a>*/
/* */
/* 				{% if menu_block.content and menu_block.content !='' %}*/
/* */
/* 					<ul class="dropdown-menu">*/
/* 						<li>*/
/* 						*/
/* 						{% if menu_block.url != '' %}*/
/* 							<p>Go to <a href="{{ menu_block.url }}">{{ menu_block.title ? menu_block.title : '' }}</a></p>*/
/* 						{% endif %}*/
/* */
/* 						{{ menu_block.content }}*/
/* */
/* 						</li>*/
/* 					</ul>*/
/* */
/* 				{% endif %}*/
/* */
/* 			</li>*/
/* */
/* 		{% endif %}*/
/* */
/* 	{% endfor %}*/
/* */
/* {% endif %}*/
/* */
/* <!-- Information pages -->*/
/* */
/* {% if clearshop_menu_infopages == 'inline' %}*/
/* */
/* 	{% for information in informations %}*/
/* 		<li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/* 	{% endfor %}*/
/* */
/* {% elseif clearshop_menu_infopages == 'vertical' %}*/
/* */
/* 	<li class="dropdown information-pages">*/
/* 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{clearshop_infopages_top_title[language_id] ? clearshop_infopages_top_title[language_id] : text_menu_information }} <span class="label" style="background: {{ clearshop_infopages_tag_color }}">{{ clearshop_infopages_tag[language_id] ? clearshop_infopages_tag[language_id] : '' }}</span> <span class="caret"></span></a>*/
/* 		<ul class="dropdown-menu">*/
/* 			{% for information in informations %}*/
/* 				<li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/* 			{% endfor %}*/
/* 		</ul>*/
/* 	</li>*/
/* */
/* {% endif %}*/
/* */
/* <!-- Custom links -->*/
/* */
/* {% for menu_link in clearshop_menu_link %}*/
/* */
/* 	{% if menu_link.status == 1 and menu_link[language_id].url !='' %}*/
/* 		<li>*/
/* 			<a href="{{ menu_link[language_id].url }}" target="{{ menu_link.target }}">{{ menu_link[language_id].title }}</a>*/
/* 		</li>*/
/* 	{% endif %}*/
/* 	*/
/* {% endfor %}*/
/* */
/* <!-- Custom dropdown links -->*/
/* */
/* {% if clearshop_menu_dropdown_status == 1 %}*/
/* */
/* 	<li class="dropdown">*/
/* 	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ clearshop_menu_dropdown_title[language_id] }} <span class="label" style="background:{{ clearshop_menu_dropdown_tag_color }}">{{ clearshop_menu_dropdown_tag[language_id] }}</span></a>*/
/* 		<ul class="dropdown-menu">*/
/* 			{% for menu_drop in clearshop_menu_dropdown %}*/
/* 				{% if menu_drop.status == 1 and menu_drop.url !='' %}*/
/* 					<li>*/
/* 						<a href="{{ menu_drop.url }}" target="{{ menu_drop.target }}">{{ menu_drop[language_id].title }}</a>*/
/* 					</li>*/
/* 				{% endif %}*/
/* 			{% endfor %}*/
/* 		</ul>*/
/* 	</li>*/
/* */
/* {% endif %}*/
/* */
/* */
