<?php

/* clearshop/template/common/search.twig */
class __TwigTemplate_7af2b8b4b7252de91e0f3637f42c3f2e9ba343963d738ace3684cbf451358720 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"search\" class=\"search-field\">
\t<div class=\"input-group\">
\t  <input type=\"text\" name=\"search\" value=\"";
        // line 3
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "\" placeholder=\"";
        echo (isset($context["text_search"]) ? $context["text_search"] : null);
        echo "\" class=\"form-control\" />
\t  <span class=\"input-group-btn\">
\t    <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-search\"></i></button>
\t  </span>
\t  <div class=\"search-area-close\"><a href=\"#\">&times;</a></div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 3,  19 => 1,);
    }
}
/* <div id="search" class="search-field">*/
/* 	<div class="input-group">*/
/* 	  <input type="text" name="search" value="{{ search }}" placeholder="{{ text_search }}" class="form-control" />*/
/* 	  <span class="input-group-btn">*/
/* 	    <button type="button" class="btn btn-default"><i class="fa fa-search"></i></button>*/
/* 	  </span>*/
/* 	  <div class="search-area-close"><a href="#">&times;</a></div>*/
/* 	</div>*/
/* </div>*/
