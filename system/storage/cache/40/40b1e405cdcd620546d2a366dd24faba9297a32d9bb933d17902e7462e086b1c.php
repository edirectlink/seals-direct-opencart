<?php

/* clearshop/template/common/content_bottom.twig */
class __TwigTemplate_9b5ea51b5ca3de01909a70a32d4fdc305085ec4e9e8095be9cf929196683db8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["modules"]) ? $context["modules"] : null)) {
            // line 2
            echo "\t<div class=\"content-bottom\">
\t\t<div class=\"container\">
\t\t  ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 5
                echo "\t\t  \t";
                echo $context["module"];
                echo "
\t\t  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "\t  </div>
\t</div>
";
        }
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/content_bottom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 7,  29 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if modules %}*/
/* 	<div class="content-bottom">*/
/* 		<div class="container">*/
/* 		  {% for module in modules %}*/
/* 		  	{{ module }}*/
/* 		  {% endfor %}*/
/* 	  </div>*/
/* 	</div>*/
/* {% endif %}*/
