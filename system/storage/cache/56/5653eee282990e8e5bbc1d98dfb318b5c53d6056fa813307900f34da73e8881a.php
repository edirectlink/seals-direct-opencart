<?php

/* clearshop/template/common/column_left.twig */
class __TwigTemplate_f275707a900672066f331589c19653d603c1190525f0989f7edba2858516aa58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["modules"]) ? $context["modules"] : null)) {
            // line 2
            echo "\t<aside id=\"column-left\" class=\"col-sm-3 hidden-xs sidebar\">
\t\t<div class=\"inner\">
\t\t  ";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["modules"]) ? $context["modules"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["module"]) {
                // line 5
                echo "\t\t  \t";
                echo $context["module"];
                echo "
\t\t  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['module'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "\t\t</div>
\t</aside>
";
        }
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/column_left.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 7,  29 => 5,  25 => 4,  21 => 2,  19 => 1,);
    }
}
/* {% if modules %}*/
/* 	<aside id="column-left" class="col-sm-3 hidden-xs sidebar">*/
/* 		<div class="inner">*/
/* 		  {% for module in modules %}*/
/* 		  	{{ module }}*/
/* 		  {% endfor %}*/
/* 		</div>*/
/* 	</aside>*/
/* {% endif %}*/
