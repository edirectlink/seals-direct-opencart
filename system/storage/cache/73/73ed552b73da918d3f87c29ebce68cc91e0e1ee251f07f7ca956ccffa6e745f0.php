<?php

/* clearshop/template/common/custom_styles.twig */
class __TwigTemplate_72fb0c32252fe6e9784f43e7cdb76d86d0fa456aed83598e67ef8b393f2d0af1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["googlefonts"]) ? $context["googlefonts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["gfont"]) {
            // line 2
            echo "\t<link href='//fonts.googleapis.com/css?family=";
            echo $context["gfont"];
            echo ":400,300,200,700,800,500";
            echo (isset($context["cyrillic"]) ? $context["cyrillic"] : null);
            echo "&amp;v1' rel='stylesheet' type='text/css' />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['gfont'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4
        echo "
\t<style type=\"text/css\" media=\"screen\">

\t\tbody {
\t\t\t
\t\t";
        // line 9
        if (((isset($context["clearshop_custom_image"]) ? $context["clearshop_custom_image"] : null) != "")) {
            // line 10
            echo "\t\t\tbackground-image: url(\"";
            echo (isset($context["base"]) ? $context["base"] : null);
            echo (isset($context["clearshop_custom_image"]) ? $context["clearshop_custom_image"] : null);
            echo "\");
\t\t\tbackground-position: top center;
\t\t\tbackground-repeat: no-repeat;
\t\t";
        } elseif ((        // line 13
(isset($context["clearshop_custom_pattern"]) ? $context["clearshop_custom_pattern"] : null) != "")) {
            // line 14
            echo "\t\t\tbackground-image: url(\"";
            echo (isset($context["base"]) ? $context["base"] : null);
            echo " ";
            echo (isset($context["clearshop_custom_pattern"]) ? $context["clearshop_custom_pattern"] : null);
            echo "\");
\t\t";
        } elseif ((        // line 15
(isset($context["clearshop_pattern_overlay"]) ? $context["clearshop_pattern_overlay"] : null) != "none")) {
            // line 16
            echo "\t\t\tbackground-image: url(\"catalog/view/theme/clearshop/image/patterns/";
            echo (isset($context["clearshop_pattern_overlay"]) ? $context["clearshop_pattern_overlay"] : null);
            echo ".png\");
\t\t";
        } else {
            // line 18
            echo "\t\t\tbackground-image: none;
\t\t";
        }
        // line 20
        echo "
\t\t}

\t\t";
        // line 23
        if (((isset($context["clearshop_responsive_layout"]) ? $context["clearshop_responsive_layout"] : null) == "normal")) {
            // line 24
            echo "\t\t\tbody.responsive .container { max-width: 982px !important; }
\t\t";
        }
        // line 26
        echo "
\t\t";
        // line 27
        if (((isset($context["clearshop_custom_colors"]) ? $context["clearshop_custom_colors"] : null) == 1)) {
            // line 28
            echo "
\t\t\t";
            // line 29
            if (((isset($context["bodybg_color"]) ? $context["bodybg_color"] : null) != "")) {
                // line 30
                echo "\t\t\t\tbody.boxed-skin {
\t\t\t\t\tbackground-color: ";
                // line 31
                echo (isset($context["bodybg_color"]) ? $context["bodybg_color"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 34
            echo "\t\t\t";
            if (((isset($context["topbar_bg"]) ? $context["topbar_bg"] : null) != "")) {
                // line 35
                echo "\t\t\t\t#thickbar {
\t\t\t\t\tbackground: ";
                // line 36
                echo (isset($context["topbar_bg"]) ? $context["topbar_bg"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 39
            echo "\t\t\t";
            if (((isset($context["menu_color"]) ? $context["menu_color"] : null) != "")) {
                // line 40
                echo "\t\t\t\t.navbar .nav > li > a {
\t\t\t\t\tcolor: ";
                // line 41
                echo (isset($context["menu_color"]) ? $context["menu_color"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 44
            echo "\t\t\t";
            if (((isset($context["menu_hover"]) ? $context["menu_hover"] : null) != "")) {
                // line 45
                echo "\t\t\t\t.navbar .nav > li:hover > a, .navbar .nav > li > a:hover,
\t\t\t\t#menu.navbar-dark .navbar .nav > li > a:hover,
\t\t\t\t#menu.navbar-dark .navbar .nav > li:hover > a {
\t\t\t\t\tcolor: ";
                // line 48
                echo (isset($context["menu_hover"]) ? $context["menu_hover"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 51
            echo "\t\t\t";
            if (((isset($context["dropdown_color"]) ? $context["dropdown_color"] : null) != "")) {
                // line 52
                echo "\t\t\t\t#header .navbar .dropdown-menu li>a {
\t\t\t\t\tcolor: ";
                // line 53
                echo (isset($context["dropdown_color"]) ? $context["dropdown_color"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 56
            echo "\t\t\t";
            if (((isset($context["dropdown_hover"]) ? $context["dropdown_hover"] : null) != "")) {
                // line 57
                echo "\t\t\t\t#header .navbar .dropdown-menu li>a:hover, .navbar .dropdown-menu li>a:focus, .navbar .dropdown-submenu:hover>a {
\t\t\t\t\tcolor: ";
                // line 58
                echo (isset($context["dropdown_hover"]) ? $context["dropdown_hover"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 61
            echo "
\t\t\t";
            // line 62
            if (((isset($context["title_color"]) ? $context["title_color"] : null) != "")) {
                // line 63
                echo "\t\t\t\t.page-header h1,
\t\t\t\t.page-header h1 a {
\t\t\t\t\tcolor: ";
                // line 65
                echo (isset($context["title_color"]) ? $context["title_color"] : null);
                echo " !important;
\t\t\t\t}
\t\t\t";
            }
            // line 68
            echo "\t\t\t";
            if (((isset($context["content_links"]) ? $context["content_links"] : null) != "")) {
                // line 69
                echo "\t\t\t\t.mainborder a {
\t\t\t\t\tcolor: ";
                // line 70
                echo (isset($context["content_links"]) ? $context["content_links"] : null);
                echo ";\t
\t\t\t\t}
\t\t\t";
            }
            // line 73
            echo "\t\t\t";
            if (((isset($context["bodytext_color"]) ? $context["bodytext_color"] : null) != "")) {
                // line 74
                echo "\t\t\t\t.mainborder, .mainborder .nav-tabs > li > a, 
\t\t\t\t.category-list a, .product-page, #continue-shopping,
\t\t\t\t.maintop-container, .product-details {
\t\t\t\t\tcolor: ";
                // line 77
                echo (isset($context["bodytext_color"]) ? $context["bodytext_color"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 80
            echo "\t\t\t";
            if (((isset($context["lighttext_color"]) ? $context["lighttext_color"] : null) != "")) {
                // line 81
                echo "\t\t\t\t.breadcrumb, .breadcrumb a, .pagination .results, .product-page .price-tax, .product-page .price .reward {
\t\t\t\t\tcolor: ";
                // line 82
                echo (isset($context["lighttext_color"]) ? $context["lighttext_color"] : null);
                echo ";
\t\t\t\t}
\t\t\t";
            }
            // line 85
            echo "\t\t\t";
            if (((isset($context["footer_text"]) ? $context["footer_text"] : null) != "")) {
                // line 86
                echo "\t\t\t#footer, #footer h3 {
\t\t\t\tcolor: ";
                // line 87
                echo (isset($context["footer_text"]) ? $context["footer_text"] : null);
                echo ";
\t\t\t}
\t\t\t";
            }
            // line 90
            echo "\t\t\t";
            if (((isset($context["footer_links"]) ? $context["footer_links"] : null) != "")) {
                // line 91
                echo "\t\t\t#footer a, #footer a:link, #footer a:visited {
\t\t\t\tcolor: ";
                // line 92
                echo (isset($context["footer_links"]) ? $context["footer_links"] : null);
                echo ";
\t\t\t}
\t\t\t";
            }
            // line 95
            echo "
\t\t\t";
            // line 96
            if (((isset($context["btninver_normal"]) ? $context["btninver_normal"] : null) != "")) {
                // line 97
                echo "\t\t\t\t.btn-primary,
\t\t\t\ta.btn-primary,
\t\t\t\t.mainborder a.btn-primary,
\t\t\t\t.btn-primary.disabled,
\t\t\t\t.btn-primary.disabled:hover,
\t\t\t\t.btn-primary[disabled] {
\t\t\t\t\tborder-color: ";
                // line 103
                echo (isset($context["btninver_normal"]) ? $context["btninver_normal"] : null);
                echo ";
\t\t\t\t\tcolor: ";
                // line 104
                echo (isset($context["btninver_normal"]) ? $context["btninver_normal"] : null);
                echo ";
\t\t\t\t\tbackground: transparent;
\t\t\t\t}

\t\t\t\t.btn-primary:hover,
\t\t\t\ta.btn-primary:hover,
\t\t\t\t.btn-primary:focus,
\t\t\t\t.btn-primary.focus,
\t\t\t\t.btn-primary.active,
\t\t\t\t.btn-primary:active,
\t\t\t\t.btn-primary:active:hover,
\t\t\t\t.btn-primary.active:hover,
\t\t\t\t.btn-primary:active:focus,
\t\t\t\t.btn-primary.active:focus,
\t\t\t\t.btn-primary:active.focus, 
\t\t\t\t.btn-primary.active.focus,
\t\t\t\t.btn-primary.disabled,
\t\t\t\t.btn-primary[disabled] {
\t\t\t\t\tborder-color: ";
                // line 122
                echo (isset($context["btninver_normal"]) ? $context["btninver_normal"] : null);
                echo ";
\t\t\t\t\tbackground-color: ";
                // line 123
                echo (isset($context["btninver_normal"]) ? $context["btninver_normal"] : null);
                echo ";
\t\t\t\t\tcolor: #fff;
\t\t\t\t}
\t\t\t";
            }
            // line 127
            echo "
\t\t\t";
            // line 128
            if (((isset($context["btncart_normal"]) ? $context["btncart_normal"] : null) != "")) {
                // line 129
                echo "\t\t\t\t.payment .right .button,
\t\t\t\t.btn-cart,
\t\t\t\ta.btn-cart,
\t\t\t\t.mainborder a.btn-cart,
\t\t\t\t#button-confirm {
\t\t\t\t\tborder-color: ";
                // line 134
                echo (isset($context["btncart_normal"]) ? $context["btncart_normal"] : null);
                echo ";
\t\t\t\t\tbackground-color: ";
                // line 135
                echo (isset($context["btncart_normal"]) ? $context["btncart_normal"] : null);
                echo ";
\t\t\t\t}
\t\t\t\t.payment .right .button:hover,
\t\t\t\t.payment .right .button:active,
\t\t\t\t.payment .right .button:focus,
\t\t\t\t.btn-cart:hover,
\t\t\t\t.btn-cart:active,
\t\t\t\t.btn-cart:focus,
\t\t\t\ta.btn-cart:hover,
\t\t\t\ta.btn-cart:active,
\t\t\t\ta.btn-cart:focus,
\t\t\t\t#button-confirm:hover {
\t\t\t\t\tborder-color: ";
                // line 147
                echo (isset($context["btncart_normal"]) ? $context["btncart_normal"] : null);
                echo ";
\t\t\t\t\tcolor: ";
                // line 148
                echo (isset($context["btncart_normal"]) ? $context["btncart_normal"] : null);
                echo ";
\t\t\t\t\tbackground: transparent;
\t\t\t\t}
\t\t\t\t.owl-controls .owl-page.active span,
\t\t\t\t.tp-bullets.simplebullets.round div.bullet.selected {
\t\t\t\t\tbackground-color: ";
                // line 153
                echo (isset($context["btncart_normal"]) ? $context["btncart_normal"] : null);
                echo " !important;
\t\t\t\t\tborder-color: ";
                // line 154
                echo (isset($context["btncart_normal"]) ? $context["btncart_normal"] : null);
                echo " !important;
\t\t\t\t}
\t\t\t\t
\t\t\t";
            }
            // line 158
            echo "
\t\t\t";
            // line 159
            if (((isset($context["product_name_color"]) ? $context["product_name_color"] : null) != "")) {
                // line 160
                echo "\t\t\t.product-thumb .name a,  .product-slider .name a  {
\t\t\t\tcolor: ";
                // line 161
                echo (isset($context["product_name_color"]) ? $context["product_name_color"] : null);
                echo ";\t
\t\t\t}
\t\t\t";
            }
            // line 164
            echo "\t\t\t";
            if (((isset($context["normal_price"]) ? $context["normal_price"] : null) != "")) {
                // line 165
                echo "\t\t\t.product-thumb .price, .product-slider .price, .product-page .price-normal {
\t\t\t\tcolor: ";
                // line 166
                echo (isset($context["normal_price"]) ? $context["normal_price"] : null);
                echo ";\t
\t\t\t}
\t\t\t";
            }
            // line 169
            echo "\t\t\t";
            if (((isset($context["old_price"]) ? $context["old_price"] : null) != "")) {
                // line 170
                echo "\t\t\t.price-old {
\t\t\t\tcolor: ";
                // line 171
                echo (isset($context["old_price"]) ? $context["old_price"] : null);
                echo ";\t
\t\t\t}
\t\t\t";
            }
            // line 174
            echo "\t\t\t";
            if (((isset($context["new_price"]) ? $context["new_price"] : null) != "")) {
                // line 175
                echo "\t\t\t.price-new {
\t\t\t\tcolor: ";
                // line 176
                echo (isset($context["new_price"]) ? $context["new_price"] : null);
                echo ";\t
\t\t\t}
\t\t\t";
            }
            // line 179
            echo "\t\t\t";
            if ((((isset($context["onsale_bg"]) ? $context["onsale_bg"] : null) != "") || ((isset($context["onsale_text"]) ? $context["onsale_text"] : null) != ""))) {
                // line 180
                echo "\t\t\t.onsale span {
\t\t\t\tbackground-color: ";
                // line 181
                echo (isset($context["onsale_bg"]) ? $context["onsale_bg"] : null);
                echo ";
\t\t\t}
\t\t\t";
            }
            // line 184
            echo "\t\t\t";
            if (((isset($context["sidebar_active_menu"]) ? $context["sidebar_active_menu"] : null) != "")) {
                // line 185
                echo "\t\t\t.box-category > ul > li a.active {
\t\t\t\tcolor: ";
                // line 186
                echo (isset($context["sidebar_active_menu"]) ? $context["sidebar_active_menu"] : null);
                echo ";
\t\t\t}
\t\t\t";
            }
            // line 189
            echo "\t
\t\t";
        }
        // line 190
        echo " /*clearshop_custom_colors END*/

\t\t";
        // line 192
        if (((isset($context["clearshop_zoom_position"]) ? $context["clearshop_zoom_position"] : null) == "inside")) {
            // line 193
            echo "\t\t\t.cloud-zoom-big { left:0 !important; }
\t\t";
        }
        // line 195
        echo "
\t\t";
        // line 196
        if (((isset($context["clearshop_display_cart_button"]) ? $context["clearshop_display_cart_button"] : null) == 1)) {
            // line 197
            echo "\t\t\t.product-thumb .cart .btn,
\t\t\t.product-slider .item .cart .btn,
\t\t\t.product-thumb .links,
\t\t\t.product-slider .item .links {
\t\t\t\topacity: 1;
\t\t\t\t-moz-opacity: 1;
\t\t\t\tfilter: alpha(opacity=100);
\t\t\t}
\t\t";
        }
        // line 206
        echo "
\t\t";
        // line 207
        if (((isset($context["clearshop_quickview_modules"]) ? $context["clearshop_quickview_modules"] : null) != 1)) {
            // line 208
            echo "\t\t\t.content-top .quickview, .content-bottom .quickview { display:none !important; }
\t\t";
        }
        // line 210
        echo "
\t\t";
        // line 211
        if (((isset($context["clearshop_quickview_mobile"]) ? $context["clearshop_quickview_mobile"] : null) != 1)) {
            // line 212
            echo "\t\t\t@media only screen and (max-width: 767px) {
\t\t\t\t.quickview { display:none !important; }
\t\t\t}
\t\t";
        }
        // line 216
        echo "
\t\t";
        // line 217
        if (((isset($context["clearshop_quickview_categories"]) ? $context["clearshop_quickview_categories"] : null) != 1)) {
            // line 218
            echo "\t\t\t#content .quickview { display:none !important; }
\t\t";
        }
        // line 220
        echo "
\t\t";
        // line 221
        if (((isset($context["clearshop_show_wishlist"]) ? $context["clearshop_show_wishlist"] : null) != 1)) {
            // line 222
            echo "\t\t\t.wishlist, .wishlist-link, #wishlist-total { display:none !important; }
\t\t";
        }
        // line 224
        echo "
\t\t";
        // line 225
        if (((isset($context["clearshop_show_compare"]) ? $context["clearshop_show_compare"] : null) != 1)) {
            // line 226
            echo "\t\t\t.compare, .product-compare { display:none !important; }
\t\t";
        }
        // line 228
        echo "
\t\t";
        // line 229
        if (((isset($context["clearshop_show_sale_bubble"]) ? $context["clearshop_show_sale_bubble"] : null) != 1)) {
            // line 230
            echo "\t\t\t.onsale { display:none !important; }
\t\t";
        }
        // line 232
        echo "
\t\t/*body font*/

\t\t";
        // line 235
        if (((isset($context["clearshop_body_font"]) ? $context["clearshop_body_font"] : null) != "")) {
            // line 236
            echo "\t\t\t";
            $context["clearshop_body_font"] = twig_replace_filter((isset($context["clearshop_body_font"]) ? $context["clearshop_body_font"] : null), array("+" => " "));
            // line 237
            echo "\t\t\tbody, p, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .cart .btn, .btn { 
\t\t\t\tfont-family: '";
            // line 238
            echo (isset($context["clearshop_body_font"]) ? $context["clearshop_body_font"] : null);
            echo "'; 
\t\t\t}
\t\t\tbody, p { 
\t\t\t\tfont-size: ";
            // line 241
            echo (isset($context["clearshop_body_font_size"]) ? $context["clearshop_body_font_size"] : null);
            echo "px;
\t\t\t}
\t\t";
        }
        // line 244
        echo "
\t\t/*title font*/

\t\t";
        // line 247
        if (((isset($context["clearshop_title_font"]) ? $context["clearshop_title_font"] : null) != "")) {
            // line 248
            echo "\t\t";
            $context["clearshop_title_font"] = twig_replace_filter((isset($context["clearshop_title_font"]) ? $context["clearshop_title_font"] : null), array("+" => " "));
            // line 249
            echo "\t\t\th1,
\t\t\t.tp-button, .tp-button:hover, .tp-button:focus,
\t\t\tdiv.tp-caption.bignumbers_white,
\t\t\tdiv.tp-caption.light_heavy_70_shadowed {
\t\t\t\tfont-family: '";
            // line 253
            echo (isset($context["clearshop_title_font"]) ? $context["clearshop_title_font"] : null);
            echo "'; 
\t\t\t}
\t\t\th1 {
\t\t\t\tfont-size: ";
            // line 256
            echo (isset($context["clearshop_title_font_size"]) ? $context["clearshop_title_font_size"] : null);
            echo "px;
\t\t\t}
\t\t";
        }
        // line 259
        echo "
\t\t/*small font*/

\t\t";
        // line 262
        if (((isset($context["clearshop_small_font"]) ? $context["clearshop_small_font"] : null) != "")) {
            // line 263
            echo "\t\t";
            $context["clearshop_small_font"] = twig_replace_filter((isset($context["clearshop_small_font"]) ? $context["clearshop_small_font"] : null), array("+" => " "));
            // line 264
            echo "\t\tsmall, .wishlist a, .compare a, .remove a, .product-compare, .product-filter, .product-filter .display li, .product-list .price-tax, .product-page .price-tax, .product-page .price .reward, .product-page .minimum, .product-page .tags a, .review-date, span.error, #copy, #footer .info, .breadcrumb a, .pagination .results, .help {
\t\t\tfont-family: '";
            // line 265
            echo (isset($context["clearshop_small_font"]) ? $context["clearshop_small_font"] : null);
            echo "';
\t\t}
\t\t";
        }
        // line 268
        echo "
\t\t";
        // line 269
        if (((isset($context["clearshop_custom_css_status"]) ? $context["clearshop_custom_css_status"] : null) == 1)) {
            // line 270
            echo "\t\t\t";
            echo (isset($context["clearshop_custom_css"]) ? $context["clearshop_custom_css"] : null);
            echo "
\t\t";
        }
        // line 272
        echo "
\t</style>

\t";
        // line 275
        if (((isset($context["clearshop_custom_stylesheet"]) ? $context["clearshop_custom_stylesheet"] : null) == 1)) {
            // line 276
            echo "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/clearshop/stylesheet/custom.css\"  media=\"screen\" />
";
        }
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/custom_styles.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  594 => 276,  592 => 275,  587 => 272,  581 => 270,  579 => 269,  576 => 268,  570 => 265,  567 => 264,  564 => 263,  562 => 262,  557 => 259,  551 => 256,  545 => 253,  539 => 249,  536 => 248,  534 => 247,  529 => 244,  523 => 241,  517 => 238,  514 => 237,  511 => 236,  509 => 235,  504 => 232,  500 => 230,  498 => 229,  495 => 228,  491 => 226,  489 => 225,  486 => 224,  482 => 222,  480 => 221,  477 => 220,  473 => 218,  471 => 217,  468 => 216,  462 => 212,  460 => 211,  457 => 210,  453 => 208,  451 => 207,  448 => 206,  437 => 197,  435 => 196,  432 => 195,  428 => 193,  426 => 192,  422 => 190,  418 => 189,  412 => 186,  409 => 185,  406 => 184,  400 => 181,  397 => 180,  394 => 179,  388 => 176,  385 => 175,  382 => 174,  376 => 171,  373 => 170,  370 => 169,  364 => 166,  361 => 165,  358 => 164,  352 => 161,  349 => 160,  347 => 159,  344 => 158,  337 => 154,  333 => 153,  325 => 148,  321 => 147,  306 => 135,  302 => 134,  295 => 129,  293 => 128,  290 => 127,  283 => 123,  279 => 122,  258 => 104,  254 => 103,  246 => 97,  244 => 96,  241 => 95,  235 => 92,  232 => 91,  229 => 90,  223 => 87,  220 => 86,  217 => 85,  211 => 82,  208 => 81,  205 => 80,  199 => 77,  194 => 74,  191 => 73,  185 => 70,  182 => 69,  179 => 68,  173 => 65,  169 => 63,  167 => 62,  164 => 61,  158 => 58,  155 => 57,  152 => 56,  146 => 53,  143 => 52,  140 => 51,  134 => 48,  129 => 45,  126 => 44,  120 => 41,  117 => 40,  114 => 39,  108 => 36,  105 => 35,  102 => 34,  96 => 31,  93 => 30,  91 => 29,  88 => 28,  86 => 27,  83 => 26,  79 => 24,  77 => 23,  72 => 20,  68 => 18,  62 => 16,  60 => 15,  53 => 14,  51 => 13,  43 => 10,  41 => 9,  34 => 4,  23 => 2,  19 => 1,);
    }
}
/* {% for gfont in googlefonts %}*/
/* 	<link href='//fonts.googleapis.com/css?family={{ gfont }}:400,300,200,700,800,500{{ cyrillic }}&amp;v1' rel='stylesheet' type='text/css' />*/
/* {% endfor %}*/
/* */
/* 	<style type="text/css" media="screen">*/
/* */
/* 		body {*/
/* 			*/
/* 		{% if clearshop_custom_image != '' %}*/
/* 			background-image: url("{{ base}}{{ clearshop_custom_image}}");*/
/* 			background-position: top center;*/
/* 			background-repeat: no-repeat;*/
/* 		{% elseif clearshop_custom_pattern !='' %}*/
/* 			background-image: url("{{ base  }} {{ clearshop_custom_pattern }}");*/
/* 		{% elseif clearshop_pattern_overlay !='none' %}*/
/* 			background-image: url("catalog/view/theme/clearshop/image/patterns/{{ clearshop_pattern_overlay }}.png");*/
/* 		{% else %}*/
/* 			background-image: none;*/
/* 		{% endif %}*/
/* */
/* 		}*/
/* */
/* 		{% if clearshop_responsive_layout == 'normal' %}*/
/* 			body.responsive .container { max-width: 982px !important; }*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_custom_colors == 1 %}*/
/* */
/* 			{% if bodybg_color != '' %}*/
/* 				body.boxed-skin {*/
/* 					background-color: {{ bodybg_color }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if topbar_bg != '' %}*/
/* 				#thickbar {*/
/* 					background: {{ topbar_bg }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if menu_color != '' %}*/
/* 				.navbar .nav > li > a {*/
/* 					color: {{ menu_color }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if menu_hover != '' %}*/
/* 				.navbar .nav > li:hover > a, .navbar .nav > li > a:hover,*/
/* 				#menu.navbar-dark .navbar .nav > li > a:hover,*/
/* 				#menu.navbar-dark .navbar .nav > li:hover > a {*/
/* 					color: {{ menu_hover }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if dropdown_color != '' %}*/
/* 				#header .navbar .dropdown-menu li>a {*/
/* 					color: {{ dropdown_color }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if dropdown_hover != '' %}*/
/* 				#header .navbar .dropdown-menu li>a:hover, .navbar .dropdown-menu li>a:focus, .navbar .dropdown-submenu:hover>a {*/
/* 					color: {{ dropdown_hover }};*/
/* 				}*/
/* 			{% endif %}*/
/* */
/* 			{% if title_color != '' %}*/
/* 				.page-header h1,*/
/* 				.page-header h1 a {*/
/* 					color: {{ title_color }} !important;*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if content_links != '' %}*/
/* 				.mainborder a {*/
/* 					color: {{ content_links }};	*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if bodytext_color != '' %}*/
/* 				.mainborder, .mainborder .nav-tabs > li > a, */
/* 				.category-list a, .product-page, #continue-shopping,*/
/* 				.maintop-container, .product-details {*/
/* 					color: {{ bodytext_color }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if lighttext_color != '' %}*/
/* 				.breadcrumb, .breadcrumb a, .pagination .results, .product-page .price-tax, .product-page .price .reward {*/
/* 					color: {{ lighttext_color }};*/
/* 				}*/
/* 			{% endif %}*/
/* 			{% if footer_text != '' %}*/
/* 			#footer, #footer h3 {*/
/* 				color: {{ footer_text }};*/
/* 			}*/
/* 			{% endif %}*/
/* 			{% if footer_links != '' %}*/
/* 			#footer a, #footer a:link, #footer a:visited {*/
/* 				color: {{ footer_links }};*/
/* 			}*/
/* 			{% endif %}*/
/* */
/* 			{% if btninver_normal != '' %}*/
/* 				.btn-primary,*/
/* 				a.btn-primary,*/
/* 				.mainborder a.btn-primary,*/
/* 				.btn-primary.disabled,*/
/* 				.btn-primary.disabled:hover,*/
/* 				.btn-primary[disabled] {*/
/* 					border-color: {{ btninver_normal }};*/
/* 					color: {{ btninver_normal }};*/
/* 					background: transparent;*/
/* 				}*/
/* */
/* 				.btn-primary:hover,*/
/* 				a.btn-primary:hover,*/
/* 				.btn-primary:focus,*/
/* 				.btn-primary.focus,*/
/* 				.btn-primary.active,*/
/* 				.btn-primary:active,*/
/* 				.btn-primary:active:hover,*/
/* 				.btn-primary.active:hover,*/
/* 				.btn-primary:active:focus,*/
/* 				.btn-primary.active:focus,*/
/* 				.btn-primary:active.focus, */
/* 				.btn-primary.active.focus,*/
/* 				.btn-primary.disabled,*/
/* 				.btn-primary[disabled] {*/
/* 					border-color: {{ btninver_normal }};*/
/* 					background-color: {{ btninver_normal }};*/
/* 					color: #fff;*/
/* 				}*/
/* 			{% endif %}*/
/* */
/* 			{% if btncart_normal != '' %}*/
/* 				.payment .right .button,*/
/* 				.btn-cart,*/
/* 				a.btn-cart,*/
/* 				.mainborder a.btn-cart,*/
/* 				#button-confirm {*/
/* 					border-color: {{ btncart_normal }};*/
/* 					background-color: {{ btncart_normal }};*/
/* 				}*/
/* 				.payment .right .button:hover,*/
/* 				.payment .right .button:active,*/
/* 				.payment .right .button:focus,*/
/* 				.btn-cart:hover,*/
/* 				.btn-cart:active,*/
/* 				.btn-cart:focus,*/
/* 				a.btn-cart:hover,*/
/* 				a.btn-cart:active,*/
/* 				a.btn-cart:focus,*/
/* 				#button-confirm:hover {*/
/* 					border-color: {{ btncart_normal }};*/
/* 					color: {{ btncart_normal }};*/
/* 					background: transparent;*/
/* 				}*/
/* 				.owl-controls .owl-page.active span,*/
/* 				.tp-bullets.simplebullets.round div.bullet.selected {*/
/* 					background-color: {{ btncart_normal }} !important;*/
/* 					border-color: {{ btncart_normal }} !important;*/
/* 				}*/
/* 				*/
/* 			{% endif %}*/
/* */
/* 			{% if product_name_color != '' %}*/
/* 			.product-thumb .name a,  .product-slider .name a  {*/
/* 				color: {{ product_name_color }};	*/
/* 			}*/
/* 			{% endif %}*/
/* 			{% if normal_price != '' %}*/
/* 			.product-thumb .price, .product-slider .price, .product-page .price-normal {*/
/* 				color: {{ normal_price }};	*/
/* 			}*/
/* 			{% endif %}*/
/* 			{% if old_price != '' %}*/
/* 			.price-old {*/
/* 				color: {{ old_price }};	*/
/* 			}*/
/* 			{% endif %}*/
/* 			{% if new_price != '' %}*/
/* 			.price-new {*/
/* 				color: {{ new_price }};	*/
/* 			}*/
/* 			{% endif %}*/
/* 			{% if onsale_bg != '' or onsale_text != '' %}*/
/* 			.onsale span {*/
/* 				background-color: {{ onsale_bg }};*/
/* 			}*/
/* 			{% endif %}*/
/* 			{% if sidebar_active_menu != '' %}*/
/* 			.box-category > ul > li a.active {*/
/* 				color: {{ sidebar_active_menu }};*/
/* 			}*/
/* 			{% endif %}*/
/* 	*/
/* 		{% endif %} /*clearshop_custom_colors END*//* */
/* */
/* 		{% if clearshop_zoom_position == 'inside' %}*/
/* 			.cloud-zoom-big { left:0 !important; }*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_display_cart_button == 1 %}*/
/* 			.product-thumb .cart .btn,*/
/* 			.product-slider .item .cart .btn,*/
/* 			.product-thumb .links,*/
/* 			.product-slider .item .links {*/
/* 				opacity: 1;*/
/* 				-moz-opacity: 1;*/
/* 				filter: alpha(opacity=100);*/
/* 			}*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_quickview_modules != 1 %}*/
/* 			.content-top .quickview, .content-bottom .quickview { display:none !important; }*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_quickview_mobile != 1 %}*/
/* 			@media only screen and (max-width: 767px) {*/
/* 				.quickview { display:none !important; }*/
/* 			}*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_quickview_categories != 1 %}*/
/* 			#content .quickview { display:none !important; }*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_show_wishlist != 1 %}*/
/* 			.wishlist, .wishlist-link, #wishlist-total { display:none !important; }*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_show_compare != 1 %}*/
/* 			.compare, .product-compare { display:none !important; }*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_show_sale_bubble != 1 %}*/
/* 			.onsale { display:none !important; }*/
/* 		{% endif %}*/
/* */
/* 		/*body font*//* */
/* */
/* 		{% if clearshop_body_font != '' %}*/
/* 			{% set clearshop_body_font = clearshop_body_font|replace({'+': ' '}) %}*/
/* 			body, p, h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, .cart .btn, .btn { */
/* 				font-family: '{{ clearshop_body_font }}'; */
/* 			}*/
/* 			body, p { */
/* 				font-size: {{ clearshop_body_font_size }}px;*/
/* 			}*/
/* 		{% endif %}*/
/* */
/* 		/*title font*//* */
/* */
/* 		{% if clearshop_title_font != '' %}*/
/* 		{% set clearshop_title_font = clearshop_title_font|replace({'+': ' '}) %}*/
/* 			h1,*/
/* 			.tp-button, .tp-button:hover, .tp-button:focus,*/
/* 			div.tp-caption.bignumbers_white,*/
/* 			div.tp-caption.light_heavy_70_shadowed {*/
/* 				font-family: '{{ clearshop_title_font }}'; */
/* 			}*/
/* 			h1 {*/
/* 				font-size: {{ clearshop_title_font_size }}px;*/
/* 			}*/
/* 		{% endif %}*/
/* */
/* 		/*small font*//* */
/* */
/* 		{% if clearshop_small_font != '' %}*/
/* 		{% set clearshop_small_font = clearshop_small_font|replace({'+': ' '}) %}*/
/* 		small, .wishlist a, .compare a, .remove a, .product-compare, .product-filter, .product-filter .display li, .product-list .price-tax, .product-page .price-tax, .product-page .price .reward, .product-page .minimum, .product-page .tags a, .review-date, span.error, #copy, #footer .info, .breadcrumb a, .pagination .results, .help {*/
/* 			font-family: '{{ clearshop_small_font }}';*/
/* 		}*/
/* 		{% endif %}*/
/* */
/* 		{% if clearshop_custom_css_status == 1 %}*/
/* 			{{ clearshop_custom_css }}*/
/* 		{% endif %}*/
/* */
/* 	</style>*/
/* */
/* 	{% if clearshop_custom_stylesheet == 1 %}*/
/* 	<link rel="stylesheet" type="text/css" href="catalog/view/theme/clearshop/stylesheet/custom.css"  media="screen" />*/
/* {% endif %}*/
