<?php

/* clearshop/template/common/footer.twig */
class __TwigTemplate_7e97342e12c4fdd5b8931110566688e69f067a888f1b2cf25a03ce88b5c17cc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "\t\t\t</div> <!-- #content_wrapper -->

\t\t\t<div id=\"footer\">

\t\t\t\t<div class=\"container\">

\t\t\t\t";
        // line 7
        echo (isset($context["footer_modules"]) ? $context["footer_modules"] : null);
        echo "

\t\t\t\t\t<div class=\"row\">

\t\t\t\t\t\t<div class=\"column col-sm-2\">
\t\t\t\t\t\t\t<h3 class=\"header\">";
        // line 12
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "</h3>
\t\t\t\t\t\t\t<ul class=\"content\">
\t\t\t\t\t\t\t\t";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 15
            echo "\t\t\t\t\t\t\t\t\t<li><a href=\"";
            echo $this->getAttribute($context["information"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["information"], "title", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"column col-sm-2\">
\t\t\t\t\t\t\t<h3 class=\"header\">";
        // line 21
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</h3>
\t\t\t\t\t\t\t<ul class=\"content\">
\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 23
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
        echo "</a></li>
\t\t\t          <li><a href=\"";
        // line 24
        echo (isset($context["return"]) ? $context["return"] : null);
        echo "\">";
        echo (isset($context["text_return"]) ? $context["text_return"] : null);
        echo "</a></li>
\t\t\t          <li><a href=\"";
        // line 25
        echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
        echo "\">";
        echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"column col-sm-2\">
\t\t\t\t\t\t\t<h3 class=\"header\">";
        // line 30
        echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
        echo "</h3>
\t\t\t\t\t\t\t\t<ul class=\"content\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 32
        echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
        echo "\">";
        echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
        echo "</a></li>
\t\t\t\t          <li><a href=\"";
        // line 33
        echo (isset($context["voucher"]) ? $context["voucher"] : null);
        echo "\">";
        echo (isset($context["text_voucher"]) ? $context["text_voucher"] : null);
        echo "</a></li>
\t\t\t\t          <li><a href=\"";
        // line 34
        echo (isset($context["affiliate"]) ? $context["affiliate"] : null);
        echo "\">";
        echo (isset($context["text_affiliate"]) ? $context["text_affiliate"] : null);
        echo "</a></li>
\t\t\t\t          <li><a href=\"";
        // line 35
        echo (isset($context["special"]) ? $context["special"] : null);
        echo "\">";
        echo (isset($context["text_special"]) ? $context["text_special"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"column col-sm-2\">
\t\t\t\t\t\t\t<h3 class=\"header\">";
        // line 40
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</h3>
\t\t\t\t\t\t\t\t<ul class=\"content\">
\t\t\t\t\t\t\t\t\t<li><a href=\"";
        // line 42
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\">";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</a></li>
\t\t\t\t          <li><a href=\"";
        // line 43
        echo (isset($context["order"]) ? $context["order"] : null);
        echo "\">";
        echo (isset($context["text_order"]) ? $context["text_order"] : null);
        echo "</a></li>
\t\t\t\t          <li><a href=\"";
        // line 44
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</a></li>
\t\t\t\t          <li><a href=\"";
        // line 45
        echo (isset($context["newsletter"]) ? $context["newsletter"] : null);
        echo "\">";
        echo (isset($context["text_newsletter"]) ? $context["text_newsletter"] : null);
        echo "</a></li>
\t\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t\t
\t\t\t\t\t\t\t<div class=\"social\">

\t\t\t\t\t\t\t\t";
        // line 53
        if (((isset($context["clearshop_social_footer"]) ? $context["clearshop_social_footer"] : null) == 1)) {
            // line 54
            echo "
\t\t\t\t\t\t\t\t\t<div class=\"social\">

\t\t\t\t\t\t\t\t\t";
            // line 57
            if ((isset($context["clearshop_social"]) ? $context["clearshop_social"] : null)) {
                // line 58
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_social"]) ? $context["clearshop_social"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                    // line 59
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["value"], "url", array())) {
                        // line 60
                        echo "\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        echo $this->getAttribute($context["value"], "url", array());
                        echo "\" class=\"fa fa-";
                        echo $this->getAttribute($context["value"], "class", array());
                        echo "\" target=\"";
                        echo $this->getAttribute($context["value"], "target", array());
                        echo "\"></a>
\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 62
                    echo "\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 63
                echo "\t\t\t\t\t\t\t\t\t";
            }
            // line 64
            echo "
\t\t\t\t\t\t\t\t\t";
            // line 65
            if ((isset($context["clearshop_custom_icon"]) ? $context["clearshop_custom_icon"] : null)) {
                // line 66
                echo "\t\t\t\t\t\t\t\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_custom_icon"]) ? $context["clearshop_custom_icon"] : null));
                foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                    // line 67
                    echo "\t\t\t\t\t\t\t\t\t\t\t";
                    if (($this->getAttribute($context["value"], "class", array()) != "")) {
                        // line 68
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t";
                        if (($this->getAttribute($context["value"], "class", array()) != "")) {
                            // line 69
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                            echo $this->getAttribute($context["value"], "url", array());
                            echo "\" class=\"fa fa-";
                            echo $this->getAttribute($context["value"], "class", array());
                            echo "\" target=\"";
                            echo $this->getAttribute($context["value"], "target", array());
                            echo "\"></a>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 71
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<a class=\"fa fa-";
                            echo $this->getAttribute($context["value"], "class", array());
                            echo "\"></a>
\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 73
                        echo "\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 74
                    echo "\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 75
                echo "\t\t\t\t\t\t\t\t\t";
            }
            // line 76
            echo "
\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t";
        }
        // line 80
        echo "
\t\t\t\t\t\t\t\t<div class=\"footer_info\">
\t\t\t\t\t\t\t\t\t";
        // line 82
        echo (isset($context["clearshop_footer_info_text"]) ? $context["clearshop_footer_info_text"] : null);
        echo "
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t</div>

\t\t\t\t\t</div> <!-- .row -->

\t\t\t\t</div> <!-- .container -->

\t\t\t</div> <!-- #footer -->

\t\t\t<footer id=\"footend\">

\t\t\t\t<div class=\"container\">

\t\t\t\t\t<div class=\"copy\">
\t\t\t\t\t\t";
        // line 100
        echo (isset($context["clearshop_copyright"]) ? $context["clearshop_copyright"] : null);
        echo "
\t\t\t\t\t</div>

\t\t\t\t</div> <!-- .container -->

\t\t</footer> <!-- #footend -->

\t\t </div> <!-- #supercontainer -->

\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/respond.min.js\"></script>
\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/jquery.easing-1.3.min.js\"></script>
\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/cloud-zoom.1.0.3-min.js\"></script>
\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/jquery.ui.totop.js\"></script>
\t\t<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/custom.js\"></script>

\t\t";
        // line 115
        if ((((isset($context["clearshop_custom_js_status"]) ? $context["clearshop_custom_js_status"] : null) == 1) && ((isset($context["clearshop_custom_js"]) ? $context["clearshop_custom_js"] : null) != ""))) {
            // line 116
            echo "\t\t\t";
            echo (isset($context["clearshop_custom_js"]) ? $context["clearshop_custom_js"] : null);
            echo "
\t\t";
        }
        // line 118
        echo "
</body></html>";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  294 => 118,  288 => 116,  286 => 115,  268 => 100,  247 => 82,  243 => 80,  237 => 76,  234 => 75,  228 => 74,  225 => 73,  219 => 71,  209 => 69,  206 => 68,  203 => 67,  198 => 66,  196 => 65,  193 => 64,  190 => 63,  184 => 62,  174 => 60,  171 => 59,  166 => 58,  164 => 57,  159 => 54,  157 => 53,  144 => 45,  138 => 44,  132 => 43,  126 => 42,  121 => 40,  111 => 35,  105 => 34,  99 => 33,  93 => 32,  88 => 30,  78 => 25,  72 => 24,  66 => 23,  61 => 21,  55 => 17,  44 => 15,  40 => 14,  35 => 12,  27 => 7,  19 => 1,);
    }
}
/* 			</div> <!-- #content_wrapper -->*/
/* */
/* 			<div id="footer">*/
/* */
/* 				<div class="container">*/
/* */
/* 				{{ footer_modules }}*/
/* */
/* 					<div class="row">*/
/* */
/* 						<div class="column col-sm-2">*/
/* 							<h3 class="header">{{ text_information }}</h3>*/
/* 							<ul class="content">*/
/* 								{% for information in informations %}*/
/* 									<li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/* 								{% endfor %}*/
/* 							</ul>*/
/* 						</div>*/
/* */
/* 						<div class="column col-sm-2">*/
/* 							<h3 class="header">{{ text_contact }}</h3>*/
/* 							<ul class="content">*/
/* 								<li><a href="{{ contact }}">{{ text_contact }}</a></li>*/
/* 			          <li><a href="{{ return }}">{{ text_return }}</a></li>*/
/* 			          <li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>*/
/* 							</ul>*/
/* 						</div>*/
/* */
/* 						<div class="column col-sm-2">*/
/* 							<h3 class="header">{{ text_manufacturer }}</h3>*/
/* 								<ul class="content">*/
/* 									<li><a href="{{ manufacturer }}">{{ text_manufacturer }}</a></li>*/
/* 				          <li><a href="{{ voucher }}">{{ text_voucher }}</a></li>*/
/* 				          <li><a href="{{ affiliate }}">{{ text_affiliate }}</a></li>*/
/* 				          <li><a href="{{ special }}">{{ text_special }}</a></li>*/
/* 								</ul>*/
/* 						</div>*/
/* */
/* 						<div class="column col-sm-2">*/
/* 							<h3 class="header">{{ text_account }}</h3>*/
/* 								<ul class="content">*/
/* 									<li><a href="{{ account }}">{{ text_account }}</a></li>*/
/* 				          <li><a href="{{ order }}">{{ text_order }}</a></li>*/
/* 				          <li><a href="{{ wishlist }}">{{ text_wishlist }}</a></li>*/
/* 				          <li><a href="{{ newsletter }}">{{ text_newsletter }}</a></li>*/
/* 								</ul>*/
/* 						</div>*/
/* */
/* 						<div class="col-sm-4">*/
/* 						*/
/* 							<div class="social">*/
/* */
/* 								{% if clearshop_social_footer == 1 %}*/
/* */
/* 									<div class="social">*/
/* */
/* 									{% if clearshop_social %}*/
/* 										{% for key,value in clearshop_social %}*/
/* 										{% if value.url %}*/
/* 											<a href="{{ value.url }}" class="fa fa-{{ value.class }}" target="{{ value.target}}"></a>*/
/* 											{% endif %}*/
/* 										{% endfor %}*/
/* 									{% endif %}*/
/* */
/* 									{% if clearshop_custom_icon %}*/
/* 										{% for key,value in clearshop_custom_icon %}*/
/* 											{% if value.class != '' %}*/
/* 												{% if value.class != '' %}*/
/* 													<a href="{{ value.url }}" class="fa fa-{{ value.class }}" target="{{ value.target}}"></a>*/
/* 												{% else %}*/
/* 													<a class="fa fa-{{ value.class }}"></a>*/
/* 												{% endif %}*/
/* 											{% endif %}*/
/* 										{% endfor %}*/
/* 									{% endif %}*/
/* */
/* 									</div>*/
/* */
/* 								{% endif %}*/
/* */
/* 								<div class="footer_info">*/
/* 									{{ clearshop_footer_info_text }}*/
/* 								</div>*/
/* 									*/
/* 							</div>*/
/* */
/* 						</div>*/
/* */
/* 					</div> <!-- .row -->*/
/* */
/* 				</div> <!-- .container -->*/
/* */
/* 			</div> <!-- #footer -->*/
/* */
/* 			<footer id="footend">*/
/* */
/* 				<div class="container">*/
/* */
/* 					<div class="copy">*/
/* 						{{ clearshop_copyright }}*/
/* 					</div>*/
/* */
/* 				</div> <!-- .container -->*/
/* */
/* 		</footer> <!-- #footend -->*/
/* */
/* 		 </div> <!-- #supercontainer -->*/
/* */
/* 		<script type="text/javascript" src="catalog/view/theme/clearshop/js/respond.min.js"></script>*/
/* 		<script type="text/javascript" src="catalog/view/theme/clearshop/js/jquery.easing-1.3.min.js"></script>*/
/* 		<script type="text/javascript" src="catalog/view/theme/clearshop/js/cloud-zoom.1.0.3-min.js"></script>*/
/* 		<script type="text/javascript" src="catalog/view/theme/clearshop/js/jquery.ui.totop.js"></script>*/
/* 		<script type="text/javascript" src="catalog/view/theme/clearshop/js/custom.js"></script>*/
/* */
/* 		{% if clearshop_custom_js_status == 1 and clearshop_custom_js != '' %}*/
/* 			{{ clearshop_custom_js }}*/
/* 		{% endif %}*/
/* */
/* </body></html>*/
