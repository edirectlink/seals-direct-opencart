<?php

/* clearshop/template/extension/module/featured_categories.twig */
class __TwigTemplate_f8c8a4d3af716a80c83db9872fe479d563459804270a3a7bfee8d35186eb1906 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"featured_categories";
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "\" class=\"category-slider product-slider top-arrows\">
\t
\t<h4>";
        // line 3
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h4>

\t<div class=\"owl-carousel\">
\t\t";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 7
            echo "\t\t<div class=\"item\">
\t\t\t<div class=\"inner product-thumb\">
\t\t\t\t<div class=\"image\">
\t\t\t\t\t<div class=\"thumb\"><a href=\"";
            // line 10
            echo $this->getAttribute($context["category"], "href", array());
            echo "\"><img src=\"";
            echo $this->getAttribute($context["category"], "thumb", array());
            echo "\" alt=\"";
            echo $this->getAttribute($context["category"], "name", array());
            echo "\" title=\"";
            echo $this->getAttribute($context["category"], "name", array());
            echo "\" class=\"img-responsive\" /></a></div>
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t<div class=\"caption\">
\t\t\t\t\t<h4 class=\"name\"><a href=\"";
            // line 14
            echo $this->getAttribute($context["category"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["category"], "name", array());
            echo "</a></h4>
\t\t\t\t\t";
            // line 15
            if (($this->getAttribute($context["category"], "display_description_status", array()) == "1")) {
                // line 16
                echo "\t\t\t\t\t\t<p class=\"catdesc\">";
                echo $this->getAttribute($context["category"], "description", array());
                echo "</p>
\t\t\t\t\t";
            }
            // line 18
            echo "\t\t\t\t\t<div class=\"cart\"><a class=\"btn btn-default btn-sm\" title=\"\" href=\"";
            echo $this->getAttribute($context["category"], "href", array());
            echo "\">";
            echo (isset($context["text_browse"]) ? $context["text_browse"] : null);
            echo "</a></div>
\t\t\t\t</div>
\t\t\t</div><!-- .inner -->
\t\t</div><!-- .item -->
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "\t</div><!-- .owl-carousel -->

</div>

<script type=\"text/javascript\"><!--
\$(document).ready(function() {
\t\$('#featured_categories";
        // line 29
        echo (isset($context["module"]) ? $context["module"] : null);
        echo " .owl-carousel').owlCarousel({
\t\titems: 5,
\t\titemsMobile:[479,2],
\t\t";
        // line 32
        if (((isset($context["clearshop_carousel_autoplay"]) ? $context["clearshop_carousel_autoplay"] : null) == 1)) {
            echo " autoPlay: 3000, ";
        }
        // line 33
        echo "\t\tnavigation: true,
\t\tnavigationText: ['<i class=\"fa fa-angle-left fa-5x\"></i>', '<i class=\"fa fa-angle-right fa-5x\"></i>'],
\t\tpagination: true,
\t\tloop:true
\t});
});
--></script>
";
    }

    public function getTemplateName()
    {
        return "clearshop/template/extension/module/featured_categories.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 33,  95 => 32,  89 => 29,  81 => 23,  67 => 18,  61 => 16,  59 => 15,  53 => 14,  40 => 10,  35 => 7,  31 => 6,  25 => 3,  19 => 1,);
    }
}
/* <div id="featured_categories{{ module }}" class="category-slider product-slider top-arrows">*/
/* 	*/
/* 	<h4>{{ heading_title }}</h4>*/
/* */
/* 	<div class="owl-carousel">*/
/* 		{% for category in categories %}*/
/* 		<div class="item">*/
/* 			<div class="inner product-thumb">*/
/* 				<div class="image">*/
/* 					<div class="thumb"><a href="{{ category.href }}"><img src="{{ category.thumb }}" alt="{{ category.name }}" title="{{ category.name }}" class="img-responsive" /></a></div>*/
/* 				</div>*/
/* 				*/
/* 				<div class="caption">*/
/* 					<h4 class="name"><a href="{{ category.href }}">{{ category.name }}</a></h4>*/
/* 					{% if category.display_description_status == '1' %}*/
/* 						<p class="catdesc">{{ category.description }}</p>*/
/* 					{% endif %}*/
/* 					<div class="cart"><a class="btn btn-default btn-sm" title="" href="{{ category.href }}">{{ text_browse }}</a></div>*/
/* 				</div>*/
/* 			</div><!-- .inner -->*/
/* 		</div><!-- .item -->*/
/* 		{% endfor %}*/
/* 	</div><!-- .owl-carousel -->*/
/* */
/* </div>*/
/* */
/* <script type="text/javascript"><!--*/
/* $(document).ready(function() {*/
/* 	$('#featured_categories{{ module }} .owl-carousel').owlCarousel({*/
/* 		items: 5,*/
/* 		itemsMobile:[479,2],*/
/* 		{% if clearshop_carousel_autoplay == 1 %} autoPlay: 3000, {% endif %}*/
/* 		navigation: true,*/
/* 		navigationText: ['<i class="fa fa-angle-left fa-5x"></i>', '<i class="fa fa-angle-right fa-5x"></i>'],*/
/* 		pagination: true,*/
/* 		loop:true*/
/* 	});*/
/* });*/
/* --></script>*/
/* */
