<?php

/* clearshop/template/extension/module/bannerplus.twig */
class __TwigTemplate_c78949fe4409a5a0aa5ba218d7157ff0f3e3813ea65492da5c5e82afb7f1d8b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["count"] = 1;
        // line 2
        echo "<style>
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sections"]) ? $context["sections"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 4
            echo "\t";
            if (($this->getAttribute($context["section"], "bgcolor", array()) != "")) {
                // line 5
                echo "\t\t.rich_banner .rbanner";
                echo (isset($context["mod_id"]) ? $context["mod_id"] : null);
                echo "-";
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .banner_inner { background: ";
                echo $this->getAttribute($context["section"], "bgcolor", array());
                echo "; border: 0; border-radius: 5px; }
\t\t.rich_banner .rbanner";
                // line 6
                echo (isset($context["mod_id"]) ? $context["mod_id"] : null);
                echo "-";
                echo (isset($context["count"]) ? $context["count"] : null);
                echo " .info_wrapper { color: #fff; }
\t";
            }
            // line 8
            echo "\t";
            echo $this->getAttribute($context["section"], "css", array());
            echo "
\t";
            // line 9
            $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "</style>

";
        // line 13
        if ((isset($context["module_title"]) ? $context["module_title"] : null)) {
            // line 14
            echo "<div class=\"contentset center ";
            echo (isset($context["title_visibility"]) ? $context["title_visibility"] : null);
            echo "\">
\t<h4 class=\"inner\"><span>";
            // line 15
            echo (isset($context["module_title"]) ? $context["module_title"] : null);
            echo "</span></h4>
</div>
";
        }
        // line 18
        echo "<div id=\"adbanner";
        echo (isset($context["mod_id"]) ? $context["mod_id"] : null);
        echo "\" class=\"rich_banner\">
\t";
        // line 19
        $context["count"] = 1;
        // line 20
        echo "\t";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["sections"]) ? $context["sections"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["section"]) {
            // line 21
            echo "
\t\t";
            // line 22
            $context["classes"] = (" col-md-" . (12 / (isset($context["columns"]) ? $context["columns"] : null)));
            // line 23
            echo "\t\t";
            if (((isset($context["columns"]) ? $context["columns"] : null) > 1)) {
                // line 24
                echo "\t\t\t";
                $context["classes"] = ((isset($context["classes"]) ? $context["classes"] : null) . " col-sm-6");
                // line 25
                echo "\t\t";
            }
            // line 26
            echo "
\t\t";
            // line 27
            $context["classes"] = (((((((isset($context["classes"]) ? $context["classes"] : null) . " rbanner") . (isset($context["mod_id"]) ? $context["mod_id"] : null)) . "-") . (isset($context["count"]) ? $context["count"] : null)) . " ") . $this->getAttribute($context["section"], "visibility", array()));
            // line 28
            echo "
\t\t";
            // line 29
            if (($this->getAttribute($context["section"], "url", array()) != "")) {
                // line 30
                echo "
\t\t<div class=\"banner ";
                // line 31
                echo (isset($context["classes"]) ? $context["classes"] : null);
                echo "\">
\t\t\t<a href=\"";
                // line 32
                echo $this->getAttribute($context["section"], "url", array());
                echo "\" class=\"";
                if (($this->getAttribute($context["section"], "image", array()) != "")) {
                    echo "image";
                }
                echo " banner_inner\">
\t\t\t\t";
                // line 33
                if (($this->getAttribute($context["section"], "image", array()) != "")) {
                    // line 34
                    echo "\t\t\t\t\t<img class=\"zoom_image img-responsive\" alt=\"\" src=\"";
                    echo $this->getAttribute($context["section"], "image", array());
                    echo "\" />
\t\t\t\t";
                }
                // line 36
                echo "\t\t\t\t<span class=\"info_wrapper\">
\t\t\t\t\t";
                // line 37
                echo $this->getAttribute($context["section"], "description", array());
                echo "
\t\t\t\t</span>
\t\t\t</a>
\t\t</div>

\t\t";
            } else {
                // line 43
                echo "
\t\t<div class=\"banner ";
                // line 44
                echo (isset($context["classes"]) ? $context["classes"] : null);
                echo "\">
\t\t\t<div class=\"";
                // line 45
                if (($this->getAttribute($context["section"], "image", array()) != "")) {
                    echo "image";
                }
                echo " banner_inner\">
\t\t\t\t";
                // line 46
                if (($this->getAttribute($context["section"], "image", array()) != "")) {
                    // line 47
                    echo "\t\t\t\t\t<img class=\"zoom_image img-responsive\" alt=\"\" src=\"";
                    echo $this->getAttribute($context["section"], "image", array());
                    echo "\" />
\t\t\t\t";
                }
                // line 49
                echo "\t\t\t\t<div class=\"info_wrapper\">
\t\t\t\t\t";
                // line 50
                echo $this->getAttribute($context["section"], "description", array());
                echo "
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t";
            }
            // line 56
            echo "
\t\t";
            // line 57
            $context["count"] = ((isset($context["count"]) ? $context["count"] : null) + 1);
            // line 58
            echo "
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['section'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "clearshop/template/extension/module/bannerplus.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 60,  184 => 58,  182 => 57,  179 => 56,  170 => 50,  167 => 49,  161 => 47,  159 => 46,  153 => 45,  149 => 44,  146 => 43,  137 => 37,  134 => 36,  128 => 34,  126 => 33,  118 => 32,  114 => 31,  111 => 30,  109 => 29,  106 => 28,  104 => 27,  101 => 26,  98 => 25,  95 => 24,  92 => 23,  90 => 22,  87 => 21,  82 => 20,  80 => 19,  75 => 18,  69 => 15,  64 => 14,  62 => 13,  58 => 11,  52 => 9,  47 => 8,  40 => 6,  31 => 5,  28 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% set count = 1 %}*/
/* <style>*/
/* {% for section in sections %}*/
/* 	{% if section.bgcolor != '' %}*/
/* 		.rich_banner .rbanner{{ mod_id }}-{{ count }} .banner_inner { background: {{ section.bgcolor }}; border: 0; border-radius: 5px; }*/
/* 		.rich_banner .rbanner{{ mod_id }}-{{ count }} .info_wrapper { color: #fff; }*/
/* 	{% endif %}*/
/* 	{{ section.css }}*/
/* 	{% set count = count + 1 %}*/
/* {% endfor %}*/
/* </style>*/
/* */
/* {% if module_title %}*/
/* <div class="contentset center {{ title_visibility }}">*/
/* 	<h4 class="inner"><span>{{ module_title }}</span></h4>*/
/* </div>*/
/* {% endif %}*/
/* <div id="adbanner{{ mod_id }}" class="rich_banner">*/
/* 	{% set count = 1 %}*/
/* 	{% for section in sections %}*/
/* */
/* 		{% set classes = ' col-md-' ~ 12 / columns %}*/
/* 		{% if columns > 1 %}*/
/* 			{% set classes = classes ~ ' col-sm-6' %}*/
/* 		{% endif %}*/
/* */
/* 		{% set classes =  classes ~ ' rbanner' ~ mod_id ~ '-' ~ count  ~ ' ' ~ section.visibility %}*/
/* */
/* 		{% if section.url != '' %}*/
/* */
/* 		<div class="banner {{ classes }}">*/
/* 			<a href="{{ section.url }}" class="{% if section.image != '' %}image{% endif %} banner_inner">*/
/* 				{% if section.image !='' %}*/
/* 					<img class="zoom_image img-responsive" alt="" src="{{ section.image }}" />*/
/* 				{% endif %}*/
/* 				<span class="info_wrapper">*/
/* 					{{ section.description }}*/
/* 				</span>*/
/* 			</a>*/
/* 		</div>*/
/* */
/* 		{% else %}*/
/* */
/* 		<div class="banner {{ classes }}">*/
/* 			<div class="{% if section.image != '' %}image{% endif %} banner_inner">*/
/* 				{% if section.image !='' %}*/
/* 					<img class="zoom_image img-responsive" alt="" src="{{ section.image }}" />*/
/* 				{% endif %}*/
/* 				<div class="info_wrapper">*/
/* 					{{ section.description }}*/
/* 				</div>*/
/* 			</div>*/
/* 		</div>*/
/* */
/* 		{% endif %}*/
/* */
/* 		{% set count = count + 1 %}*/
/* */
/* 	{% endfor %}*/
/* </div>*/
