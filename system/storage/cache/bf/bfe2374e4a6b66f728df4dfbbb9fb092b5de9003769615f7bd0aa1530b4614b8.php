<?php

/* clearshop/template/common/currency.twig */
class __TwigTemplate_ebfd245bc2211837c4e8da4f09807683be6510ee15ca4c6ec0506641d9eca0f0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["currencies"]) ? $context["currencies"] : null)) > 1)) {
            // line 2
            echo "
<form action=\"";
            // line 3
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-currency\">
\t<div class=\"btn-group dropdown\">
\t\t<button class=\"btn btn-link dropdown-toggle\" data-toggle=\"dropdown\">
\t\t";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 7
                echo "\t\t\t";
                if (($this->getAttribute($context["currency"], "symbol_left", array()) && ($this->getAttribute($context["currency"], "code", array()) == (isset($context["code"]) ? $context["code"] : null)))) {
                    echo " 
\t\t\t<strong>";
                    // line 8
                    echo $this->getAttribute($context["currency"], "symbol_left", array());
                    echo "</strong> 
\t\t\t";
                } elseif (($this->getAttribute(                // line 9
$context["currency"], "symbol_right", array()) && ($this->getAttribute($context["currency"], "code", array()) == (isset($context["code"]) ? $context["code"] : null)))) {
                    echo " 
\t\t\t<strong>";
                    // line 10
                    echo $this->getAttribute($context["currency"], "symbol_right", array());
                    echo "</strong> 
\t\t\t";
                }
                // line 12
                echo "\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "\t\t";
            echo (isset($context["text_currency"]) ? $context["text_currency"] : null);
            echo " <span class=\"caret\"></span></button>
\t\t<ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 16
                echo "\t\t\t
\t\t\t\t";
                // line 17
                if ($this->getAttribute($context["currency"], "symbol_left", array())) {
                    // line 18
                    echo "\t\t\t\t\t";
                    $context["currency_symbol"] = $this->getAttribute($context["currency"], "symbol_left", array());
                    // line 19
                    echo "\t\t\t\t";
                } else {
                    // line 20
                    echo "\t\t\t\t\t";
                    $context["currency_symbol"] = $this->getAttribute($context["currency"], "symbol_right", array());
                    // line 21
                    echo "\t\t\t\t";
                }
                // line 22
                echo "\t\t\t
\t\t\t\t";
                // line 23
                if (($this->getAttribute($context["currency"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                    // line 24
                    echo "\t\t\t\t\t<li><span class=\"btn btn-block current\">";
                    echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : null);
                    echo " ";
                    echo $this->getAttribute($context["currency"], "title", array());
                    echo "</span></li>
\t\t\t\t";
                } else {
                    // line 26
                    echo "\t\t\t\t\t<li><button class=\"currency-select btn btn-link btn-block\" type=\"button\" name=\"";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "\">";
                    echo (isset($context["currency_symbol"]) ? $context["currency_symbol"] : null);
                    echo " ";
                    echo $this->getAttribute($context["currency"], "title", array());
                    echo "</button></li>
\t\t\t\t";
                }
                // line 28
                echo "
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "\t\t</ul>
\t</div>
\t<input type=\"hidden\" name=\"code\" value=\"\" />
\t<input type=\"hidden\" name=\"redirect\" value=\"";
            // line 33
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "\" />
</form>

";
        }
        // line 36
        echo " 
";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/currency.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 36,  120 => 33,  115 => 30,  108 => 28,  98 => 26,  90 => 24,  88 => 23,  85 => 22,  82 => 21,  79 => 20,  76 => 19,  73 => 18,  71 => 17,  68 => 16,  64 => 15,  58 => 13,  52 => 12,  47 => 10,  43 => 9,  39 => 8,  34 => 7,  30 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if currencies|length > 1 %}*/
/* */
/* <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-currency">*/
/* 	<div class="btn-group dropdown">*/
/* 		<button class="btn btn-link dropdown-toggle" data-toggle="dropdown">*/
/* 		{% for currency in currencies %}*/
/* 			{% if currency.symbol_left and currency.code == code %} */
/* 			<strong>{{ currency.symbol_left }}</strong> */
/* 			{% elseif currency.symbol_right and currency.code == code %} */
/* 			<strong>{{ currency.symbol_right }}</strong> */
/* 			{% endif %}*/
/* 		{% endfor %}*/
/* 		{{ text_currency }} <span class="caret"></span></button>*/
/* 		<ul class="dropdown-menu dropdown-menu-right">*/
/* 			{% for currency in currencies %}*/
/* 			*/
/* 				{% if currency.symbol_left %}*/
/* 					{% set currency_symbol = currency.symbol_left %}*/
/* 				{% else %}*/
/* 					{% set currency_symbol = currency.symbol_right %}*/
/* 				{% endif %}*/
/* 			*/
/* 				{% if currency.code == code %}*/
/* 					<li><span class="btn btn-block current">{{ currency_symbol }} {{ currency.title }}</span></li>*/
/* 				{% else %}*/
/* 					<li><button class="currency-select btn btn-link btn-block" type="button" name="{{ currency.code }}">{{ currency_symbol }} {{ currency.title }}</button></li>*/
/* 				{% endif %}*/
/* */
/* 			{% endfor %}*/
/* 		</ul>*/
/* 	</div>*/
/* 	<input type="hidden" name="code" value="" />*/
/* 	<input type="hidden" name="redirect" value="{{ redirect }}" />*/
/* </form>*/
/* */
/* {% endif %} */
/* */
