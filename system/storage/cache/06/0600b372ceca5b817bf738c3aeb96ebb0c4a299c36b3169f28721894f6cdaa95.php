<?php

/* clearshop/template/product/category.twig */
class __TwigTemplate_9b925f897ed3b5aee8bec49dda00e3f142f893a20cb0eb1a5b32b0cc305f2975 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

\t<div class=\"maintop-container\">

\t\t<div id=\"product-category\" class=\"container\">

\t\t\t<ul class=\"breadcrumb\">
\t\t\t\t";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 9
            echo "\t\t\t\t<li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "\t\t\t</ul>

\t\t\t";
        // line 13
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "

\t\t\t<header class=\"page-header visible-xs\">
\t\t\t\t<h3>";
        // line 16
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
\t\t\t</header>

\t\t\t";
        // line 19
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 20
            echo "\t\t\t\t<div class=\"catdesc visible-xs\">
\t\t\t\t";
            // line 21
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "
\t\t\t\t</div>
\t\t\t";
        }
        // line 24
        echo "
\t\t\t";
        // line 25
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 26
            echo "\t\t\t\t";
            $context["class"] = "col-sm-6 middle sideleft";
            // line 27
            echo "\t\t\t";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 28
            echo "\t\t\t\t";
            $context["class"] = "col-sm-9";
            // line 29
            echo "\t\t\t";
        } else {
            // line 30
            echo "\t\t\t\t";
            $context["class"] = "col-sm-12";
            // line 31
            echo "\t\t\t";
        }
        // line 32
        echo "
\t\t\t<div class=\"row\">

\t\t\t\t";
        // line 35
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "

\t\t\t\t<div id=\"content\" class=\"";
        // line 37
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\" role=\"main\">

\t\t\t\t\t<div class=\"mainborder\">

\t\t\t\t\t\t<div class=\"page-header hidden-xs\">
\t\t\t\t\t\t\t<h1>";
        // line 42
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"category-details clearfix\">

\t\t\t\t\t\t\t";
        // line 47
        if (((isset($context["thumb"]) ? $context["thumb"] : null) || (isset($context["description"]) ? $context["description"] : null))) {
            // line 48
            echo "
\t\t\t\t\t\t\t\t<div class=\"category-info hidden-xs\">
\t\t\t\t\t\t\t\t\t";
            // line 50
            if ((isset($context["thumb"]) ? $context["thumb"] : null)) {
                // line 51
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"image\"><img src=\"";
                echo (isset($context["thumb"]) ? $context["thumb"] : null);
                echo "\" class=\"img-responsive\" alt=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" /></div>
\t\t\t\t\t\t\t\t\t";
            }
            // line 53
            echo "\t\t\t\t\t\t\t\t\t";
            if ((isset($context["description"]) ? $context["description"] : null)) {
                // line 54
                echo "\t\t\t\t\t\t\t\t\t\t";
                echo (isset($context["description"]) ? $context["description"] : null);
                echo "
\t\t\t\t\t\t\t\t\t";
            }
            // line 56
            echo "\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t";
        }
        // line 59
        echo "

\t\t\t\t\t\t\t";
        // line 61
        if ((((isset($context["categories"]) ? $context["categories"] : null) && (isset($context["clearshop_subcat_status"]) ? $context["clearshop_subcat_status"] : null)) && ((isset($context["clearshop_subcat_status"]) ? $context["clearshop_subcat_status"] : null) == 1))) {
            // line 62
            echo "
\t\t\t\t\t\t\t\t";
            // line 63
            if (((isset($context["clearshop_subcat_thumbnails_status"]) ? $context["clearshop_subcat_thumbnails_status"] : null) == 1)) {
                // line 64
                echo "
\t\t\t\t\t\t\t\t<div class=\"clearfix\" id=\"subcategories\">
\t\t\t\t\t\t\t\t  <ul class=\"category-list\" aria-labelledby=\"subcategories\">
\t\t\t\t\t\t\t\t    ";
                // line 67
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 68
                    echo "\t\t\t\t\t\t\t\t    <li class=\"subcat_thumb\">
\t\t\t\t\t\t\t\t    \t<a href=\"";
                    // line 69
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "\">
\t\t\t\t\t\t\t\t    \t\t";
                    // line 70
                    if ($this->getAttribute($context["category"], "thumb", array())) {
                        // line 71
                        echo "\t\t\t\t\t\t\t\t    \t\t\t<img src=\"";
                        echo $this->getAttribute($context["category"], "thumb", array());
                        echo "\" class=\"img-responsive\" alt=\"";
                        echo $this->getAttribute($context["category"], "name", array());
                        echo "\" />
\t\t\t\t\t\t\t\t    \t\t";
                    } else {
                        // line 73
                        echo "\t\t\t\t\t\t\t\t    \t\t\t<img src=\"image/no_image.jpg\" class=\"img-responsive\" width=\"";
                        echo (isset($context["clearshop_subcat_thumb_width"]) ? $context["clearshop_subcat_thumb_width"] : null);
                        echo "\" height=\"";
                        echo (isset($context["clearshop_subcat_thumb_height"]) ? $context["clearshop_subcat_thumb_height"] : null);
                        echo "\" />
\t\t\t\t\t\t\t\t    \t\t";
                    }
                    // line 75
                    echo "\t\t\t\t\t\t\t\t    \t\t<span>";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</span>
\t\t\t\t\t\t\t\t    \t</a>
\t\t\t\t\t\t\t\t    </li>
\t\t\t\t\t\t\t\t    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 79
                echo "\t\t\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t";
            } else {
                // line 83
                echo "
\t\t\t\t\t\t\t\t\t<ul class=\"category-list\">
\t\t\t\t\t\t\t\t\t\t";
                // line 85
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                    // line 86
                    echo "\t\t\t\t\t\t\t\t\t\t<li class=\"subcat\"><a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a></li>
\t\t\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 88
                echo "\t\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t";
            }
            // line 91
            echo "
\t\t\t\t\t\t\t";
        }
        // line 93
        echo "
\t\t\t\t\t\t</div>

\t\t\t\t\t\t";
        // line 96
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 97
            echo "
\t\t\t\t\t\t\t";
            // line 99
            echo "
\t\t\t\t\t\t\t<div class=\"product-filter\">

\t\t\t\t\t\t\t\t<div class=\"btn-group display\">
\t\t\t\t\t\t\t\t  <button type=\"button\" id=\"list-view\" class=\"btn btn-link\" data-toggle=\"tooltip\" title=\"";
            // line 103
            echo (isset($context["button_list"]) ? $context["button_list"] : null);
            echo "\"><i class=\"fa fa-th-list\"></i></button>
\t\t\t\t\t\t\t\t  <button type=\"button\" id=\"grid-view\" class=\"btn btn-link\" data-toggle=\"tooltip\" title=\"";
            // line 104
            echo (isset($context["button_grid"]) ? $context["button_grid"] : null);
            echo "\"><i class=\"fa fa-th\"></i></button>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t<span class=\"product-compare\"><a href=\"";
            // line 107
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\" id=\"compare-total\" data-toggle=\"tooltip\" class=\"btn btn-link\" title=\"";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo "\"><i class=\"fa fa-exchange\"></i></a></span>

\t\t\t\t\t\t\t\t<div class=\"list-options\"> 

\t\t\t\t\t\t\t\t\t<div class=\"sort\">

\t\t\t\t\t\t\t\t\t\t<i class=\"fa fa-sort-amount-asc\" data-toggle=\"tooltip\" title=\"";
            // line 113
            echo (isset($context["text_sort"]) ? $context["text_sort"] : null);
            echo "\"></i>

\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown\">
\t\t\t\t\t\t\t\t\t\t  <ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t\t\t\t\t\t\t\t  \t";
            // line 117
            $context["current_sort"] = "";
            // line 118
            echo "\t\t\t\t\t\t\t\t\t\t  \t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 119
                echo "\t\t\t\t\t\t\t\t\t\t  \t";
                if (($this->getAttribute($context["sorts"], "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
                    // line 120
                    echo "\t\t\t\t\t\t\t\t\t\t  \t<li><a href=\"";
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\"><b>";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</b></a></li>
\t\t\t\t\t\t\t\t\t\t  \t";
                    // line 121
                    $context["current_sort"] = $this->getAttribute($context["sorts"], "text", array());
                    // line 122
                    echo "\t\t\t\t\t\t\t\t\t\t  \t";
                } else {
                    // line 123
                    echo "\t\t\t\t\t\t\t\t\t\t  \t<li><a href=\"";
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</a></li>
\t\t\t\t\t\t\t\t\t\t  \t";
                }
                // line 125
                echo "\t\t\t\t\t\t\t\t\t\t  \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 126
            echo "\t\t\t\t\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t\t\t\t\t  <button type=\"button\" id=\"input-sort\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\t\t    ";
            // line 128
            echo (isset($context["current_sort"]) ? $context["current_sort"] : null);
            echo " <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t\t\t  </button>
\t\t\t\t\t\t\t\t\t\t  
\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t<div class=\"limit\">

\t\t\t\t\t\t\t\t\t\t<div class=\"dropdown\">
\t\t\t\t\t\t\t\t\t\t  <ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t\t\t\t\t\t\t\t  \t";
            // line 139
            $context["current_limit"] = "";
            // line 140
            echo "\t\t\t\t\t\t\t\t\t\t  \t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 141
                echo "\t\t\t\t\t\t\t\t\t\t  \t";
                if (($this->getAttribute($context["limits"], "value", array()) == (isset($context["limit"]) ? $context["limit"] : null))) {
                    // line 142
                    echo "\t\t\t\t\t\t\t\t\t\t  \t<li><a href=\"";
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\"><b>";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</b></a></li>
\t\t\t\t\t\t\t\t\t\t  \t";
                    // line 143
                    $context["current_limit"] = $this->getAttribute($context["limits"], "text", array());
                    // line 144
                    echo "\t\t\t\t\t\t\t\t\t\t  \t";
                } else {
                    // line 145
                    echo "\t\t\t\t\t\t\t\t\t\t  \t<li><a href=\"";
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</a></li>
\t\t\t\t\t\t\t\t\t\t  \t";
                }
                // line 147
                echo "\t\t\t\t\t\t\t\t\t\t  \t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 148
            echo "\t\t\t\t\t\t\t\t\t\t  </ul>
\t\t\t\t\t\t\t\t\t\t  <button type=\"button\" id=\"input-limit\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
\t\t\t\t\t\t\t\t\t\t    ";
            // line 150
            echo (isset($context["current_limit"]) ? $context["current_limit"] : null);
            echo " <span class=\"caret\"></span>
\t\t\t\t\t\t\t\t\t\t  </button>
\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t</div> <!-- .list-options -->

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t";
            // line 161
            echo "
\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t";
            // line 163
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 164
                echo "\t\t\t\t\t\t\t\t<div class=\"product-layout product-list col-xs-12\">
\t\t\t\t\t\t\t\t\t<div class=\"product-thumb\">
\t\t\t\t\t\t\t\t\t\t<div class=\"inner\">
\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t<div class=\"image hover_fade_in_back\">
\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 170
                if ((($this->getAttribute($context["product"], "thumb", array()) && $this->getAttribute($context["product"], "thumb_swap", array())) && ((isset($context["clearshop_rollover_images"]) ? $context["clearshop_rollover_images"] : null) == 1))) {
                    // line 171
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"front-image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 172
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\"/></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"back-image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    // line 175
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb_swap", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\"/></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t";
                } elseif ($this->getAttribute(                // line 177
$context["product"], "thumb", array())) {
                    // line 178
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" /></a>
\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 180
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 181
                if ($this->getAttribute($context["product"], "special", array())) {
                    // line 182
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"onsale\"><span>";
                    echo (isset($context["text_onsale"]) ? $context["text_onsale"] : null);
                    echo "</span></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 184
                echo "
\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">
\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"name\"><a href=\"";
                // line 190
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></h4>
\t\t\t\t\t\t\t\t\t\t\t\t\t<p class=\"description\">";
                // line 191
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>

\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 193
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 194
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 195
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 196
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } else {
                        // line 198
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span> <span class=\"price-old\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 200
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    if ($this->getAttribute($context["product"], "tax", array())) {
                        // line 201
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-tax\">";
                        echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                        echo " ";
                        echo $this->getAttribute($context["product"], "tax", array());
                        echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 203
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 205
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 206
                if ($this->getAttribute($context["product"], "rating", array())) {
                    // line 207
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                    // line 208
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(5);
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 209
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                            // line 210
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                        } else {
                            // line 212
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                        }
                        // line 214
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 215
                    echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 217
                echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"cart\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" onclick=\"cart.add('";
                // line 219
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array(), "array");
                echo "');\" class=\"btn btn-sm btn-primary\">";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"links\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-link btn-sm wishlist\" title=\"";
                // line 222
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-heart-o\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-link btn-sm compare\" title=\"";
                // line 223
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\"><i class=\"fa fa-exchange\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a id=\"qv";
                // line 224
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\" title=\"";
                echo (isset($context["text_quickview"]) ? $context["text_quickview"] : null);
                echo "\" data-toggle=\"tooltip\" class=\"btn btn-link quickview\" rel=\"mp-quickview\" href=\"index.php?route=product/quickview&product_id=";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "\" ><i class=\"fa fa-window-restore\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 235
            echo "\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t  <div class=\"col-sm-6 text-left\">";
            // line 238
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "</div>
\t\t\t\t\t\t\t  <div class=\"col-sm-6 text-right\">";
            // line 239
            echo (isset($context["results"]) ? $context["results"] : null);
            echo "</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t";
        }
        // line 242
        echo " ";
        // line 243
        echo "

\t\t\t\t\t\t";
        // line 246
        echo "
\t\t\t\t\t\t";
        // line 247
        if (( !(isset($context["categories"]) ? $context["categories"] : null) &&  !(isset($context["products"]) ? $context["products"] : null))) {
            // line 248
            echo "
\t\t\t\t\t\t\t<div class=\"content empty\">

\t\t\t\t\t\t\t\t<p>";
            // line 251
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>

\t\t\t\t\t\t\t\t<div class=\"buttons\">
\t\t\t\t\t\t\t\t\t<a href=\"";
            // line 254
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a>
\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t";
        }
        // line 260
        echo "
\t\t\t\t\t</div>

\t\t\t\t</div> ";
        // line 264
        echo "\t\t\t\t
\t\t\t\t";
        // line 265
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "

\t\t\t</div> ";
        // line 268
        echo "
\t\t</div> ";
        // line 270
        echo "
\t</div> ";
        // line 272
        echo "\t
\t";
        // line 273
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "

";
        // line 275
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "clearshop/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  675 => 275,  670 => 273,  667 => 272,  664 => 270,  661 => 268,  656 => 265,  653 => 264,  648 => 260,  637 => 254,  631 => 251,  626 => 248,  624 => 247,  621 => 246,  617 => 243,  615 => 242,  608 => 239,  604 => 238,  599 => 235,  578 => 224,  572 => 223,  566 => 222,  556 => 219,  552 => 217,  548 => 215,  542 => 214,  538 => 212,  534 => 210,  531 => 209,  527 => 208,  524 => 207,  522 => 206,  519 => 205,  515 => 203,  507 => 201,  504 => 200,  496 => 198,  490 => 196,  488 => 195,  485 => 194,  483 => 193,  478 => 191,  472 => 190,  464 => 184,  458 => 182,  456 => 181,  453 => 180,  441 => 178,  439 => 177,  428 => 175,  416 => 172,  413 => 171,  411 => 170,  403 => 164,  399 => 163,  395 => 161,  382 => 150,  378 => 148,  372 => 147,  364 => 145,  361 => 144,  359 => 143,  352 => 142,  349 => 141,  344 => 140,  342 => 139,  328 => 128,  324 => 126,  318 => 125,  310 => 123,  307 => 122,  305 => 121,  298 => 120,  295 => 119,  290 => 118,  288 => 117,  281 => 113,  270 => 107,  264 => 104,  260 => 103,  254 => 99,  251 => 97,  249 => 96,  244 => 93,  240 => 91,  235 => 88,  222 => 86,  218 => 85,  214 => 83,  208 => 79,  197 => 75,  189 => 73,  181 => 71,  179 => 70,  173 => 69,  170 => 68,  166 => 67,  161 => 64,  159 => 63,  156 => 62,  154 => 61,  150 => 59,  145 => 56,  139 => 54,  136 => 53,  128 => 51,  126 => 50,  122 => 48,  120 => 47,  112 => 42,  104 => 37,  99 => 35,  94 => 32,  91 => 31,  88 => 30,  85 => 29,  82 => 28,  79 => 27,  76 => 26,  74 => 25,  71 => 24,  65 => 21,  62 => 20,  60 => 19,  54 => 16,  48 => 13,  44 => 11,  33 => 9,  29 => 8,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* 	<div class="maintop-container">*/
/* */
/* 		<div id="product-category" class="container">*/
/* */
/* 			<ul class="breadcrumb">*/
/* 				{% for breadcrumb in breadcrumbs %}*/
/* 				<li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/* 				{% endfor %}*/
/* 			</ul>*/
/* */
/* 			{{ content_top }}*/
/* */
/* 			<header class="page-header visible-xs">*/
/* 				<h3>{{ heading_title }}</h3>*/
/* 			</header>*/
/* */
/* 			{% if description %}*/
/* 				<div class="catdesc visible-xs">*/
/* 				{{ description }}*/
/* 				</div>*/
/* 			{% endif %}*/
/* */
/* 			{% if column_left and column_right %}*/
/* 				{% set class = 'col-sm-6 middle sideleft' %}*/
/* 			{% elseif column_left or column_right %}*/
/* 				{% set class = 'col-sm-9' %}*/
/* 			{% else %}*/
/* 				{% set class = 'col-sm-12' %}*/
/* 			{% endif %}*/
/* */
/* 			<div class="row">*/
/* */
/* 				{{ column_left }}*/
/* */
/* 				<div id="content" class="{{ class }}" role="main">*/
/* */
/* 					<div class="mainborder">*/
/* */
/* 						<div class="page-header hidden-xs">*/
/* 							<h1>{{ heading_title }}</h1>*/
/* 						</div>*/
/* */
/* 						<div class="category-details clearfix">*/
/* */
/* 							{% if thumb or description %}*/
/* */
/* 								<div class="category-info hidden-xs">*/
/* 									{% if thumb %}*/
/* 										<div class="image"><img src="{{ thumb }}" class="img-responsive" alt="{{ heading_title }}" /></div>*/
/* 									{% endif %}*/
/* 									{% if description %}*/
/* 										{{ description }}*/
/* 									{% endif %}*/
/* 								</div>*/
/* */
/* 							{% endif %}*/
/* */
/* */
/* 							{% if categories and clearshop_subcat_status and clearshop_subcat_status == 1 %}*/
/* */
/* 								{% if clearshop_subcat_thumbnails_status == 1 %}*/
/* */
/* 								<div class="clearfix" id="subcategories">*/
/* 								  <ul class="category-list" aria-labelledby="subcategories">*/
/* 								    {% for category in categories %}*/
/* 								    <li class="subcat_thumb">*/
/* 								    	<a href="{{ category.href }}" title="{{ category.name }}">*/
/* 								    		{% if category.thumb %}*/
/* 								    			<img src="{{ category.thumb }}" class="img-responsive" alt="{{ category.name }}" />*/
/* 								    		{% else %}*/
/* 								    			<img src="image/no_image.jpg" class="img-responsive" width="{{ clearshop_subcat_thumb_width }}" height="{{ clearshop_subcat_thumb_height }}" />*/
/* 								    		{% endif %}*/
/* 								    		<span>{{ category.name }}</span>*/
/* 								    	</a>*/
/* 								    </li>*/
/* 								    {% endfor %}*/
/* 								  </ul>*/
/* 								</div>*/
/* */
/* 								{% else %}*/
/* */
/* 									<ul class="category-list">*/
/* 										{% for category in categories %}*/
/* 										<li class="subcat"><a href="{{ category.href }}" title="{{ category.name }}">{{ category.name }}</a></li>*/
/* 										{% endfor %}*/
/* 									</ul>*/
/* */
/* 								{% endif %}*/
/* */
/* 							{% endif %}*/
/* */
/* 						</div>*/
/* */
/* 						{% if products %}*/
/* */
/* 							{# Grid/Lis view, filters #}*/
/* */
/* 							<div class="product-filter">*/
/* */
/* 								<div class="btn-group display">*/
/* 								  <button type="button" id="list-view" class="btn btn-link" data-toggle="tooltip" title="{{ button_list }}"><i class="fa fa-th-list"></i></button>*/
/* 								  <button type="button" id="grid-view" class="btn btn-link" data-toggle="tooltip" title="{{ button_grid }}"><i class="fa fa-th"></i></button>*/
/* 								</div>*/
/* */
/* 								<span class="product-compare"><a href="{{ compare }}" id="compare-total" data-toggle="tooltip" class="btn btn-link" title="{{ text_compare }}"><i class="fa fa-exchange"></i></a></span>*/
/* */
/* 								<div class="list-options"> */
/* */
/* 									<div class="sort">*/
/* */
/* 										<i class="fa fa-sort-amount-asc" data-toggle="tooltip" title="{{ text_sort }}"></i>*/
/* */
/* 										<div class="dropdown">*/
/* 										  <ul class="dropdown-menu dropdown-menu-right">*/
/* 										  	{% set current_sort = '' %}*/
/* 										  	{% for sorts in sorts %}*/
/* 										  	{% if sorts.value == '%s-%s'|format(sort, order) %}*/
/* 										  	<li><a href="{{ sorts.href }}"><b>{{ sorts.text }}</b></a></li>*/
/* 										  	{% set current_sort = sorts.text %}*/
/* 										  	{% else %}*/
/* 										  	<li><a href="{{ sorts.href }}">{{ sorts.text }}</a></li>*/
/* 										  	{% endif %}*/
/* 										  	{% endfor %}*/
/* 										  </ul>*/
/* 										  <button type="button" id="input-sort" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/
/* 										    {{ current_sort }} <span class="caret"></span>*/
/* 										  </button>*/
/* 										  */
/* 										</div>*/
/* */
/* 									</div>*/
/* */
/* 									<div class="limit">*/
/* */
/* 										<div class="dropdown">*/
/* 										  <ul class="dropdown-menu dropdown-menu-right">*/
/* 										  	{% set current_limit = '' %}*/
/* 										  	{% for limits in limits %}*/
/* 										  	{% if limits.value == limit %}*/
/* 										  	<li><a href="{{ limits.href }}"><b>{{ limits.text }}</b></a></li>*/
/* 										  	{% set current_limit = limits.text %}*/
/* 										  	{% else %}*/
/* 										  	<li><a href="{{ limits.href }}">{{ limits.text }}</a></li>*/
/* 										  	{% endif %}*/
/* 										  	{% endfor %}*/
/* 										  </ul>*/
/* 										  <button type="button" id="input-limit" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">*/
/* 										    {{ current_limit }} <span class="caret"></span>*/
/* 										  </button>*/
/* 										</div>*/
/* */
/* 									</div>*/
/* */
/* 								</div> <!-- .list-options -->*/
/* */
/* 							</div>*/
/* */
/* 							{# Product list (Default to Grid) #}*/
/* */
/* 							<div class="row">*/
/* 								{% for product in products %}*/
/* 								<div class="product-layout product-list col-xs-12">*/
/* 									<div class="product-thumb">*/
/* 										<div class="inner">*/
/* 											*/
/* 											<div class="image hover_fade_in_back">*/
/* 												*/
/* 												{% if product.thumb and product.thumb_swap and clearshop_rollover_images == 1 %}*/
/* 													<div class="front-image">*/
/* 														<a href="{{ product.href }}"><img src="{{ product.thumb }}" title="{{ product.name }}" class="img-responsive" alt="{{ product.name }}"/></a>*/
/* 													</div>*/
/* 													<div class="back-image">*/
/* 														<a href="{{ product.href }}"><img src="{{ product.thumb_swap }}" title="{{ product.name }}" class="img-responsive" alt="{{ product.name }}"/></a>*/
/* 													</div>*/
/* 												{% elseif product.thumb %}*/
/* 													<a href="{{ product.href }}"><img src="{{ product.thumb }}" title="{{ product.name }}" class="img-responsive" alt="{{ product.name }}" /></a>*/
/* 												{% endif %}*/
/* */
/* 												{% if product.special %}*/
/* 													<div class="onsale"><span>{{ text_onsale }}</span></div>*/
/* 													{% endif %}*/
/* */
/* 											</div>*/
/* */
/* 											<div>*/
/* 												<div class="caption">*/
/* 													*/
/* 													<h4 class="name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* 													<p class="description">{{ product.description }}</p>*/
/* */
/* 													{% if product.price %}*/
/* 														<div class="price">*/
/* 															{% if not product.special %}*/
/* 																{{ product.price }}*/
/* 															{% else %}*/
/* 																<span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/* 															{% endif %}*/
/* 															{% if product.tax %}*/
/* 																<span class="price-tax">{{ text_tax }} {{ product.tax }}</span>*/
/* 															{% endif %}*/
/* 														</div>*/
/* 														{% endif %}*/
/* */
/* 														{% if product.rating %}*/
/* 														<div class="rating">*/
/* 														  {% for i in 5 %}*/
/* 														  {% if product.rating < i %}*/
/* 														  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 														  {% else %}*/
/* 														  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 														  {% endif %}*/
/* 														  {% endfor %}*/
/* 														</div>*/
/* 													{% endif %}*/
/* */
/* 													<div class="cart">*/
/* 														<button type="button" onclick="cart.add('{{ product.product_id }}', '{{ product['minimum'] }}');" class="btn btn-sm btn-primary">{{ button_cart }}</button>*/
/* 													</div>*/
/* 													<div class="links">*/
/* 														<button type="button" data-toggle="tooltip" class="btn btn-link btn-sm wishlist" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart-o"></i></button>*/
/* 														<button type="button" data-toggle="tooltip" class="btn btn-link btn-sm compare" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-exchange"></i></button>*/
/* 														<a id="qv{{ product.product_id }}" title="{{ text_quickview }}" data-toggle="tooltip" class="btn btn-link quickview" rel="mp-quickview" href="index.php?route=product/quickview&product_id={{ product.product_id }}" ><i class="fa fa-window-restore"></i></a>*/
/* 													</div>*/
/* */
/* 												</div>*/
/* */
/* 											</div>*/
/* */
/* 										</div>*/
/* 									</div>*/
/* 								</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* */
/* 							<div class="row">*/
/* 							  <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/* 							  <div class="col-sm-6 text-right">{{ results }}</div>*/
/* 							</div>*/
/* 						*/
/* 						{% endif %} {# products END #}*/
/* */
/* */
/* 						{# If Category is empty #}*/
/* */
/* 						{% if not categories and not products %}*/
/* */
/* 							<div class="content empty">*/
/* */
/* 								<p>{{ text_empty }}</p>*/
/* */
/* 								<div class="buttons">*/
/* 									<a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a>*/
/* 								</div>*/
/* */
/* 							</div>*/
/* */
/* 						{% endif %}*/
/* */
/* 					</div>*/
/* */
/* 				</div> {# #content #}*/
/* 				*/
/* 				{{ column_right }}*/
/* */
/* 			</div> {# .row #}*/
/* */
/* 		</div> {# .container #}*/
/* */
/* 	</div> {# #maintop-container #}*/
/* 	*/
/* 	{{ content_bottom }}*/
/* */
/* {{ footer }}*/
