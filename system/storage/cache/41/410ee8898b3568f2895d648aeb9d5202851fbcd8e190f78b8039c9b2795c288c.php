<?php

/* clearshop/template/common/menumob.twig */
class __TwigTemplate_b8b1aaa44eb1ec09b21c85f0e33f454cf1efd7499b9f22123f7a0d4613eea4c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if (((isset($context["direction"]) ? $context["direction"] : null) == "rtl")) {
            // line 3
            echo "\t";
            $context["position"] = "navmenu-fixed-right";
        } else {
            // line 5
            echo "\t";
            $context["position"] = "navmenu-fixed-left";
        }
        // line 7
        echo "
<nav id=\"mobmenu\" class=\"navbar navbar-default navmenu ";
        // line 8
        echo (isset($context["position"]) ? $context["position"] : null);
        echo " offcanvas offcanvas-sm offcanvas-md hidden-lg\" role=\"navigation\">

\t<div class=\"navcontainer\">

\t\t<div class=\"navbar-collapse\">

\t\t\t\t<ul class=\"nav navbar-nav\">

\t\t\t\t\t<li><a class=\"closesidebar\" onclick=\"\$('#mobmenu').offcanvas('hide');\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"Close\"><i class=\"fa fa-angle-left\"></i></a></li>

\t\t\t\t\t";
        // line 18
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 19
            echo "
\t\t\t\t\t<li class=\"dropdown login-options\">
\t\t\t\t\t\t<div class=\"btn-group wishlist\" role=\"group\" aria-label=\"...\">
\t\t\t\t\t\t\t<a href=\"";
            // line 22
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\" class=\"btn btn-default\"><i class=\"fa fa-heart-o\"></i>&nbsp; ";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t<li class=\"dropdown account-options\">
\t\t\t\t\t
\t\t\t\t\t\t<a href=\"";
            // line 27
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\" title=\"";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user-circle-o\"></i> ";
            echo (isset($context["text_welcome"]) ? $context["text_welcome"] : null);
            echo ", ";
            echo (isset($context["customer_firstname"]) ? $context["customer_firstname"] : null);
            echo " <span class=\"caret\"></span></a>
\t\t\t\t\t\t<ul class=\"dropdown-menu dropdown-menu-right\">
\t\t\t\t\t\t\t<li><a href=\"";
            // line 29
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 30
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 31
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\">";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 32
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\">";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
\t\t\t\t\t\t\t<li><a href=\"";
            // line 33
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\"><i class=\"fa fa-sign-out\"></i> ";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
\t\t\t\t\t\t </ul>
\t\t\t\t\t</li>

\t\t\t\t\t";
        } else {
            // line 38
            echo "
\t\t\t\t\t<li class=\"dropdown login-options\">
\t\t\t\t\t\t<div class=\"btn-group\" role=\"group\" aria-label=\"...\">
\t\t\t\t\t\t\t<a href=\"";
            // line 41
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\" class=\"btn btn-default\"><i class=\"fa fa-user-circle-o\"></i> ";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a>
\t\t\t\t\t\t\t<a href=\"";
            // line 42
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\" class=\"btn btn-default\">";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</li>
\t\t\t\t\t
\t\t\t\t\t";
        }
        // line 47
        echo "
\t\t\t\t";
        // line 48
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo "

\t\t\t\t<li>
\t\t\t\t\t<div class=\"clearfix browse-options\">
\t\t\t\t\t\t";
        // line 52
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "
\t\t\t\t\t\t";
        // line 53
        echo (isset($context["currency"]) ? $context["currency"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t</li>

\t\t\t\t<li>

\t\t\t\t\t<div class=\"social\">

\t\t\t\t\t";
        // line 61
        if ((isset($context["clearshop_social"]) ? $context["clearshop_social"] : null)) {
            // line 62
            echo "\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_social"]) ? $context["clearshop_social"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 63
                echo "\t\t\t\t\t\t";
                if ($this->getAttribute($context["value"], "url", array())) {
                    // line 64
                    echo "\t\t\t\t\t\t\t<a href=\"";
                    echo $this->getAttribute($context["value"], "url", array());
                    echo "\" class=\"fa fa-";
                    echo $this->getAttribute($context["value"], "class", array());
                    echo "\" target=\"";
                    echo $this->getAttribute($context["value"], "target", array());
                    echo "\"></a>
\t\t\t\t\t\t\t";
                }
                // line 66
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 67
            echo "\t\t\t\t\t";
        }
        // line 68
        echo "
\t\t\t\t\t";
        // line 69
        if ((isset($context["clearshop_custom_icon"]) ? $context["clearshop_custom_icon"] : null)) {
            // line 70
            echo "\t\t\t\t\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["clearshop_custom_icon"]) ? $context["clearshop_custom_icon"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 71
                echo "\t\t\t\t\t\t\t";
                if (($this->getAttribute($context["value"], "class", array()) != "")) {
                    // line 72
                    echo "\t\t\t\t\t\t\t\t";
                    if (($this->getAttribute($context["value"], "class", array()) != "")) {
                        // line 73
                        echo "\t\t\t\t\t\t\t\t\t<a href=\"";
                        echo $this->getAttribute($context["value"], "url", array());
                        echo "\" class=\"fa fa-";
                        echo $this->getAttribute($context["value"], "class", array());
                        echo "\" target=\"";
                        echo $this->getAttribute($context["value"], "target", array());
                        echo "\"></a>
\t\t\t\t\t\t\t\t";
                    } else {
                        // line 75
                        echo "\t\t\t\t\t\t\t\t\t<a class=\"fa fa-";
                        echo $this->getAttribute($context["value"], "class", array());
                        echo "\"></a>
\t\t\t\t\t\t\t\t";
                    }
                    // line 77
                    echo "\t\t\t\t\t\t\t";
                }
                // line 78
                echo "\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 79
            echo "\t\t\t\t\t";
        }
        // line 80
        echo "
\t\t\t\t\t</div>

\t\t\t\t</li>

\t\t\t</ul>

\t\t</div>

\t</div>

</nav><!-- #navbar -->";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/menumob.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 80,  223 => 79,  217 => 78,  214 => 77,  208 => 75,  198 => 73,  195 => 72,  192 => 71,  187 => 70,  185 => 69,  182 => 68,  179 => 67,  173 => 66,  163 => 64,  160 => 63,  155 => 62,  153 => 61,  142 => 53,  138 => 52,  131 => 48,  128 => 47,  118 => 42,  112 => 41,  107 => 38,  97 => 33,  91 => 32,  85 => 31,  79 => 30,  73 => 29,  62 => 27,  52 => 22,  47 => 19,  45 => 18,  32 => 8,  29 => 7,  25 => 5,  21 => 3,  19 => 2,);
    }
}
/* {# mobile menu file for header.twig #}*/
/* {% if direction =='rtl' %}*/
/* 	{% set position = "navmenu-fixed-right" %}*/
/* {% else %}*/
/* 	{% set position = "navmenu-fixed-left" %}*/
/* {% endif %}*/
/* */
/* <nav id="mobmenu" class="navbar navbar-default navmenu {{ position }} offcanvas offcanvas-sm offcanvas-md hidden-lg" role="navigation">*/
/* */
/* 	<div class="navcontainer">*/
/* */
/* 		<div class="navbar-collapse">*/
/* */
/* 				<ul class="nav navbar-nav">*/
/* */
/* 					<li><a class="closesidebar" onclick="$('#mobmenu').offcanvas('hide');" data-toggle="tooltip" data-placement="left" title="Close"><i class="fa fa-angle-left"></i></a></li>*/
/* */
/* 					{% if logged %}*/
/* */
/* 					<li class="dropdown login-options">*/
/* 						<div class="btn-group wishlist" role="group" aria-label="...">*/
/* 							<a href="{{ wishlist }}" class="btn btn-default"><i class="fa fa-heart-o"></i>&nbsp; {{ text_wishlist }}</a>*/
/* 						</div>*/
/* 					</li>*/
/* 					<li class="dropdown account-options">*/
/* 					*/
/* 						<a href="{{ account }}" title="{{ text_account }}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-circle-o"></i> {{ text_welcome }}, {{ customer_firstname }} <span class="caret"></span></a>*/
/* 						<ul class="dropdown-menu dropdown-menu-right">*/
/* 							<li><a href="{{ account }}">{{ text_account }}</a></li>*/
/* 							<li><a href="{{ order }}">{{ text_order }}</a></li>*/
/* 							<li><a href="{{ transaction }}">{{ text_transaction }}</a></li>*/
/* 							<li><a href="{{ download }}">{{ text_download }}</a></li>*/
/* 							<li><a href="{{ logout }}"><i class="fa fa-sign-out"></i> {{ text_logout }}</a></li>*/
/* 						 </ul>*/
/* 					</li>*/
/* */
/* 					{% else %}*/
/* */
/* 					<li class="dropdown login-options">*/
/* 						<div class="btn-group" role="group" aria-label="...">*/
/* 							<a href="{{ register }}" class="btn btn-default"><i class="fa fa-user-circle-o"></i> {{ text_register }}</a>*/
/* 							<a href="{{ login }}" class="btn btn-default">{{ text_login }}</a>*/
/* 						</div>*/
/* 					</li>*/
/* 					*/
/* 					{% endif %}*/
/* */
/* 				{{ menu }}*/
/* */
/* 				<li>*/
/* 					<div class="clearfix browse-options">*/
/* 						{{ lang }}*/
/* 						{{ currency }}*/
/* 					</div>*/
/* 				</li>*/
/* */
/* 				<li>*/
/* */
/* 					<div class="social">*/
/* */
/* 					{% if clearshop_social %}*/
/* 						{% for key,value in clearshop_social %}*/
/* 						{% if value.url %}*/
/* 							<a href="{{ value.url }}" class="fa fa-{{ value.class }}" target="{{ value.target}}"></a>*/
/* 							{% endif %}*/
/* 						{% endfor %}*/
/* 					{% endif %}*/
/* */
/* 					{% if clearshop_custom_icon %}*/
/* 						{% for key,value in clearshop_custom_icon %}*/
/* 							{% if value.class != '' %}*/
/* 								{% if value.class != '' %}*/
/* 									<a href="{{ value.url }}" class="fa fa-{{ value.class }}" target="{{ value.target}}"></a>*/
/* 								{% else %}*/
/* 									<a class="fa fa-{{ value.class }}"></a>*/
/* 								{% endif %}*/
/* 							{% endif %}*/
/* 						{% endfor %}*/
/* 					{% endif %}*/
/* */
/* 					</div>*/
/* */
/* 				</li>*/
/* */
/* 			</ul>*/
/* */
/* 		</div>*/
/* */
/* 	</div>*/
/* */
/* </nav><!-- #navbar -->*/
