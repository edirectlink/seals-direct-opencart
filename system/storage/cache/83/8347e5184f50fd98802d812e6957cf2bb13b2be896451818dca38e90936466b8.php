<?php

/* clearshop/template/common/menudesk.twig */
class __TwigTemplate_eed47597283b013dbdc56fe184d25da29bdc4840b6c15f1cc94524e33be5b3e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"menu\">

\t<div class=\"container\">

\t\t<nav class=\"navbar navbar-default hidden-sm hidden-xs\" role=\"navigation\">

\t\t\t<div class=\"navcontainer\">

\t\t\t\t<div class=\"navbar-header\">
\t\t\t\t\t<a class=\"navbar-brand\" href=\"";
        // line 10
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\">
\t\t\t\t\t ";
        // line 11
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 12
            echo "\t\t\t\t\t\t<img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" />
\t\t\t\t\t";
        } else {
            // line 14
            echo "\t\t\t\t\t\t<img src=\"catalog/view/theme/clearshop/image/logo.png\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" />
\t\t\t\t\t";
        }
        // line 16
        echo "\t\t\t\t\t</a>
\t\t\t\t</div>

\t\t\t\t<div class=\"navbar-collapse\">

\t\t\t\t\t<ul class=\"nav navbar-nav\">

\t\t\t\t\t\t<!-- Home link -->

\t\t\t\t\t\t<li class=\"home home-";
        // line 25
        echo (isset($context["clearshop_homepage_link_style"]) ? $context["clearshop_homepage_link_style"] : null);
        echo "\"><a href=\"";
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\" title=\"";
        echo (isset($context["text_home"]) ? $context["text_home"] : null);
        echo "\"><i class=\"fa fa-home\"></i> <span>";
        echo (isset($context["text_home"]) ? $context["text_home"] : null);
        echo "</span></a></li>

\t\t\t\t\t\t";
        // line 27
        echo (isset($context["menu"]) ? $context["menu"] : null);
        echo "\t\t\t\t\t\t

\t\t\t\t\t</ul>

\t\t\t\t\t";
        // line 31
        if (((isset($context["clearshop_sticky_menu"]) ? $context["clearshop_sticky_menu"] : null) == 1)) {
            // line 32
            echo "
\t\t\t\t\t\t<ul class=\"nav navbar-nav navbar-right\">
\t\t\t\t\t\t\t";
            // line 34
            echo (isset($context["cart"]) ? $context["cart"] : null);
            echo "
\t\t\t\t\t\t</ul>
\t\t\t\t\t
\t\t\t\t\t";
        }
        // line 38
        echo "
\t\t\t\t</div>

\t\t\t</div>

\t\t</nav><!-- #navbar -->

\t</div>

</div>";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/menudesk.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 38,  89 => 34,  85 => 32,  83 => 31,  76 => 27,  65 => 25,  54 => 16,  46 => 14,  36 => 12,  34 => 11,  30 => 10,  19 => 1,);
    }
}
/* <div id="menu">*/
/* */
/* 	<div class="container">*/
/* */
/* 		<nav class="navbar navbar-default hidden-sm hidden-xs" role="navigation">*/
/* */
/* 			<div class="navcontainer">*/
/* */
/* 				<div class="navbar-header">*/
/* 					<a class="navbar-brand" href="{{ home }}">*/
/* 					 {% if logo %}*/
/* 						<img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" />*/
/* 					{% else %}*/
/* 						<img src="catalog/view/theme/clearshop/image/logo.png" title="{{ name }}" alt="{{ name }}" />*/
/* 					{% endif %}*/
/* 					</a>*/
/* 				</div>*/
/* */
/* 				<div class="navbar-collapse">*/
/* */
/* 					<ul class="nav navbar-nav">*/
/* */
/* 						<!-- Home link -->*/
/* */
/* 						<li class="home home-{{ clearshop_homepage_link_style }}"><a href="{{ home }}" title="{{ text_home }}"><i class="fa fa-home"></i> <span>{{ text_home }}</span></a></li>*/
/* */
/* 						{{ menu }}						*/
/* */
/* 					</ul>*/
/* */
/* 					{% if clearshop_sticky_menu == 1 %}*/
/* */
/* 						<ul class="nav navbar-nav navbar-right">*/
/* 							{{ cart }}*/
/* 						</ul>*/
/* 					*/
/* 					{% endif %}*/
/* */
/* 				</div>*/
/* */
/* 			</div>*/
/* */
/* 		</nav><!-- #navbar -->*/
/* */
/* 	</div>*/
/* */
/* </div>*/
