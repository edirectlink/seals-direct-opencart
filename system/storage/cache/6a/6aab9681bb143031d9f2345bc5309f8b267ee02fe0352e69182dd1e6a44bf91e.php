<?php

/* clearshop/template/common/home.twig */
class __TwigTemplate_00cb566a044eab4a5db451b0531cb48f3b7d1532a189a271ba496dbee3ea9713 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 4
            echo "\t";
            $context["class"] = "col-sm-6 middle sideleft";
        } elseif ((        // line 5
(isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 6
            echo "\t";
            $context["class"] = "col-sm-9";
        } else {
            // line 8
            echo "\t";
            $context["class"] = "col-sm-12";
        }
        // line 10
        echo "
\t<div class=\"container\"><div id=\"notification\" class=\"home\"></div></div>
\t
\t<div class=\"maintop-container\" class=\"home\">

\t\t<div id=\"common-home\" class=\"container\">

\t\t\t";
        // line 17
        echo (isset($context["home_top"]) ? $context["home_top"] : null);
        echo "

\t\t\t<div class=\"row\">

\t\t\t\t";
        // line 21
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
\t\t\t\t
\t\t\t\t<div id=\"content\" class=\"";
        // line 23
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">

\t\t\t\t\t<div class=\"mainborder\">

\t\t\t\t\t\t";
        // line 27
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
\t\t\t\t\t\t\t
\t\t\t\t\t</div>
\t\t\t\t\t
\t\t\t\t</div>
\t\t\t\t
\t\t\t\t";
        // line 33
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "

\t\t\t</div>

\t\t</div> <!-- .container -->

\t</div> <!-- #maintop-container -->

\t";
        // line 41
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "

";
        // line 43
        echo (isset($context["footer"]) ? $context["footer"] : null);
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 43,  87 => 41,  76 => 33,  67 => 27,  60 => 23,  55 => 21,  48 => 17,  39 => 10,  35 => 8,  31 => 6,  29 => 5,  26 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% if column_left and column_right %}*/
/* 	{% set class = 'col-sm-6 middle sideleft' %}*/
/* {% elseif column_left or column_right %}*/
/* 	{% set class = 'col-sm-9' %}*/
/* {% else %}*/
/* 	{% set class = 'col-sm-12' %}*/
/* {% endif %}*/
/* */
/* 	<div class="container"><div id="notification" class="home"></div></div>*/
/* 	*/
/* 	<div class="maintop-container" class="home">*/
/* */
/* 		<div id="common-home" class="container">*/
/* */
/* 			{{ home_top }}*/
/* */
/* 			<div class="row">*/
/* */
/* 				{{ column_left }}*/
/* 				*/
/* 				<div id="content" class="{{ class }}">*/
/* */
/* 					<div class="mainborder">*/
/* */
/* 						{{ content_top }}*/
/* 							*/
/* 					</div>*/
/* 					*/
/* 				</div>*/
/* 				*/
/* 				{{ column_right }}*/
/* */
/* 			</div>*/
/* */
/* 		</div> <!-- .container -->*/
/* */
/* 	</div> <!-- #maintop-container -->*/
/* */
/* 	{{ content_bottom }}*/
/* */
/* {{ footer }}*/
