<?php

/* clearshop/template/common/cart.twig */
class __TwigTemplate_84efe119d00f56e6cffb5385374673920699c660a65e1f9ee011ce7ba872a4be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"cart\" class=\"btn-group btn-block\">
  <button type=\"button\" data-toggle=\"dropdown\" data-loading-text=\"";
        // line 2
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" class=\"btn btn-default dropdown-toggle cart-heading\"><i class=\"fa fa-shopping-bag\"></i> <span id=\"cart-total\">";
        echo (isset($context["text_items"]) ? $context["text_items"] : null);
        echo "</span></button>
  <ul class=\"dropdown-menu dropdown-menu-right\">
    ";
        // line 4
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 5
            echo "      
      <li>
        <table class=\"table table-items\">
          ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 9
                echo "            <tr>
              <td class=\"text-center\" width=\"20%\">
                ";
                // line 11
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    // line 12
                    echo "                  <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive\" /></a>
                ";
                }
                // line 14
                echo "              </td>
              <td class=\"text-left\" width=\"70%\"><span class=\"name\"><a href=\"";
                // line 15
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></span>
                ";
                // line 16
                if ($this->getAttribute($context["product"], "option", array())) {
                    // line 17
                    echo "                  ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        echo " <br />
                    - <small>";
                        // line 18
                        echo $this->getAttribute($context["option"], "name", array());
                        echo " ";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "</small>
                  ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 20
                    echo "                ";
                }
                // line 21
                echo "                ";
                if ($this->getAttribute($context["product"], "recurring", array())) {
                    echo " <br />
                  - <small>";
                    // line 22
                    echo (isset($context["text_recurring"]) ? $context["text_recurring"] : null);
                    echo " ";
                    echo $this->getAttribute($context["product"], "recurring", array());
                    echo "</small>
                ";
                }
                // line 24
                echo "                <br><span class=\"badge\">";
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "</span> <span class=\"cart-price\">";
                echo $this->getAttribute($context["product"], "total", array());
                echo "</span>
              </td>
              <td class=\"text-center\" width=\"10%\"><button type=\"button\" onclick=\"cart.remove('";
                // line 26
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "'); \$(this).tooltip('destroy')\" data-toggle=\"tooltip\" data-placement=\"left\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-remove\">&times;</button></td>
            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "          ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
                // line 30
                echo "            <tr>
              <td class=\"text-center\"></td>
              <td class=\"text-left\">";
                // line 32
                echo $this->getAttribute($context["voucher"], "description", array());
                echo " <br> <span class=\"badge\">1</span> ";
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</td>
              <td class=\"text-center text-danger\"><button type=\"button\" onclick=\"voucher.remove('";
                // line 33
                echo $this->getAttribute($context["voucher"], "key", array());
                echo "'); \$(this).tooltip('destroy');\" data-toggle=\"tooltip\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-remove\">&times;</button></td>
            </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "        </table>
        <div class=\"cart-summary\">
          <table class=\"table totals\">
            ";
            // line 39
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 40
                echo "            <tr>
              <td class=\"text-right\"><strong>";
                // line 41
                echo $this->getAttribute($context["total"], "title", array());
                echo "</strong></td>
              <td class=\"text-right\">";
                // line 42
                echo $this->getAttribute($context["total"], "text", array());
                echo "</td>
            </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "          </table>
          <div class=\"text-right\">
            <a href=\"";
            // line 47
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo "\" class=\"btn btn-cart btn-block\"> ";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "</a>
            <a href=\"";
            // line 48
            echo (isset($context["cart"]) ? $context["cart"] : null);
            echo "\" class=\"btn btn-default btn-block\">";
            echo (isset($context["text_cart"]) ? $context["text_cart"] : null);
            echo "</a>
          </div>
        </div>
      </li>
    
    ";
        } else {
            // line 54
            echo "
      <li>
        <p class=\"empty white text-center\">";
            // line 56
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
      </li>
    
    ";
        }
        // line 60
        echo "  </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 60,  196 => 56,  192 => 54,  181 => 48,  175 => 47,  171 => 45,  162 => 42,  158 => 41,  155 => 40,  151 => 39,  146 => 36,  135 => 33,  129 => 32,  125 => 30,  120 => 29,  109 => 26,  101 => 24,  94 => 22,  89 => 21,  86 => 20,  76 => 18,  69 => 17,  67 => 16,  61 => 15,  58 => 14,  46 => 12,  44 => 11,  40 => 9,  36 => 8,  31 => 5,  29 => 4,  22 => 2,  19 => 1,);
    }
}
/* <div id="cart" class="btn-group btn-block">*/
/*   <button type="button" data-toggle="dropdown" data-loading-text="{{ text_loading }}" class="btn btn-default dropdown-toggle cart-heading"><i class="fa fa-shopping-bag"></i> <span id="cart-total">{{ text_items }}</span></button>*/
/*   <ul class="dropdown-menu dropdown-menu-right">*/
/*     {% if products or vouchers %}*/
/*       */
/*       <li>*/
/*         <table class="table table-items">*/
/*           {% for product in products %}*/
/*             <tr>*/
/*               <td class="text-center" width="20%">*/
/*                 {% if product.thumb %}*/
/*                   <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive" /></a>*/
/*                 {% endif %}*/
/*               </td>*/
/*               <td class="text-left" width="70%"><span class="name"><a href="{{ product.href }}">{{ product.name }}</a></span>*/
/*                 {% if product.option %}*/
/*                   {% for option in product.option %} <br />*/
/*                     - <small>{{ option.name }} {{ option.value }}</small>*/
/*                   {% endfor %}*/
/*                 {% endif %}*/
/*                 {% if product.recurring %} <br />*/
/*                   - <small>{{ text_recurring }} {{ product.recurring }}</small>*/
/*                 {% endif %}*/
/*                 <br><span class="badge">{{ product.quantity }}</span> <span class="cart-price">{{ product.total }}</span>*/
/*               </td>*/
/*               <td class="text-center" width="10%"><button type="button" onclick="cart.remove('{{ product.cart_id }}'); $(this).tooltip('destroy')" data-toggle="tooltip" data-placement="left" title="{{ button_remove }}" class="btn btn-remove">&times;</button></td>*/
/*             </tr>*/
/*           {% endfor %}*/
/*           {% for voucher in vouchers %}*/
/*             <tr>*/
/*               <td class="text-center"></td>*/
/*               <td class="text-left">{{ voucher.description }} <br> <span class="badge">1</span> {{ voucher.amount }}</td>*/
/*               <td class="text-center text-danger"><button type="button" onclick="voucher.remove('{{ voucher.key }}'); $(this).tooltip('destroy');" data-toggle="tooltip" title="{{ button_remove }}" class="btn btn-remove">&times;</button></td>*/
/*             </tr>*/
/*           {% endfor %}*/
/*         </table>*/
/*         <div class="cart-summary">*/
/*           <table class="table totals">*/
/*             {% for total in totals %}*/
/*             <tr>*/
/*               <td class="text-right"><strong>{{ total.title }}</strong></td>*/
/*               <td class="text-right">{{ total.text }}</td>*/
/*             </tr>*/
/*             {% endfor %}*/
/*           </table>*/
/*           <div class="text-right">*/
/*             <a href="{{ checkout }}" class="btn btn-cart btn-block"> {{ text_checkout }}</a>*/
/*             <a href="{{ cart }}" class="btn btn-default btn-block">{{ text_cart }}</a>*/
/*           </div>*/
/*         </div>*/
/*       </li>*/
/*     */
/*     {% else %}*/
/* */
/*       <li>*/
/*         <p class="empty white text-center">{{ text_empty }}</p>*/
/*       </li>*/
/*     */
/*     {% endif %}*/
/*   </ul>*/
/* </div>*/
/* */
