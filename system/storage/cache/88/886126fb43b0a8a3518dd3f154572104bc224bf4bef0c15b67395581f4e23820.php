<?php

/* clearshop/template/extension/module/tabbed_products.twig */
class __TwigTemplate_2e0b01490ac5b72117e1da693acc59d4336e7d5b85125b41f8ccd10dd27e9f2c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"tabbed-products\">
\t
\t";
        // line 3
        $context["tabs"] = 0;
        // line 4
        echo "\t
\t<ul id=\"tabs-";
        // line 5
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "\" class=\"nav nav-pills\">

\t\t";
        // line 7
        if (((isset($context["featured_products"]) ? $context["featured_products"] : null) && ((isset($context["featured_mode"]) ? $context["featured_mode"] : null) != "disabled"))) {
            // line 8
            echo "\t\t ";
            $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
            // line 9
            echo "\t\t\t<li ";
            echo ((((isset($context["tabs"]) ? $context["tabs"] : null) == 1)) ? ("class=\"active\"") : (""));
            echo " ><a id=\"#tab-featured-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "-link\" href=\"#tab-featured-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_featured"]) ? $context["tab_featured"] : null);
            echo "</a></li>
\t\t";
        }
        // line 11
        echo "
\t\t";
        // line 12
        if (((isset($context["latest_products"]) ? $context["latest_products"] : null) && ((isset($context["latest_mode"]) ? $context["latest_mode"] : null) != "disabled"))) {
            // line 13
            echo "\t\t ";
            $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
            // line 14
            echo "\t\t\t<li ";
            echo ((((isset($context["tabs"]) ? $context["tabs"] : null) == 1)) ? ("class=\"active\"") : (""));
            echo " ><a id=\"#tab-latest-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "-link\" href=\"#tab-latest-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_latest"]) ? $context["tab_latest"] : null);
            echo "</a></li>
\t\t";
        }
        // line 16
        echo "
\t\t";
        // line 17
        if (((isset($context["bestseller_products"]) ? $context["bestseller_products"] : null) && ((isset($context["bestseller_mode"]) ? $context["bestseller_mode"] : null) != "disabled"))) {
            // line 18
            echo "\t\t ";
            $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
            // line 19
            echo "\t\t\t<li ";
            echo ((((isset($context["tabs"]) ? $context["tabs"] : null) == 1)) ? ("class=\"active\"") : (""));
            echo " ><a id=\"#tab-bestseller-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "-link\" href=\"#tab-bestseller-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_bestseller"]) ? $context["tab_bestseller"] : null);
            echo "</a></li>
\t\t";
        }
        // line 21
        echo "
\t\t";
        // line 22
        if (((isset($context["special_products"]) ? $context["special_products"] : null) && ((isset($context["special_mode"]) ? $context["special_mode"] : null) != "disabled"))) {
            // line 23
            echo "\t\t ";
            $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
            // line 24
            echo "\t\t\t<li ";
            echo ((((isset($context["tabs"]) ? $context["tabs"] : null) == 1)) ? ("class=\"active\"") : (""));
            echo " ><a id=\"#tab-special-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "-link\" href=\"#tab-special-";
            echo (isset($context["module"]) ? $context["module"] : null);
            echo "\" data-toggle=\"tab\">";
            echo (isset($context["tab_special"]) ? $context["tab_special"] : null);
            echo "</a></li>
\t\t";
        }
        // line 26
        echo "
\t</ul>

\t<div class=\"tab-content\">

\t\t";
        // line 31
        $context["tabs"] = 0;
        // line 32
        echo "
\t\t\t";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["t"]) {
            // line 34
            echo "
\t\t\t\t";
            // line 35
            $context["disabled"] = "true";
            // line 36
            echo "\t\t\t\t";
            $context["carousel"] = 1;
            // line 37
            echo "
\t\t\t\t\t";
            // line 38
            if (($context["t"] == 1)) {
                // line 39
                echo "
\t\t\t\t\t";
                // line 40
                if (((isset($context["featured_products"]) ? $context["featured_products"] : null) && ((isset($context["featured_mode"]) ? $context["featured_mode"] : null) != "disabled"))) {
                    // line 41
                    echo "\t\t\t\t\t\t";
                    $context["products"] = (isset($context["featured_products"]) ? $context["featured_products"] : null);
                    // line 42
                    echo "\t\t\t\t\t\t";
                    $context["tabID"] = "tab-featured-";
                    // line 43
                    echo "\t\t\t\t\t\t";
                    $context["disabled"] = "false";
                    // line 44
                    echo "\t\t\t\t\t\t";
                    $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
                    // line 45
                    echo "\t\t\t\t\t\t";
                    if (((isset($context["featured_mode"]) ? $context["featured_mode"] : null) == "carousel")) {
                        // line 46
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 1;
                        // line 47
                        echo "\t\t\t\t\t\t";
                    } else {
                        // line 48
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 0;
                        // line 49
                        echo "\t\t\t\t\t\t";
                    }
                    // line 50
                    echo "\t\t\t\t\t";
                }
                // line 51
                echo "
\t\t\t\t";
            } elseif ((            // line 52
$context["t"] == 2)) {
                // line 53
                echo "
\t\t\t\t\t";
                // line 54
                if (((isset($context["latest_products"]) ? $context["latest_products"] : null) && ((isset($context["latest_mode"]) ? $context["latest_mode"] : null) != "disabled"))) {
                    // line 55
                    echo "\t\t\t\t\t\t";
                    $context["products"] = (isset($context["latest_products"]) ? $context["latest_products"] : null);
                    // line 56
                    echo "\t\t\t\t\t\t";
                    $context["tabID"] = "tab-latest-";
                    // line 57
                    echo "\t\t\t\t\t\t";
                    $context["disabled"] = "false";
                    // line 58
                    echo "\t\t\t\t\t\t";
                    $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
                    // line 59
                    echo "\t\t\t\t\t\t";
                    if (((isset($context["latest_mode"]) ? $context["latest_mode"] : null) == "carousel")) {
                        // line 60
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 1;
                        // line 61
                        echo "\t\t\t\t\t\t";
                    } else {
                        // line 62
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 0;
                        // line 63
                        echo "\t\t\t\t\t\t";
                    }
                    // line 64
                    echo "\t\t\t\t\t";
                }
                // line 65
                echo "
\t\t\t\t";
            } elseif ((            // line 66
$context["t"] == 3)) {
                // line 67
                echo "
\t\t\t\t\t";
                // line 68
                if (((isset($context["bestseller_products"]) ? $context["bestseller_products"] : null) && ((isset($context["bestseller_mode"]) ? $context["bestseller_mode"] : null) != "disabled"))) {
                    // line 69
                    echo "\t\t\t\t\t\t";
                    $context["products"] = (isset($context["bestseller_products"]) ? $context["bestseller_products"] : null);
                    // line 70
                    echo "\t\t\t\t\t\t";
                    $context["tabID"] = "tab-bestseller-";
                    // line 71
                    echo "\t\t\t\t\t\t";
                    $context["disabled"] = "false";
                    // line 72
                    echo "\t\t\t\t\t\t";
                    $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
                    // line 73
                    echo "\t\t\t\t\t\t";
                    if (((isset($context["bestseller_mode"]) ? $context["bestseller_mode"] : null) == "carousel")) {
                        // line 74
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 1;
                        // line 75
                        echo "\t\t\t\t\t\t";
                    } else {
                        // line 76
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 0;
                        // line 77
                        echo "\t\t\t\t\t\t";
                    }
                    // line 78
                    echo "\t\t\t\t\t";
                }
                // line 79
                echo "
\t\t\t\t";
            } elseif ((            // line 80
$context["t"] == 4)) {
                // line 81
                echo "
\t\t\t\t\t";
                // line 82
                if (((isset($context["special_products"]) ? $context["special_products"] : null) && ((isset($context["special_mode"]) ? $context["special_mode"] : null) != "disabled"))) {
                    // line 83
                    echo "\t\t\t\t\t\t";
                    $context["products"] = (isset($context["special_products"]) ? $context["special_products"] : null);
                    // line 84
                    echo "\t\t\t\t\t\t";
                    $context["tabID"] = "tab-special-";
                    // line 85
                    echo "\t\t\t\t\t\t";
                    $context["disabled"] = "false";
                    // line 86
                    echo "\t\t\t\t\t\t";
                    $context["tabs"] = ((isset($context["tabs"]) ? $context["tabs"] : null) + 1);
                    // line 87
                    echo "\t\t\t\t\t\t";
                    if (((isset($context["special_mode"]) ? $context["special_mode"] : null) == "carousel")) {
                        // line 88
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 1;
                        // line 89
                        echo "\t\t\t\t\t\t";
                    } else {
                        // line 90
                        echo "\t\t\t\t\t\t\t";
                        $context["carousel"] = 0;
                        // line 91
                        echo "\t\t\t\t\t\t";
                    }
                    // line 92
                    echo "\t\t\t\t\t";
                }
                // line 93
                echo "
\t\t\t\t";
            }
            // line 95
            echo "
\t\t\t\t";
            // line 96
            if (((isset($context["carousel"]) ? $context["carousel"] : null) == 1)) {
                echo " 
\t\t\t\t\t";
                // line 97
                $context["class1"] = "product-slider top-arrows";
                // line 98
                echo "\t\t\t\t\t";
                $context["class2"] = "owl-carousel";
                // line 99
                echo "\t\t\t\t\t";
                $context["class3"] = "item";
                // line 100
                echo "\t\t\t\t";
            } else {
                echo " 
\t\t\t\t\t";
                // line 101
                $context["class1"] = "box";
                // line 102
                echo "\t\t\t\t\t";
                $context["class2"] = "row";
                // line 103
                echo "\t\t\t\t\t";
                $context["class3"] = "product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12";
                // line 104
                echo "\t\t\t\t";
            }
            // line 105
            echo "
\t\t\t\t";
            // line 106
            if (((isset($context["disabled"]) ? $context["disabled"] : null) == "false")) {
                // line 107
                echo "
\t\t\t\t\t<div id=\"";
                // line 108
                echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                echo "\" class=\"tab-pane active ";
                echo ((((isset($context["tabs"]) ? $context["tabs"] : null) > 1)) ? ("secondary") : (""));
                echo "\">

\t\t\t\t\t\t<div id=\"";
                // line 110
                echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                echo "_carousel\" class=\"";
                echo (isset($context["class1"]) ? $context["class1"] : null);
                echo "\" ";
                echo ((((isset($context["direction"]) ? $context["direction"] : null) == "rtl")) ? ("style=\"direction:rtl\"") : (""));
                echo " >

\t\t\t\t\t\t\t<div class=\"";
                // line 112
                echo (isset($context["class2"]) ? $context["class2"] : null);
                echo "\">

\t\t\t\t\t\t\t\t";
                // line 114
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                    // line 115
                    echo "\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t<div class=\"";
                    // line 116
                    echo (isset($context["class3"]) ? $context["class3"] : null);
                    echo "\">
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div class=\"product-thumb transition\">

\t\t\t\t\t\t\t\t\t\t\t<div class=\"inner\">

\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"image hover_fade_in_back\">

\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 124
                    if ((($this->getAttribute($context["product"], "thumb", array()) && $this->getAttribute($context["product"], "thumb_swap", array())) && ((isset($context["clearshop_rollover_images"]) ? $context["clearshop_rollover_images"] : null) == 1))) {
                        // line 125
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"front-image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 126
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\"><img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\"/></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"back-image\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        // line 129
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\"><img src=\"";
                        echo $this->getAttribute($context["product"], "thumb_swap", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\"/></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    } elseif ($this->getAttribute(                    // line 131
$context["product"], "thumb", array())) {
                        // line 132
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a href=\"";
                        echo $this->getAttribute($context["product"], "href", array());
                        echo "\"><img src=\"";
                        echo $this->getAttribute($context["product"], "thumb", array());
                        echo "\" title=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" class=\"img-responsive\" alt=\"";
                        echo $this->getAttribute($context["product"], "name", array());
                        echo "\" /></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 134
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 135
                    if ($this->getAttribute($context["product"], "special", array())) {
                        // line 136
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"onsale\"><span>";
                        echo (isset($context["text_onsale"]) ? $context["text_onsale"] : null);
                        echo "</span></div>
\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 138
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"caption\">

\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"name\"><a href=\"";
                    // line 143
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "</a></h4>

\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 145
                    if ($this->getAttribute($context["product"], "price", array())) {
                        // line 146
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"price\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        // line 147
                        if ( !$this->getAttribute($context["product"], "special", array())) {
                            // line 148
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                            echo $this->getAttribute($context["product"], "price", array());
                            echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        } else {
                            // line 150
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-new\">";
                            echo $this->getAttribute($context["product"], "special", array());
                            echo "</span> <span class=\"price-old\">";
                            echo $this->getAttribute($context["product"], "price", array());
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 152
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        if ($this->getAttribute($context["product"], "tax", array())) {
                            // line 153
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"price-tax\">";
                            echo (isset($context["text_tax"]) ? $context["text_tax"] : null);
                            echo " ";
                            echo $this->getAttribute($context["product"], "tax", array());
                            echo "</span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                        }
                        // line 155
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 157
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    // line 158
                    if ($this->getAttribute($context["product"], "rating", array())) {
                        // line 159
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"rating\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                        // line 160
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(5);
                        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                            // line 161
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                            if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                                // line 162
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                            } else {
                                // line 164
                                echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  <span class=\"fa fa-stack\"><i class=\"fa fa-star fa-stack-2x\"></i><i class=\"fa fa-star-o fa-stack-2x\"></i></span>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                            }
                            // line 166
                            echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t  ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 167
                        echo "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
                    }
                    // line 169
                    echo "
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"cart\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" onclick=\"cart.add('";
                    // line 171
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\" class=\"btn btn-sm btn-primary\">";
                    echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                    echo "</button>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"links\">
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-link btn-sm wishlist\" title=\"";
                    // line 174
                    echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                    echo "\" onclick=\"wishlist.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-heart-o\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<button type=\"button\" data-toggle=\"tooltip\" class=\"btn btn-link btn-sm compare\" title=\"";
                    // line 175
                    echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                    echo "\" onclick=\"compare.add('";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "');\"><i class=\"fa fa-exchange\"></i></button>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<a id=\"qv";
                    // line 176
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\" title=\"";
                    echo (isset($context["text_quickview"]) ? $context["text_quickview"] : null);
                    echo "\" data-toggle=\"tooltip\" class=\"btn btn-link quickview\" rel=\"mp-quickview\" href=\"index.php?route=product/quickview&product_id=";
                    echo $this->getAttribute($context["product"], "product_id", array());
                    echo "\" ><i class=\"fa fa-window-restore\"></i></a>
\t\t\t\t\t\t\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t\t\t\t\t\t</div> ";
                    // line 180
                    echo "
\t\t\t\t\t\t\t\t\t\t\t</div> <!-- .inner -->\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t</div> ";
                    // line 184
                    echo "
\t\t\t\t\t\t\t\t\t</div> ";
                    // line 186
                    echo "
\t\t\t\t\t\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 188
                echo "
\t\t\t\t\t\t\t</div> ";
                // line 190
                echo "
\t\t\t\t\t\t\t</div>

\t\t\t\t\t\t\t<script type=\"text/javascript\">

\t\t\t\t\t\t\t\t\$(window).load(function() {

\t\t\t\t\t\t\t\t\t";
                // line 197
                if (((isset($context["carousel"]) ? $context["carousel"] : null) == 1)) {
                    // line 198
                    echo "
\t\t\t\t\t\t\t\t\t\$('#";
                    // line 199
                    echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                    echo "_carousel .owl-carousel').owlCarousel({
\t\t\t\t\t\t\t\t\t\titems: 4,
\t\t\t\t\t\t\t\t\t\titemsMobile:[479,2],
\t\t\t\t\t\t\t\t\t\t";
                    // line 202
                    if (((isset($context["clearshop_carousel_autoplay"]) ? $context["clearshop_carousel_autoplay"] : null) == 1)) {
                        echo " autoPlay: 3000, ";
                    }
                    // line 203
                    echo "\t\t\t\t\t\t\t\t\t\tnavigation: true,
\t\t\t\t\t\t\t\t\t\tnavigationText: ['<i class=\"fa fa-angle-left fa-5x\"></i>', '<i class=\"fa fa-angle-right fa-5x\"></i>'],
\t\t\t\t\t\t\t\t\t\tpagination: true,
\t\t\t\t\t\t\t\t\t\tloop:true,
\t\t\t\t\t\t\t\t\t\tafterInit: function(){
\t\t\t\t\t\t\t\t\t\t\tif (\$('#";
                    // line 208
                    echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                    echo "').hasClass('secondary')) {
\t\t\t\t\t\t\t\t\t\t\t\t\$('#";
                    // line 209
                    echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                    echo "').removeClass('active');
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 215
                    echo "\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\tif (\$('#";
                    // line 216
                    echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                    echo "').hasClass('secondary')) {
\t\t\t\t\t\t\t\t\t\t\t\$('#";
                    // line 217
                    echo ((isset($context["tabID"]) ? $context["tabID"] : null) . (isset($context["module"]) ? $context["module"] : null));
                    echo "').removeClass('active');
\t\t\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\t\t";
                }
                // line 221
                echo "
\t\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t</script>

\t\t\t\t\t</div>

\t\t\t\t";
            }
            // line 228
            echo " ";
            // line 229
            echo "
\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['t'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 231
        echo "
\t</div>

</div>";
    }

    public function getTemplateName()
    {
        return "clearshop/template/extension/module/tabbed_products.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  641 => 231,  634 => 229,  632 => 228,  622 => 221,  615 => 217,  611 => 216,  608 => 215,  599 => 209,  595 => 208,  588 => 203,  584 => 202,  578 => 199,  575 => 198,  573 => 197,  564 => 190,  561 => 188,  554 => 186,  551 => 184,  546 => 180,  536 => 176,  530 => 175,  524 => 174,  516 => 171,  512 => 169,  508 => 167,  502 => 166,  498 => 164,  494 => 162,  491 => 161,  487 => 160,  484 => 159,  482 => 158,  479 => 157,  475 => 155,  467 => 153,  464 => 152,  456 => 150,  450 => 148,  448 => 147,  445 => 146,  443 => 145,  436 => 143,  429 => 138,  423 => 136,  421 => 135,  418 => 134,  406 => 132,  404 => 131,  393 => 129,  381 => 126,  378 => 125,  376 => 124,  365 => 116,  362 => 115,  358 => 114,  353 => 112,  344 => 110,  337 => 108,  334 => 107,  332 => 106,  329 => 105,  326 => 104,  323 => 103,  320 => 102,  318 => 101,  313 => 100,  310 => 99,  307 => 98,  305 => 97,  301 => 96,  298 => 95,  294 => 93,  291 => 92,  288 => 91,  285 => 90,  282 => 89,  279 => 88,  276 => 87,  273 => 86,  270 => 85,  267 => 84,  264 => 83,  262 => 82,  259 => 81,  257 => 80,  254 => 79,  251 => 78,  248 => 77,  245 => 76,  242 => 75,  239 => 74,  236 => 73,  233 => 72,  230 => 71,  227 => 70,  224 => 69,  222 => 68,  219 => 67,  217 => 66,  214 => 65,  211 => 64,  208 => 63,  205 => 62,  202 => 61,  199 => 60,  196 => 59,  193 => 58,  190 => 57,  187 => 56,  184 => 55,  182 => 54,  179 => 53,  177 => 52,  174 => 51,  171 => 50,  168 => 49,  165 => 48,  162 => 47,  159 => 46,  156 => 45,  153 => 44,  150 => 43,  147 => 42,  144 => 41,  142 => 40,  139 => 39,  137 => 38,  134 => 37,  131 => 36,  129 => 35,  126 => 34,  122 => 33,  119 => 32,  117 => 31,  110 => 26,  98 => 24,  95 => 23,  93 => 22,  90 => 21,  78 => 19,  75 => 18,  73 => 17,  70 => 16,  58 => 14,  55 => 13,  53 => 12,  50 => 11,  38 => 9,  35 => 8,  33 => 7,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }
}
/* <div class="tabbed-products">*/
/* 	*/
/* 	{% set tabs = 0 %}*/
/* 	*/
/* 	<ul id="tabs-{{ module }}" class="nav nav-pills">*/
/* */
/* 		{% if featured_products and featured_mode != 'disabled' %}*/
/* 		 {% set tabs =  tabs + 1 %}*/
/* 			<li {{ tabs == 1 ? 'class="active"' : '' }} ><a id="#tab-featured-{{ module }}-link" href="#tab-featured-{{ module }}" data-toggle="tab">{{ tab_featured }}</a></li>*/
/* 		{% endif %}*/
/* */
/* 		{% if latest_products and latest_mode != 'disabled' %}*/
/* 		 {% set tabs =  tabs + 1 %}*/
/* 			<li {{ tabs == 1 ? 'class="active"' : '' }} ><a id="#tab-latest-{{ module }}-link" href="#tab-latest-{{ module }}" data-toggle="tab">{{ tab_latest }}</a></li>*/
/* 		{% endif %}*/
/* */
/* 		{% if bestseller_products and bestseller_mode != 'disabled' %}*/
/* 		 {% set tabs =  tabs + 1 %}*/
/* 			<li {{ tabs == 1 ? 'class="active"' : '' }} ><a id="#tab-bestseller-{{ module }}-link" href="#tab-bestseller-{{ module }}" data-toggle="tab">{{ tab_bestseller }}</a></li>*/
/* 		{% endif %}*/
/* */
/* 		{% if special_products and special_mode != 'disabled' %}*/
/* 		 {% set tabs =  tabs + 1 %}*/
/* 			<li {{ tabs == 1 ? 'class="active"' : '' }} ><a id="#tab-special-{{ module }}-link" href="#tab-special-{{ module }}" data-toggle="tab">{{ tab_special }}</a></li>*/
/* 		{% endif %}*/
/* */
/* 	</ul>*/
/* */
/* 	<div class="tab-content">*/
/* */
/* 		{% set tabs = 0 %}*/
/* */
/* 			{% for t in 1..4 %}*/
/* */
/* 				{% set disabled = 'true' %}*/
/* 				{% set carousel = 1 %}*/
/* */
/* 					{% if t == 1 %}*/
/* */
/* 					{% if featured_products and featured_mode != 'disabled' %}*/
/* 						{% set products = featured_products %}*/
/* 						{% set tabID = 'tab-featured-' %}*/
/* 						{% set disabled = 'false' %}*/
/* 						{% set tabs = tabs + 1 %}*/
/* 						{% if featured_mode == 'carousel' %}*/
/* 							{% set carousel = 1 %}*/
/* 						{% else %}*/
/* 							{% set carousel = 0 %}*/
/* 						{% endif %}*/
/* 					{% endif %}*/
/* */
/* 				{% elseif t == 2 %}*/
/* */
/* 					{% if latest_products and latest_mode != 'disabled' %}*/
/* 						{% set products = latest_products %}*/
/* 						{% set tabID = 'tab-latest-' %}*/
/* 						{% set disabled = 'false' %}*/
/* 						{% set tabs = tabs + 1 %}*/
/* 						{% if latest_mode == 'carousel' %}*/
/* 							{% set carousel = 1 %}*/
/* 						{% else %}*/
/* 							{% set carousel = 0 %}*/
/* 						{% endif %}*/
/* 					{% endif %}*/
/* */
/* 				{% elseif t == 3 %}*/
/* */
/* 					{% if bestseller_products and bestseller_mode != 'disabled' %}*/
/* 						{% set products = bestseller_products %}*/
/* 						{% set tabID = 'tab-bestseller-' %}*/
/* 						{% set disabled = 'false' %}*/
/* 						{% set tabs = tabs + 1 %}*/
/* 						{% if bestseller_mode == 'carousel' %}*/
/* 							{% set carousel = 1 %}*/
/* 						{% else %}*/
/* 							{% set carousel = 0 %}*/
/* 						{% endif %}*/
/* 					{% endif %}*/
/* */
/* 				{% elseif t == 4 %}*/
/* */
/* 					{% if special_products and special_mode != 'disabled' %}*/
/* 						{% set products = special_products %}*/
/* 						{% set tabID = 'tab-special-' %}*/
/* 						{% set disabled = 'false' %}*/
/* 						{% set tabs = tabs + 1 %}*/
/* 						{% if special_mode == 'carousel' %}*/
/* 							{% set carousel = 1 %}*/
/* 						{% else %}*/
/* 							{% set carousel = 0 %}*/
/* 						{% endif %}*/
/* 					{% endif %}*/
/* */
/* 				{% endif %}*/
/* */
/* 				{% if carousel == 1 %} */
/* 					{% set class1= 'product-slider top-arrows' %}*/
/* 					{% set class2= 'owl-carousel' %}*/
/* 					{% set class3= 'item' %}*/
/* 				{% else %} */
/* 					{% set class1= 'box' %}*/
/* 					{% set class2= 'row' %}*/
/* 					{% set class3= 'product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12' %}*/
/* 				{% endif %}*/
/* */
/* 				{% if disabled == 'false' %}*/
/* */
/* 					<div id="{{ tabID ~ module }}" class="tab-pane active {{ tabs > 1 ? 'secondary' }}">*/
/* */
/* 						<div id="{{ tabID ~ module }}_carousel" class="{{ class1 }}" {{ direction == 'rtl' ? 'style="direction:rtl"' }} >*/
/* */
/* 							<div class="{{ class2 }}">*/
/* */
/* 								{% for product in products %}*/
/* 										*/
/* 									<div class="{{ class3 }}">*/
/* 										*/
/* 										<div class="product-thumb transition">*/
/* */
/* 											<div class="inner">*/
/* */
/* 												<div class="image hover_fade_in_back">*/
/* */
/* 													{% if product.thumb and product.thumb_swap and clearshop_rollover_images == 1 %}*/
/* 														<div class="front-image">*/
/* 															<a href="{{ product.href }}"><img src="{{ product.thumb }}" title="{{ product.name }}" class="img-responsive" alt="{{ product.name }}"/></a>*/
/* 														</div>*/
/* 														<div class="back-image">*/
/* 															<a href="{{ product.href }}"><img src="{{ product.thumb_swap }}" title="{{ product.name }}" class="img-responsive" alt="{{ product.name }}"/></a>*/
/* 														</div>*/
/* 													{% elseif product.thumb %}*/
/* 														<a href="{{ product.href }}"><img src="{{ product.thumb }}" title="{{ product.name }}" class="img-responsive" alt="{{ product.name }}" /></a>*/
/* 													{% endif %}*/
/* */
/* 													{% if product.special %}*/
/* 													<div class="onsale"><span>{{ text_onsale }}</span></div>*/
/* 													{% endif %}*/
/* */
/* 												</div>*/
/* 						*/
/* 												<div class="caption">*/
/* */
/* 													<h4 class="name"><a href="{{ product.href }}">{{ product.name }}</a></h4>*/
/* */
/* 													{% if product.price %}*/
/* 														<div class="price">*/
/* 															{% if not product.special %}*/
/* 																{{ product.price }}*/
/* 															{% else %}*/
/* 																<span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/* 															{% endif %}*/
/* 															{% if product.tax %}*/
/* 																<span class="price-tax">{{ text_tax }} {{ product.tax }}</span>*/
/* 															{% endif %}*/
/* 														</div>*/
/* 														{% endif %}*/
/* */
/* 														{% if product.rating %}*/
/* 														<div class="rating">*/
/* 														  {% for i in 5 %}*/
/* 														  {% if product.rating < i %}*/
/* 														  <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 														  {% else %}*/
/* 														  <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>*/
/* 														  {% endif %}*/
/* 														  {% endfor %}*/
/* 														</div>*/
/* 														{% endif %}*/
/* */
/* 													<div class="cart">*/
/* 														<button type="button" onclick="cart.add('{{ product.product_id }}');" class="btn btn-sm btn-primary">{{ button_cart }}</button>*/
/* 													</div>*/
/* 													<div class="links">*/
/* 														<button type="button" data-toggle="tooltip" class="btn btn-link btn-sm wishlist" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');"><i class="fa fa-heart-o"></i></button>*/
/* 														<button type="button" data-toggle="tooltip" class="btn btn-link btn-sm compare" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');"><i class="fa fa-exchange"></i></button>*/
/* 														<a id="qv{{ product.product_id }}" title="{{ text_quickview }}" data-toggle="tooltip" class="btn btn-link quickview" rel="mp-quickview" href="index.php?route=product/quickview&product_id={{ product.product_id }}" ><i class="fa fa-window-restore"></i></a>*/
/* 													</div>*/
/* */
/* 												</div> {# .caption #}*/
/* */
/* 											</div> <!-- .inner -->							*/
/* 										*/
/* 										</div> {# .product-thumb #}*/
/* */
/* 									</div> {# .item, .product-layout #}*/
/* */
/* 								{% endfor %}*/
/* */
/* 							</div> {# .owl-carousel, .box #}*/
/* */
/* 							</div>*/
/* */
/* 							<script type="text/javascript">*/
/* */
/* 								$(window).load(function() {*/
/* */
/* 									{% if carousel == 1 %}*/
/* */
/* 									$('#{{ tabID ~ module }}_carousel .owl-carousel').owlCarousel({*/
/* 										items: 4,*/
/* 										itemsMobile:[479,2],*/
/* 										{% if clearshop_carousel_autoplay == 1 %} autoPlay: 3000, {% endif %}*/
/* 										navigation: true,*/
/* 										navigationText: ['<i class="fa fa-angle-left fa-5x"></i>', '<i class="fa fa-angle-right fa-5x"></i>'],*/
/* 										pagination: true,*/
/* 										loop:true,*/
/* 										afterInit: function(){*/
/* 											if ($('#{{ tabID ~ module }}').hasClass('secondary')) {*/
/* 												$('#{{ tabID ~ module }}').removeClass('active');*/
/* 											}*/
/* 										}*/
/* 									});*/
/* */
/* 									{% else %}*/
/* 										*/
/* 										if ($('#{{ tabID ~ module }}').hasClass('secondary')) {*/
/* 											$('#{{ tabID ~ module }}').removeClass('active');*/
/* 										}*/
/* */
/* 									{% endif %}*/
/* */
/* 								});*/
/* */
/* 							</script>*/
/* */
/* 					</div>*/
/* */
/* 				{% endif %} {# if $disabled #}*/
/* */
/* 		{% endfor %}*/
/* */
/* 	</div>*/
/* */
/* </div>*/
