<?php

/* clearshop/template/common/language.twig */
class __TwigTemplate_a417a95cceb277fd4c4798565cc7d9985d0bc116a56b80fb3a2b42ada3df102e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["languages"]) ? $context["languages"] : null)) > 1)) {
            // line 2
            echo "
<form action=\"";
            // line 3
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-language\">
  <div class=\"btn-group dropdown\">
    <button class=\"btn btn-link dropdown-toggle\" data-toggle=\"dropdown\">
    ";
            // line 6
            echo (isset($context["text_language"]) ? $context["text_language"] : null);
            echo " <span class=\"caret\"></span></button>
    <ul class=\"dropdown-menu dropdown-menu-right\">
      ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 9
                echo "      ";
                if (($this->getAttribute($context["language"], "code", array()) == (isset($context["code"]) ? $context["code"] : null))) {
                    // line 10
                    echo "        <li><span class=\"btn btn-block current\">";
                    echo $this->getAttribute($context["language"], "name", array());
                    echo "</span></li>
      ";
                } else {
                    // line 12
                    echo "        <li><button class=\"btn btn-link btn-block language-select\" type=\"button\" name=\"";
                    echo $this->getAttribute($context["language"], "code", array());
                    echo "\">";
                    echo $this->getAttribute($context["language"], "name", array());
                    echo "</button></li>
      ";
                }
                // line 14
                echo "      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "    </ul>
  </div>
  <input type=\"hidden\" name=\"code\" value=\"\" />
  <input type=\"hidden\" name=\"redirect\" value=\"";
            // line 18
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "\" />
</form>

";
        }
    }

    public function getTemplateName()
    {
        return "clearshop/template/common/language.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 18,  62 => 15,  56 => 14,  48 => 12,  42 => 10,  39 => 9,  35 => 8,  30 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if languages|length > 1 %}*/
/* */
/* <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-language">*/
/*   <div class="btn-group dropdown">*/
/*     <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">*/
/*     {{ text_language }} <span class="caret"></span></button>*/
/*     <ul class="dropdown-menu dropdown-menu-right">*/
/*       {% for language in languages %}*/
/*       {% if language.code == code %}*/
/*         <li><span class="btn btn-block current">{{ language.name }}</span></li>*/
/*       {% else %}*/
/*         <li><button class="btn btn-link btn-block language-select" type="button" name="{{ language.code }}">{{ language.name }}</button></li>*/
/*       {% endif %}*/
/*       {% endfor %}*/
/*     </ul>*/
/*   </div>*/
/*   <input type="hidden" name="code" value="" />*/
/*   <input type="hidden" name="redirect" value="{{ redirect }}" />*/
/* </form>*/
/* */
/* {% endif %}*/
/* */
