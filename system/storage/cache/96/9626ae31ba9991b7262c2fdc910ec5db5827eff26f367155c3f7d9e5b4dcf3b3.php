<?php

/* clearshop/template/extension/module/category_accordion.twig */
class __TwigTemplate_7bd6f06098b8c370c84c6700657d95315e1bd0419288c11bbaefa19c51dbe61e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\" src=\"catalog/view/theme/clearshop/js/jquery.dcjqaccordion.js\"></script> 

<div class=\"box category-accordion\">

\t<a class=\"btn btn-default visible-xs\" role=\"button\" data-toggle=\"collapse\" href=\"#category_accordion\" aria-expanded=\"false\" aria-controls=\"collapseCategoryAccordion\">";
        // line 5
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo " <i class=\"fa fa-angle-right\"></i></a>

\t<div class=\"box-content box-category collapse\" id=\"category_accordion\">";
        // line 7
        echo (isset($context["category_accordion"]) ? $context["category_accordion"] : null);
        echo "</div>
\t
</div>

<script type=\"text/javascript\">
\$(document).ready(function() {
\t\$('#cat_accordion').dcAccordion({
\t\tclassExpand : 'cid";
        // line 14
        echo (isset($context["category_accordion_cid"]) ? $context["category_accordion_cid"] : null);
        echo "',
\t\tmenuClose: false,
\t\tautoClose: true,
\t\tsaveState: false,
\t\tdisableLink: false,\t\t
\t\tautoExpand: true
\t});
});
</script>
";
    }

    public function getTemplateName()
    {
        return "clearshop/template/extension/module/category_accordion.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 14,  30 => 7,  25 => 5,  19 => 1,);
    }
}
/* <script type="text/javascript" src="catalog/view/theme/clearshop/js/jquery.dcjqaccordion.js"></script> */
/* */
/* <div class="box category-accordion">*/
/* */
/* 	<a class="btn btn-default visible-xs" role="button" data-toggle="collapse" href="#category_accordion" aria-expanded="false" aria-controls="collapseCategoryAccordion">{{ heading_title }} <i class="fa fa-angle-right"></i></a>*/
/* */
/* 	<div class="box-content box-category collapse" id="category_accordion">{{ category_accordion }}</div>*/
/* 	*/
/* </div>*/
/* */
/* <script type="text/javascript">*/
/* $(document).ready(function() {*/
/* 	$('#cat_accordion').dcAccordion({*/
/* 		classExpand : 'cid{{ category_accordion_cid }}',*/
/* 		menuClose: false,*/
/* 		autoClose: true,*/
/* 		saveState: false,*/
/* 		disableLink: false,		*/
/* 		autoExpand: true*/
/* 	});*/
/* });*/
/* </script>*/
/* */
