<?php

		$data['tabs'][] = array(
			'id'		=> 'tab_general',
			'title'		=> $this->language->get('tab_general')
		);

		$data['tabs'][] = array(
			'id'		=> 'tab_support',
			'title'		=> $this->language->get('tab_support')
		);

		$data['fields'][] = array(
			'tab'			=> 'tab_general',
			'entry' 		=> $this->language->get('entry_status'),
			'type'			=> 'select',
			'name' 			=> 'status',
			'value' 		=> (isset($this->request->post['status'])) ? $this->request->post['status'] : $this->config->get($classname . '_status'),
			'required' 		=> false,
			'options'		=> array(
				'0' => $this->language->get('text_disabled'),
				'1' => $this->language->get('text_enabled')
			),
			'help'			=> ($this->language->get('help_status') != 'help_status' ? $this->language->get('help_status') : ''),
			'tooltip'		=> ($this->language->get('tooltip_status') != 'tooltip_status' ? $this->language->get('tooltip_status') : '')
		);

		foreach ($languages as $language_id => $language_name) {
			$data['fields'][] = array(
				'entry' 		=> '[ ' . $language_name . ' ] ' . $this->language->get('entry_title'),
				'type'			=> 'text',
				'size'			=> '20',
				'name' 			=> 'title_' . $language_id,
				'value' 		=> ((isset($this->request->post['title_' . $language_id])) ? $this->request->post['title_' . $language_id] : $this->config->get($classname . '_title_' . $language_id) ? $this->config->get($classname . '_title_' . $language_id) : ucwords(str_replace(array('-','_','.'), " ", $classname))),
				'required' 		=> false,
				'help'			=> ($this->language->get('help_title') != 'help_title' ? $this->language->get('help_title') : ''),
				'tooltip'		=> ($this->language->get('tooltip_title') != 'tooltip_title' ? $this->language->get('tooltip_title') : '')
			);
		}

		$data['fields'][] = array(
			'tab'			=> 'tab_general',
			'entry' 		=> $this->language->get('entry_tax_class'),
			'type'			=> 'select',
			'name' 			=> 'tax_class_id',
			'value' 		=> (isset($this->request->post['tax_class_id'])) ? $this->request->post['tax_class_id'] : $this->config->get($classname . '_tax_class_id'),
			'required' 		=> false,
			'options'		=> $tax_classes,
			'help'			=> ($this->language->get('help_tax_class') != 'help_tax_class' ? $this->language->get('help_tax_class') : ''),
			'tooltip'		=> ($this->language->get('tooltip_tax_class') != 'tooltip_tax_class' ? $this->language->get('tooltip_tax_class') : '')
		);

		$data['fields'][] = array(
			'tab'			=> 'tab_general',
			'entry'			=> $this->language->get('entry_sort_order'),
			'type'			=> 'text',
			'name'			=> 'sort_order',
			'value'			=> (isset($this->request->post['sort_order'])) ? $this->request->post['sort_order'] : $this->config->get($classname . '_sort_order'),
			'required'		=> false,
			'help'			=> ($this->language->get('help_sort_order') != 'help_sort_order' ? $this->language->get('help_sort_order') : ''),
			'tooltip'		=> ($this->language->get('tooltip_sort_order') != 'tooltip_sort_order' ? $this->language->get('tooltip_sort_order') : '')
		);

		$data['fields'][] = array(
			'tab'			=> 'tab_support',
			'entry'			=> 'Troubleshooting Info: ',
			'type'			=> 'label',
			'name'			=> 'troubleshooting',
			'value'			=> 'If you are not seeing an expected rate, check the /system/logs/zone_plus_debug.txt file using FTP. I log the reason for each time a rate is skipped for easy troubleshooting.',
			'help'			=> '',
			'tooltip'		=> ''
		);

		$data['fields'][] = array(
			'tab'			=> 'tab_support',
			'entry'			=> 'Support Info:',
			'type'			=> 'label',
			'name'			=> 'support',
			'value'			=> 'For support questions, contact me at qphoria@gmail.com or on skype: taqmobile',
			'help'			=> '',
			'tooltip'		=> ''
		);

		// Remove the All Zones
		unset($geo_zones[0]);

		foreach ($geo_zones as $key => $value) {

			$data['tabs'][] = array(
				'id'		=> 'tab_' . $key,
				'title'		=> $value
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_status'),
				'type'			=> 'select',
				'name' 			=> 'status_' . $key,
				'value' 		=> (isset($this->request->post['status_' . $key])) ? $this->request->post['status_' . $key] : $this->config->get($classname . '_status_' . $key),
				'required' 		=> false,
				'options'		=> array(
					'0' => $this->language->get('text_disabled'),
					'1' => $this->language->get('text_enabled')
				),
				'help'			=> ($this->language->get('help_status') != 'help_status' ? $this->language->get('help_status') : ''),
				'tooltip'		=> ($this->language->get('tooltip_status') != 'tooltip_status' ? $this->language->get('tooltip_status') : '')
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_method'),
				'type'			=> 'select',
				'name' 			=> 'method_' . $key,
				'value' 		=> (isset($this->request->post['method_' . $key])) ? $this->request->post['method_' . $key] : $this->config->get($classname . '_method_' . $key),
				'required' 		=> false,
				'options'		=> array(
					'weight' 	=> $this->language->get('text_method_weight'),
					'subtotal' 	=> $this->language->get('text_method_subtotal'),
					'total' 	=> $this->language->get('text_method_total'),
					'itemcount' => $this->language->get('text_method_itemcount')
				),
				'help'			=> ($this->language->get('help_method') != 'help_method' ? $this->language->get('help_method') : ''),
				'tooltip'		=> ($this->language->get('tooltip_method') != 'tooltip_method' ? $this->language->get('tooltip_method') : '')
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_calc'),
				'type'			=> 'select',
				'name' 			=> 'calc_' . $key,
				'value' 		=> (isset($this->request->post['calc_' . $key])) ? $this->request->post['calc_' . $key] : $this->config->get($classname . '_calc_' . $key),
				'required' 		=> false,
				'options'		=> array(
					'inc' 	=> $this->language->get('text_incremental'),
					'dec' 	=> $this->language->get('text_decremental')
				),
				'help'			=> ($this->language->get('help_calc') != 'help_calc' ? $this->language->get('help_calc') : ''),
				'tooltip'		=> ($this->language->get('tooltip_calc') != 'tooltip_calc' ? $this->language->get('tooltip_calc') : '')
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_cost'),
				'type'			=> 'textarea',
				'cols'			=> '100',
				'rows'			=> '5',
				'name' 			=> 'cost_' . $key,
				'value' 		=> (isset($this->request->post['cost_' . $key])) ? $this->request->post['cost_' . $key] : $this->config->get($classname . '_cost_' . $key),
				'required' 		=> false,
				'help'			=> ($this->language->get('help_cost') != 'help_cost' ? $this->language->get('help_cost') : ''),
				'tooltip'		=> ($this->language->get('tooltip_cost') != 'tooltip_cost' ? $this->language->get('tooltip_cost') : '')
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_filter'),
				'type'			=> 'select',
				'name' 			=> 'filter_' . $key,
				'value' 		=> (isset($this->request->post['filter_' . $key])) ? $this->request->post['filter_' . $key] : $this->config->get($classname . '_filter_' . $key),
				'required' 		=> false,
				'options'		=> array(
					'' 			=> $this->language->get('text_disabled'),
					'postcode' 	=> 'postcode',
					'city'		=> 'city',
					'address_1' => 'address_1',
					'address_2' => 'address_2',
					'zone'      => 'zone',
				),
				'help'			=> ($this->language->get('help_filter') != 'help_filter' ? $this->language->get('help_filter') : ''),
				'tooltip'		=> ($this->language->get('tooltip_filter') != 'tooltip_filter' ? $this->language->get('tooltip_filter') : '')
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_include'),
				'type'			=> 'text',
				'size'			=> '100',
				'name' 			=> 'include_' . $key,
				'value' 		=> (isset($this->request->post['include_' . $key])) ? $this->request->post['include_' . $key] : $this->config->get($classname . '_include_' . $key),
				'required' 		=> false,
				'help'			=> ($this->language->get('help_include') != 'help_include' ? $this->language->get('help_include') : ''),
				'tooltip'		=> ($this->language->get('tooltip_include') != 'tooltip_include' ? $this->language->get('tooltip_include') : '')
			);

			$data['fields'][] = array(
				'tab'			=> 'tab_' . $key,
				'entry' 		=> $this->language->get('entry_exclude'),
				'type'			=> 'text',
				'size'			=> '100',
				'name' 			=> 'exclude_' . $key,
				'value' 		=> (isset($this->request->post['exclude_' . $key])) ? $this->request->post['exclude_' . $key] : $this->config->get($classname . '_exclude_' . $key),
				'required' 		=> false,
				'help'			=> ($this->language->get('help_exclude') != 'help_exclude' ? $this->language->get('help_exclude') : ''),
				'tooltip'		=> ($this->language->get('tooltip_exclude') != 'tooltip_exclude' ? $this->language->get('tooltip_exclude') : '')
			);

		}

?>