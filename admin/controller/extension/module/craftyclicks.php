<?php
/**
 * UK Postcode Lookup by Crafty Clicks
 *
 * @author	  	Crafty Clicks Limited
 * @link		https://craftyclicks.co.uk
 * @copyright  	Copyright (c) 2016, Crafty Clicks Limited
 * @license		Licensed under the terms of the MIT license.
 * @version		3.0.0
**/

class ControllerExtensionModuleCraftyclicks extends Controller {
	private $error = array();
	public function index() {
		// Loading the language file of craftyclicks
		$this->load->language('extension/module/craftyclicks');

		// Set the title of the page to the heading title in the Language file i.e., Hello World
		$this->document->setTitle($this->language->get('heading_title'));

		// Load the Setting Model  (All of the OpenCart Module & General Settings are saved using this Model )
		$this->load->model('setting/setting');

		// Start If: Validates and check if data is coming by save (POST) method
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			// Parse all the coming data to Setting Model to save it in database.
			$this->model_setting_setting->editSetting('module_craftyclicks', $this->request->post);

			// To display the success text on data save
			$this->session->data['success'] = $this->language->get('text_success');

			// Redirect to the Module Listing
			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		// Assign the language data for parsing it to view
		$ccTextValues = array(
			'heading_title',
			'text_edit',
			'text_enabled',
			'text_disabled',
			'entry_status',
			'label_access_token'
		);
		$data['craftyAdminLabels'] = array();
		foreach ($ccTextValues as $elem) {
			$data['craftyAdminLabels'][$elem] = $this->language->get($elem);
		}

		//Loading variables
		$ccVariableValues = array(
			'module_craftyclicks_status',
			'module_craftyclicks_access_token'
		);
		$data['craftyValues'] = array();
		foreach ($ccVariableValues as $elem){
			$data['craftyValues'][$elem] = $this->loadFromConfig($data, $elem);
		}

		//For Admin Panel locale_lookup
		$configValues = array(
			'access_token',
			'status'
		);

		$data['craftyclicksConfig']= array();
		foreach ($configValues as $crafty) {
			$data['craftyclicksConfig'][$crafty] = $this->config->get('module_craftyclicks_'.$crafty);
		}

		// This Block returns the warning if any
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		// This Block returns the error code if any
		if (isset($this->error['code'])) {
			$data['error_code'] = $this->error['code'];
		} else {
			$data['error_code'] = '';
		}

		// Making of Breadcrumbs to be displayed on site
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'	  => $this->language->get('text_home'),
			'href'	  => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
			'separator' => false
		);
		$data['breadcrumbs'][] = array(
			'text'	  => $this->language->get('text_module'),
			'href'	  => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true),
			'separator' => ' :: '
		);
		$data['breadcrumbs'][] = array(
			'text'	  => $this->language->get('heading_title'),
			'href'	  => $this->url->link('extension/module/craftyclicks', 'user_token=' . $this->session->data['user_token'], true),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/module/craftyclicks', 'user_token=' . $this->session->data['user_token'], true); // URL to be directed when the save button is pressed
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true); // URL to be redirected when cancel button is pressed

		if (isset($this->request->post['module_craftyGlobal_status'])) {
			$data['module_craftyclicks_status'] = $this->request->post['module_craftyclicks_status'];
		} else {
			$data['module_craftyclicks_status'] = $this->config->get('module_craftyclicks_status');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/craftyclicks', $data));
	}

	protected function validate() {

		if (!$this->user->hasPermission('modify', 'extension/module/craftyclicks')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	private function loadFromConfig(&$data, $key) {
		$data[$key] = $this->config->get($key);
	}
}
