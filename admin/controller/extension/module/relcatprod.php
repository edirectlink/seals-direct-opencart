<?php
class ControllerExtensionModulerelcatprod extends Controller { 	
	private $error = array();
	private $modpath = 'module/relcatprod'; 
	private $modtpl = 'module/relcatprod.tpl';
	private $modtplform = 'module/relcatprodform.tpl';	
	private $modname = 'relcatprod';
	private $modtext = 'Related Categories At Product';
	private $modid = '33973';
	private $modssl = 'SSL';
	private $modemail = 'opencarttools@gmail.com';
	private $token_str = '';
	private $modurl = 'extension/module';
	private $modurltext = '';

	public function __construct($registry) {
		parent::__construct($registry);
 		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3') { 
			$this->modtpl = 'extension/module/relcatprod';
			$this->modtplform = 'extension/module/relcatprodform';
			$this->modpath = 'extension/module/relcatprod';
		} else if(substr(VERSION,0,3)=='2.2') {
			$this->modtplform = 'module/relcatprodform';
			$this->modtpl = 'module/relcatprod';
		} 
		
		if(substr(VERSION,0,3)>='3.0') { 
			$this->modname = 'module_relcatprod';
			$this->modurl = 'marketplace/extension'; 
			$this->token_str = 'user_token=' . $this->session->data['user_token'] . '&type=module';
		} else if(substr(VERSION,0,3)=='2.3') {
			$this->modurl = 'extension/extension';
			$this->token_str = 'token=' . $this->session->data['token'] . '&type=module';
		} else {
			$this->token_str = 'token=' . $this->session->data['token'];
		}
		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
			$this->modssl = true;
		} 
 	} 
	
	public function index() {
		$data = $this->load->language($this->modpath);
		$this->modurltext = $this->language->get('text_extension');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting($this->modname, $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			if(! (isset($this->request->post['svsty']) && $this->request->post['svsty'] == 1)) {
				$this->response->redirect($this->url->link($this->modurl, $this->token_str, $this->modssl));
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');
 		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
 		$data['entry_status'] = $this->language->get('entry_status');
  		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', $this->token_str, $this->modssl)
		);
		
		$data['breadcrumbs'][] = array(
			'text' => $this->modurltext,
			'href' => $this->url->link($this->modurl, $this->token_str, $this->modssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->modpath, $this->token_str, $this->modssl)
		);

		$data['action'] = $this->url->link($this->modpath, $this->token_str, $this->modssl);
		
		$data['cancel'] = $this->url->link($this->modurl, $this->token_str , $this->modssl); 
		
		if(substr(VERSION,0,3)>='3.0') { 
			$data['user_token'] = $this->session->data['user_token'];
		} else {
			$data['token'] = $this->session->data['token'];
		}

		$data[$this->modname.'_status'] = $this->setvalue($this->modname.'_status');
		$data[$this->modname.'_img_w'] = $this->setvalue($this->modname.'_img_w');
		$data[$this->modname.'_img_h'] = $this->setvalue($this->modname.'_img_h');
		$data[$this->modname.'_desc_len'] = $this->setvalue($this->modname.'_desc_len');
		$data[$this->modname.'_newwin'] = $this->setvalue($this->modname.'_newwin');
 		
 		$data['modname'] = $this->modname;
		$data['modemail'] = $this->modemail;
  		  
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view($this->modtpl, $data));
	} 
	
	public function getform() { 		
		$data[$this->modname.'_status'] = $this->setvalue($this->modname.'_status');
		if($data[$this->modname.'_status']) { 

			$this->load->model('catalog/category');
			$this->load->model('extension/relcatprod');
			
			$this->load->language($this->modpath);
			$data['entry_relcatprod_title'] = $this->language->get('entry_relcatprod_title');
			$data['entry_relcatprod_category_id'] = $this->language->get('entry_relcatprod_category_id');
			
			if(substr(VERSION,0,3)>='3.0') { 
				$data['user_token'] = $this->session->data['user_token'];
			} else {
				$data['token'] = $this->session->data['token'];
			}
			
			$this->load->model('localisation/language');
			$languages = $this->model_localisation_language->getLanguages();
			foreach($languages as $language) {
				if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') {
					$imgsrc = "language/".$language['code']."/".$language['code'].".png";
				} else {
					$imgsrc = "view/image/flags/".$language['image'];
				}
				$data['languages'][] = array("language_id" => $language['language_id'], "name" => $language['name'], "imgsrc" => $imgsrc);
			} 
			
			$relcatprod_data = array();
			if (isset($this->request->get['product_id'])) {
				$relcatprod_data = $this->model_extension_relcatprod->getdata($this->request->get['product_id']);
			}
			//print_r($relcatprod_data);exit;
			
			if (isset($this->request->post['relcatprod_title'])) {
				$data['relcatprod_title'] = $this->request->post['relcatprod_title'];
			} elseif (isset($this->request->get['product_id']) && $relcatprod_data) {
				$data['relcatprod_title'] = json_decode($relcatprod_data['relcatprod_title'], true);
			} else {
				$data['relcatprod_title'] = '';
			} 
			
			if (isset($this->request->post['relcatprod_category_str_id'])) {
				$categories = $this->request->post['relcatprod_category_str_id'];
			} elseif (isset($this->request->get['product_id']) && $relcatprod_data) {
				$categories = ($relcatprod_data['relcatprod_category_str_id']) ? explode(",", $relcatprod_data['relcatprod_category_str_id']) : array();
			} else {
				$categories = array();
			}  
			
			$data['relcatprod_category_str_ids'] = array();

			if($categories) {
				foreach ($categories as $category_id) {
					$related_info = $this->model_catalog_category->getCategory($category_id);
		
					if ($related_info) {
						$data['relcatprod_category_str_ids'][] = array(
							'category_id' => $related_info['category_id'],
							'name' => $related_info['name']
						);
					}
				}
			}
			
			return ($this->load->view($this->modtplform, $data));
		} 
	} 
	
	protected function setvalue($postfield) {
		if (isset($this->request->post[$postfield])) {
			$postfield_value = $this->request->post[$postfield];
		} else {
			$postfield_value = $this->config->get($postfield);
		} 	
 		return $postfield_value;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', $this->modpath)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function install() {
		$query = $this->db->query("SHOW TABLES LIKE '".DB_PREFIX."relcatprod' ");
		if(!$query->num_rows){
			$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."relcatprod` (
			  `relcatprod_id` int(11) NOT NULL AUTO_INCREMENT,
 			  `product_id` int(11),
			  `relcatprod_title` text,
			  `relcatprod_category_str_id` TEXT,
  			  PRIMARY KEY (`relcatprod_id`)
			  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8");
		}
		
		@mail($this->modemail,
		"Extension Installed",
		"Hello!" . "\r\n" .  
		"Extension Name :  ".$this->modtext."" ."\r\n". 
		"Extension ID : ".$this->modid ."\r\n". 
		"Installed At : " .HTTP_CATALOG ."\r\n". 
		"Version : " . VERSION. "\r\n". 
		"Licence Start Date : " .date("Y-m-d") ."\r\n".  
		"Licence Expiry Date : " .date("Y-m-d", strtotime('+1 year'))."\r\n". 
		"From: ".$this->config->get('config_email'),
		"From: ".$this->config->get('config_email'));      
	}
	public function uninstall() { 
		$this->db->query("DROP TABLE `".DB_PREFIX."relcatprod` ");
		
		@mail($this->modemail,
		"Extension Uninstalled",
		"Hello!" . "\r\n" .  
		"Extension Name :  ".$this->modtext."" ."\r\n". 
		"Extension ID : ".$this->modid ."\r\n". 
		"Installed At : " .HTTP_CATALOG ."\r\n". 
		"Version : " . VERSION. "\r\n". 
		"Licence Start Date : " .date("Y-m-d") ."\r\n".  
		"Licence Expiry Date : " .date("Y-m-d", strtotime('+1 year'))."\r\n". 
		"From: ".$this->config->get('config_email'),
		"From: ".$this->config->get('config_email'));        
	}  
}