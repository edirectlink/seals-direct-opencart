<?php
class ControllerExtensionModuleContactUs extends Controller {
    private $error = array();

	public function index() {
		$this->load->model('setting/setting');
		$this->load->model("extension/module/contactus");
		$this->load->language('extension/module/contactus');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$data['heading_title'] = $this->language->get('heading_title');
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['input_option'])) {
		    if(isset($this->request->post['input_delete'])) {
			    foreach ($this->request->post['input_delete'] as $delete) {
		           $this->model_extension_module_contactus->delOptions($delete);
			    }
			}
			
			foreach (array_combine($this->request->post['input_order'], $this->request->post['input_option']) as $order => $option) {
                if (is_numeric($order) && $option != '') {
				    $this->model_extension_module_contactus->setOptions($order, $option);
				}
            }
			
			$this->model_extension_module_contactus->clearOptions();
			$this->session->data['success'] = $this->language->get('success_options');
			$this->response->redirect($this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['input_upload']) && $this->validate()) {
		    $size = $this->request->post['input_size'] * 1048576;
		    $this->model_extension_module_contactus->setUpload($this->request->post['input_upload'],$this->request->post['input_number'],$size);
			$this->session->data['success'] = $this->language->get('success_upload');
			$this->response->redirect($this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['input_folder']) && $this->validate()) {
		    if (!isset($this->error['badfolder'])) {
			    $root = $this->language->get('root');
				$this->model_extension_module_contactus->setFolder($root,$this->request->post['input_folder']);
				$this->session->data['success'] = $this->language->get('success_upload');
			    $this->response->redirect($this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true));
			}
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['file-refresh'])) {
		    $this->session->data['success'] = $this->language->get('success_refresh');
		    $this->response->redirect($this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true));
		}

		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['file-delete'])) {
            $results = $this->model_extension_module_contactus->getParams();
		    foreach ($results as $result) {
			   	$folder = $result['folder'];
			}
			$root = $this->language->get('root');
			for ($x = 0; $x <= $this->request->post['files'] - 1; $x++) {
			    $file = "file".(string)$x;
                if(isset($this->request->post[$file])) {
				    $filename = $root.$folder.$this->request->post[$file];
					$delfile = $this->deleteFile($filename);
				}
            } 
			$this->session->data['success'] = $this->language->get('success_delete');
		    $this->response->redirect($this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true));
		}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['input_email']) && $this->validate()) {
		    if (!isset($this->error['noemail']) && !isset($this->error['bademail'])) {
				$this->model_extension_module_contactus->setEmail($this->request->post['input_email'],$this->request->post['acknemail'],$this->request->post['copyemail']);
				$this->session->data['success'] = $this->language->get('success_email');
			    $this->response->redirect($this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true));
		    }
		}
		
		if (isset($this->error['badfolder'])) {
			$data['error_badfolder']         = $this->error['badfolder'];
		} else {
			$data['error_badfolder']         = '';
		}
		
		if (isset($this->error['nofolder'])) {
			$data['error_nofolder']         = $this->error['nofolder'];
		} else {
			$data['error_nofolder']         = '';
		}
		
		if (isset($this->error['noemail'])) {
			$data['error_noemail']          = $this->error['noemail'];
		} else {
			$data['error_noemail']          = '';
		}
		
		if (isset($this->error['bademail'])) {
			$data['error_bademail']         = $this->error['bademail'];
		} else {
			$data['error_bademail']         = '';
		}
		
		$results = $this->model_extension_module_contactus->getOptions();
		$data['options'] = array();
		foreach ($results as $result) {
			$data['options'][]              = array(
			    'order'                     => $result['order'],
				'option'                    => $result['option']
			);
		}
		$options = $this->model_extension_module_contactus->getOptions();
		
		$results = $this->model_extension_module_contactus->getParams();
		foreach ($results as $result) {
		    $data['upload']                 = $result['upload'];
			$data['number']                 = $result['number'];
		    $data['size']                   = $result['size'] / 1048576;
			$data['folder']                 = $result['folder'];
			$data['adminemail']             = $result['adminemail'];
			$data['acknemail']              = $result['acknemail'];
			$data['copyemail']              = $result['copyemail'];
			
			$folder                         = $result['folder'];
			$email                          = $result['adminemail'];
		}
		
		
		
		
		if (!$email) {
		    $email = $this->config->get('config_email');
			$data['adminemail']             = $email;
		}
        
		$root = $this->language->get('root');
        
	if ($folder) {
		$dir = $root.$folder;

		$data['files'] = array();

		$dirlist = $this->getFileList($dir);
		foreach ($dirlist as $file) {
		    $filename = $file['name'];
            $mime_type = $this->mime_content_type($filename);
			$param = explode($folder, $filename);
			$epoch = gmdate('Y/m/d G:i', $file['lastmod']); 
			$data['files'][] = array(
			    'name'         => $param[1],
			    'mime'         => $mime_type,
			    'lastmod'      => $epoch,
				'href'         => $this->url()."/".$folder.$param[1]
		    );
		}
	}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['breadcrumbs']   = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		$data['action'] = $this->url->link('extension/module/contactus', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view('extension/module/contactus', $data));
	}
	
	protected function validate() {
	    
		if (isset($this->request->post['input_folder'])) {
		    if(!preg_match('%^[A-Za-z0-9-/]+$%', $this->request->post['input_folder'])) {
                $this->error['badfolder']        = $this->language->get('error_badfolder');
		    }
		}
		
		if (isset($this->request->post['input_upload'])) {
		    if($this->request->post['input_upload'] == 1) {
			   $results = $this->model_extension_module_contactus->getParams();
		       foreach ($results as $result) {
			   	   $folder                          = $result['folder'];
			   }
			   if(!$folder) {
			       $this->error['nofolder']         = $this->language->get('error_nofolder');
			   }
			}
		}
		
		
		if (isset($this->request->post['input_email'])) {
		    if($this->request->post['input_email'] == '') {
			    $this->error['noemail']        = $this->language->get('error_noemail');
			} else {
			    $emails = explode(",", $this->request->post['input_email']);
			    foreach ($emails as $email) {
			        $email = preg_replace('/\s+/','',$email);
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
//				    if (!preg_match('/^[^\@]+@.*.[a-z]{2,15}$/i', $email)) {
			            $this->error['bademail']       = $this->language->get('error_bademail');
		            }
                }
			}
		}
		
		return !$this->error;	
	}
	
	public function getFileList($dir) {
    // array to hold return value
    $retval = array();

    // add trailing slash if missing
    if(substr($dir, -1) != "/") $dir .= "/";

    // open pointer to directory and read list of files
    $d = @dir($dir) or die("getFileList: Failed opening directory $dir for reading");
    while(false !== ($entry = $d->read())) {
      // skip hidden files
      if($entry[0] == ".") continue;
      if(is_dir("$dir$entry")) {
        $retval[] = array(
          "name" => "$dir$entry/",
          "type" => filetype("$dir$entry"),
          "size" => 0,
          "lastmod" => filemtime("$dir$entry")
        );
      } elseif(is_readable("$dir$entry")) {
        $retval[] = array(
          "name" => "$dir$entry",
          "size" => filesize("$dir$entry"),
          "lastmod" => filemtime("$dir$entry")
        );
      }
    }
    $d->close();

    return $retval;
  }
  
  public function mime_content_type($filename) {
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
			'docx' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
			'xlsx' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

		$arr = pathinfo($filename, PATHINFO_EXTENSION);
		$ext = strtolower($arr);
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }

	public function deleteFile($file) {
		if(is_file($file) && @unlink($file)){
            return " - Success";
        } else if (is_file ($file)) {
            return " - Failed";
        } else {
           return " - Does not exist";
        }
	}
	
	public function url(){
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['HTTP_HOST'];
    }
		
	public function install() {
        $this->load->model("extension/module/contactus");
        $this->model_extension_module_contactus->createSchema();
    }
 
    public function uninstall() {
        $this->load->model("extension/module/contactus");
        $this->model_extension_module_contactus->deleteSchema();
    }
}

