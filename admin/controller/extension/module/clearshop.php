<?php

class ControllerExtensionModuleClearshop extends Controller {
	
	private $error = array(); 
	
	public function index() {   
		
		//Set the title from the language file $_['heading_title'] string
		$language = $this->load->language('extension/module/clearshop');

		$this->document->setTitle($this->language->get('heading_title'));
		//$data = array_merge($data, $language);

		//Load the settings model. You can also add any other models you want to load here.
		$this->load->model('setting/setting');

		// Multilanguage
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->load->model('tool/image');    
		
		   
		// Multistore
		
		if (isset($this->request->post['store_id'])) {
			$data['store_id'] = $this->request->post['store_id'];
		} else {
			$data['store_id'] = $this->config->get('clearshop_store_id');
		}
		
		$data['stores'] = array();
		
		$this->load->model('setting/store');
				  
		$results = $this->model_setting_store->getStores();

		$data['stores'][] = array(
			'name' => 'Default',
			'href' => '',
			'store_id' => 0
		);

		foreach ($results as $result) {
			$data['stores'][] = array(
				'name' => $result['name'],
				'href' => $result['url'],
				'store_id' => $result['store_id']
			);
		}		


		if(isset($_GET['store_id'])) {
			$data['store_id'] = $_GET['store_id'];
		} else {
			if (isset($_GET['submit'])) {
				$data['store_id'] = $data['store_id'];
			} else {
				$data['store_id'] = 0;
			}
		}

		$data['fonts'] = array(
			'Arial'                 => 'Arial',
			'Verdana'               => 'Verdana',
			'Helvetica'             => 'Helvetica',
			'Lucida+Grande'         => 'Lucida Grande',
			'Trebuchet+MS'          => 'Trebuchet MS',
			'Times+New+Roman'       => 'Times New Roman',
			'Tahoma'                => 'Tahoma',
			'Georgia'               => 'Georgia',
			''                      => '-- GOOGLE FONTS --',
			'Abel'                  => 'Abel',
			'Abril+Fatface'         => 'Abril Fatface',
			'Acme'                  => 'Acme',
			'Adamina'               => 'Adamina',
			'Advent+Pro'            => 'Advent Pro',
			'Alfa+Slab+One'         => 'Alfa Slab One',
			'Alice'                 => 'Alice',
			'Allan'                 => 'Allan',
			'Amaranth'              => 'Amaranth',
			'Amatic+SC'             => 'Amatic SC',
			'Andika'                => 'Andika',
			'Anonymous+Pro'         => 'Anonymous Pro',
			'Anton'                 => 'Anton',
			'Arimo'                 => 'Arimo',
			'Bangers'               => 'Bangers',
			'Basic'                 => 'Basic',
			'Baumans'               => 'Baumans',
			'Belgrano'              => 'Belgrano',
			'Berkshire+Swash'       => 'Berkshire Swash',
			'Bitter'                => 'Bitter',
			'Boogaloo'              => 'Boogaloo',
			'Brawler'               => 'Brawler',
			'Bree+Serif'            => 'Bree Serif',
			'Bubblegum+Sans'        => 'Bubblegum Sans',
			'Buda'                  => 'Buda',
			'Cabin+Condensed'       => 'Cabin Condensed',
			'Cabin+Sketch'          => 'Cabin Sketch',
			'Caudex'                => 'Caudex',
			'Cinzel'                => 'Cinzel',
			'Comfortaa'             => 'Comfortaa',
			'Contrail+One'          => 'Contrail One',
			'Courgette'             => 'Courgette',
			'Coustard'              => 'Coustard',
			'Crushed'               => 'Crushed',
			'Cuprum'                => 'Cuprum',
			'Damion'                => 'Damion',
			'Days+One'              => 'Days One',
			'Didact+Gothic'         => 'Didact Gothic',
			'Dorsa'                 => 'Dorsa',
			'Droid+Sans'            => 'Droid Sans',
			'Droid+Serif'           => 'Droid Serif',
			'Duru+Sans'             => 'Duru Sans',
			'Enriqueta'             => 'Enriqueta',
			'Exo'                   => 'Exo',
			'Exo+2'                 => 'Exo 2',
			'Federo'                => 'Federo',
			'Francois+One'          => 'Francois One',
			'Fredericka+the+Great'  => 'Fredericka the Great',
			'Fredoka+One'           => 'Fredoka One',
			'Goudy+Bookletter+1911' => 'Goudy Bookletter 1911',
			'Gruppo'                => 'Gruppo',
			'Homenaje'              => 'Homenaje',
			'Imprima'               => 'Imprima',
			'Inder'                 => 'Inder',
			'Istok+Web'             => 'Istok Web',
			'Jockey+One'            => 'Jockey One',
			'Josefin+Slab'          => 'Josefin Slab',
			'Just+Another+Hand'     => 'Just Another Hand',
			'Kaushan+Script'        => 'Kaushan Script',
			'Kotta+One'             => 'Kotta One',
			'Lato'                  => 'Lato',
			'Lemon'                 => 'Lemon',
			'Lobster+Two'           => 'Lobster Two',
			'Lobster'               => 'Lobster',
			'Maiden+Orange'         => 'Maiden Orange',
			'Marvel'                => 'Marvel',
			'Merienda+One'          => 'Merienda One',
			'Molengo'               => 'Molengo',
			'Montserrat'            => 'Montserrat',
			'News+Cycle'            => 'News Cycle',
			'Niconne'               => 'Niconne',
			'Nixie+One'             => 'Nixie One',
			'Nobile'                => 'Nobile',
			'Nunito'                => 'Nunito',
			'Oleo+Script'           => 'Oleo Script',
			'Open+Sans'             => 'Open Sans',
			'Overlock'              => 'Overlock',
			'Ovo'                   => 'Ovo',
			'PT+Sans'               => 'PT Sans',
			'Passion+One'           => 'Passion One',
			'Philosopher'           => 'Philosopher',
			'Playball'              => 'Playball',
			'Poiret+One'            => 'Poiret One',
			'Quando'                => 'Quando',
			'Quattrocento+Sans'     => 'Quattrocento Sans',
			'Quicksand'             => 'Quicksand',
			'Raleway'               => 'Raleway',
			'Righteous'             => 'Righteous',
			'Roboto'                => 'Roboto',
			'Roboto+Slab'           => 'Roboto Slab',
			'Rokkitt'               => 'Rokkitt',
			'Ropa+Sans'             => 'Ropa Sans',
			'Sansita+One'           => 'Sansita One',
			'Sofia'                 => 'Sofia',
			'Source+Sans+Pro'       => 'Source Sans Pro',
			'Stoke'                 => 'Stoke',
			'Ubuntu'                => 'Ubuntu',
			'Wire+One'              => 'Wire One',
			'Yanone+Kaffeesatz'     => 'Yanone Kaffeesatz',
			'Yellowtail'            => 'Yellowtail'
			); 
		
		$data['skins'] = array(
		''       => 'Default',
		'ribbon' => 'Ribbon',
		'boxed'  => 'Boxed'
		);

		$data['product_details'] = array(
			'tabs'      => 'Tabs',
			'accordion' => 'Accordion'
		);
		$data['sidebars_width'] = array(
			'3' => 'Normal',
			'2' => 'Narrow'
		);

		$data['product_layouts'] = array(
			'1' => '2 columns',
			'2' => '3 columns',
			'3' => 'full width'
		);
		$data['visibility'] = array(
			'all'     => 'All',
			'desktop' => 'Desktop',
			'mobile'  => 'Mobiles'
		);
		$data['responsive'] = array(
			'large'  => 'Responsive large (>1200px)',
			'normal' => 'Responsive regular (960px)'
		);
		$data['zoom'] = array(
			'right'  => 'Right',
			'inside' => 'Inside'
		);
		$data['category_style'] = array(
			'grid' => 'Grid',
			'list' => 'List'
		);
		$data['logo_position'] = array(
			'center'  => 'Center',
			'left' => 'Left'
		);
		$data['searchbox_position'] = array(
		'top' => 'Top header',
		'navbar' => 'On navigation bar'
	);
		$data['homepage_link'] = array(
			'icon'  => 'Icon',
			'text' => 'Text',
			'hidden' => 'Don\'t show'
		);
		$data['menu_category_style'] = array(
			'inline'  => 'Inline',
			'table' => 'Dropdown Table',
			'hidden' => 'Don\'t show'
		);
		$data['menu_infopages_style'] = array(
			'inline'  => 'Inline',
			'vertical' => 'Vertical',
			'hidden' => 'Don\'t show'
		);
		$data['menu_brands_style'] = array(
			'logo' => 'Logo',
			'name' => 'Name',
			'logoname' => 'Logo & Name',
			'hidden' => 'Don\'t show'
		);
		$data['menu_target'] = array(
			'_self' => 'Same tab/window',
			'_blank' => 'New tab/window'
		);
		$data['menu_style'] = array(
			'light'  => 'Light',
			'dark' => 'Dark',
			'transparent' => 'Transparent'
		);
		$data['prevnext_style'] = array(
			'full'    => 'Full',
			'compact' => 'Compact'
		);
		$data['social_media'] = array(
			'facebook'    => 'Facebook',
			'twitter'     => 'Twitter',
			'google-plus' => 'Google+',
			'rss'         => 'RSS',
			'pinterest'   => 'Pinterest',
			'instagram'   => 'Instagram',
			'linkedin'    => 'Linkedin',
			'vimeo'       => 'Vimeo',
			'youtube'     => 'YouTube',
			'flickr'      => 'Flickr',
			'vk'          => 'VK',
			'dribbble'    => 'Dribbble',
			'spotify'     => 'Spotify',
			'tumblr'      => 'Tumblr',
			'xing'        => 'Xing',
			'wordpress'   => 'WordPress',
		);
			
		$data['text_image_manager'] = 'Image manager';
		$data['user_token'] = $this->session->data['user_token'];
		
		$data['modules'] = array();

		// store config data
		
		$config_data = array(

		'clearshop',

		'module_clearshop_status',

		'clearshop_skins',
		'clearshop_product_details_layout',
		'clearshop_responsive_layout',

		'clearshop_sidebar_left_width',
		'clearshop_sidebar_right_width',

		'clearshop_product_layout',

		'clearshop_category_style',

		'clearshop_product_columns',
		'clearshop_display_cart_button',
		'clearshop_subcat_status',
		'clearshop_subcat_thumbnails_status',
		'clearshop_subcat_thumb_width',
		'clearshop_subcat_thumb_height',
		'clearshop_rollover_images',

		'clearshop_quickview_categories',
		'clearshop_quickview_modules',
		'clearshop_quickview_mobile',
		
		'clearshop_menu_categories',
		'clearshop_menu_category_icons',
		'clearshop_menu_categories_x_row',
		'clearshop_categories_top_title',
		'clearshop_categories_tag',
		'clearshop_categories_tag_color',
		'clearshop_3rd_level_cat',
		
		'clearshop_menu_brands',
		'clearshop_menu_brands_x_row',
		'clearshop_brands_top_title',
		'clearshop_brands_tag',
		'clearshop_brands_tag_color',
			
		'clearshop_sticky_menu',
		'clearshop_fullwidth_dropdown_menu',
		'clearshop_menu_link',
		'clearshop_homepage_link_style',
		'clearshop_menu_dropdown_status',
		'clearshop_menu_dropdown',
		'clearshop_menu_dropdown_title',
		'clearshop_menu_dropdown_tag',
		'clearshop_menu_dropdown_tag_color',
		
		'clearshop_custom_block',
		'clearshop_custom_block_status',
		'clearshop_custom_block_title',
		'clearshop_custom_block_tag',
		'clearshop_custom_block_tag_color',

		'clearshop_menu_infopages',
		'clearshop_infopages_top_title',
		'clearshop_infopages_tag',
		'clearshop_infopages_tag_color',

		'clearshop_custom_colors',

		'clearshop_background_color',
		'clearshop_thickbar_color',

		'clearshop_title_color',
		'clearshop_bodytext_color',
		'clearshop_lighttext_color',

		'clearshop_menu_color',
		'clearshop_menu_color',
		'clearshop_menu_hover',
		'clearshop_dropdown_color',
		'clearshop_dropdown_hover',
		'clearshop_cart_count_background',
		'clearshop_cart_count_color',
		'clearshop_cart_total_color',

		'clearshop_links_color',

		'clearshop_footer_text_color',
		'clearshop_footer_links_color',
		'clearshop_content_links_color',

		'clearshop_button_normal_bg',
		'clearshop_button_hover_bg',
		'clearshop_button_text_color',

		'clearshop_2button_normal_bg',
		'clearshop_2button_hover_bg',
		'clearshop_2button_text_color',

		'clearshop_product_name_color',
		'clearshop_normal_price_color',
		'clearshop_old_price_color',
		'clearshop_new_price_color',
		'clearshop_onsale_background_color',
		'clearshop_onsale_text_color',

		'clearshop_show_wishlist',
		'clearshop_show_compare',
		'clearshop_show_sale_bubble',

		'clearshop_category_box_border',
		'clearshop_category_box_background',

		'clearshop_categories_menu_color',
		'clearshop_categories_sub_color',
		'clearshop_categories_active_color',

		'clearshop_active_tab_color',

		'clearshop_carousel_dots_normal',
		'clearshop_carousel_dots_hover',
		'clearshop_carousel_dots_active',

		'clearshop_pattern_overlay',
		'clearshop_custom_image',
		'clearshop_custom_pattern',
		'clearshop_image_preview',
		'clearshop_pattern_preview',
		'clearshop_full_background',
		
		'clearshop_title_font',
		'clearshop_title_font_size',
		'clearshop_body_font',
		'clearshop_body_font_size',
		'clearshop_small_font',
		'clearshop_small_font_size',
		'clearshop_cyrillic_fonts',

		'clearshop_featured_carousel',
		'clearshop_bestseller_carousel',
		'clearshop_latest_carousel',
		'clearshop_special_carousel',
		'clearshop_related_carousel',
		'clearshop_ebay_carousel',

		'clearshop_carousel_autoplay',

		'clearshop_facebook_id',
		'clearshop_facebook_page',
		'clearshop_facebook_icon',
		'clearshop_facebook_button',
		'clearshop_twitter_username',
		'clearshop_gplus_id',
		'clearshop_youtube_username',
		'clearshop_tumblr_username',
		'clearshop_skype_username',
		'clearshop_pinterest_id',
		'clearshop_instagram_username',
		'clearshop_custom_icon_class',
		'clearshop_custom_icon_url',
		'clearshop_custom_icon_color',

		'clearshop_footer_modules',
		'clearshop_footer_info_text',

		'clearshop_payment_logos',
		'clearshop_copyright',

		'clearshop_social',
		'clearshop_social_footer',
		'clearshop_facebook_widget',
		'clearshop_custom_icon',

		'clearshop_slider',
		'clearshop_slider_speed',
		'clearshop_slider_transition_time',

		'clearshop_responsive',
		'clearshop_cloud_zoom',
		'clearshop_zoom_position',
		'clearshop_zoom_width',
		'clearshop_zoom_heigth',
		'clearshop_zoom_autosize',
		'clearshop_prevnext_mode',        

		'clearshop_subcat_thumbs',
		'clearshop_display_shadows',
		'clearshop_logo_position',
		'clearshop_search_navbar',
		'clearshop_select_mobile_menu',
		'clearshop_searchbox',
		'clearshop_searchbox_position',
		'clearshop_header_info_text',

		'clearshop_toggle_sidebar',

		'clearshop_address',
		'clearshop_phone1',
		'clearshop_phone2',
		'clearshop_email',
		'clearshop_contact_custom_text',

		'clearshop_twitter_widget_id',
		'clearshop_twitter_theme',
		'clearshop_tweets_number',
		'clearshop_show_twitter_feed',
		'clearshop_twitter_icon',

		'clearshop_custom_stylesheet',
		'clearshop_custom_css',
		'clearshop_custom_css_status',
		'clearshop_custom_js',
		'clearshop_custom_js_status'

		);
		
			$data['clearshop_conf'] = $this->model_setting_setting->getSetting('clearshop', $data['store_id']);
		
		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$data[$conf] = $this->request->post[$conf];
			} else {
				if(isset($data['clearshop_conf'][$conf])) {
					$data[$conf] = $data['clearshop_conf'][$conf];
				} else {
					$data[$conf] = false;
				}
			}
		}
	

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
					
			$data['array'] = array(
				'clearshop_store_id' => $this->request->post['store_id']
			);

			$this->model_setting_setting->editSetting('clearshop', $this->request->post, $this->request->post['store_id']);	
			
			$this->model_setting_setting->editSetting('clearshop_store_id', $data['array']);	

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module/clearshop&submit=true', 'user_token=' . $this->session->data['user_token'], 'true'));
		}
	
		//This creates an error message. The error['warning'] variable is set by the call to function validate() in this controller (below)
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		//Set up breadcrumb trail.
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);
		
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/clearshop', 'user_token=' . $this->session->data['user_token'], true)
		);
		
		$data['action'] = $this->url->link('extension/module/clearshop', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$this->load->model('design/layout');
		
		$data['layouts'] = $this->model_design_layout->getLayouts();

		
		if (isset($data['clearshop_custom_pattern']) && $data['clearshop_custom_pattern'] != "" && file_exists(DIR_IMAGE . $data['clearshop_custom_pattern'])) {
			$data['clearshop_pattern_preview'] = $this->model_tool_image->resize($data['clearshop_custom_pattern'], 70, 70);
		} else {
			$data['clearshop_pattern_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		}


		if (isset($data['clearshop_custom_image']) && $data['clearshop_custom_image'] != "" && file_exists(DIR_IMAGE . $data['clearshop_custom_image'])) {
			$data['clearshop_image_preview'] = $this->model_tool_image->resize($data['clearshop_custom_image'], 70, 70);
		} else {
			$data['clearshop_image_preview'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);
		}
		
		$data['placeholder'] = $this->model_tool_image->resize('no_image.jpg', 70, 70);

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		//Send the output.
		$this->response->setOutput($this->load->view('extension/module/clearshop', $data));
	}
	
	/*
	 * 
	 * This function is called to ensure that the settings chosen by the admin user are allowed/valid.
	 * You can add checks in here of your own.
	 * 
	 */
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/clearshop')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}   
	}

	public function install() {

		// Blog

		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog` ( ";
		$sql .= "`blog_id` int(11) NOT NULL AUTO_INCREMENT, ";
		$sql .= "`allow_comment` int(1) NOT NULL DEFAULT '1', ";
		$sql .= "`count_read` int(11) NOT NULL DEFAULT '0', ";
		$sql .= "`sort_order` int(3) NOT NULL, ";
		$sql .= "`status` int(1) NOT NULL DEFAULT '1', ";
		$sql .= "`author` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL, ";
		$sql .= "`date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', ";
		$sql .= "`image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL, ";
		$sql .= "PRIMARY KEY (`blog_id`) ";
		$sql .= ") ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_category` ( ";
		$sql .= "`blog_category_id` int(11) NOT NULL AUTO_INCREMENT, ";
		$sql .= "`parent_id` int(11) NOT NULL DEFAULT '0', ";
		$sql .= "`sort_order` int(3) NOT NULL DEFAULT '0', ";
		$sql .= "`date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', ";
		$sql .= "`status` int(1) NOT NULL DEFAULT '1', ";
		$sql .= "PRIMARY KEY (`blog_category_id`) ";
		$sql .= ") ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=49 ; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_category_description` ( ";
		$sql .= "`blog_category_id` int(11) NOT NULL, ";
		$sql .= "`language_id` int(11) NOT NULL, ";
		$sql .= "`name` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '', ";
		$sql .= "`page_title` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '', ";
		$sql .= "`meta_keywords` varchar(255) COLLATE utf8_bin NOT NULL, ";
		$sql .= "`meta_description` varchar(255) COLLATE utf8_bin NOT NULL, ";
		$sql .= "`description` text COLLATE utf8_bin NOT NULL, ";
		$sql .= "PRIMARY KEY (`blog_category_id`,`language_id`), ";
		$sql .= "KEY `name` (`name`) ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_category_to_layout` ( ";
		$sql .= "`blog_category_id` int(11) NOT NULL, ";
		$sql .= "`store_id` int(11) NOT NULL, ";
		$sql .= "`layout_id` int(11) NOT NULL, ";
		$sql .= "PRIMARY KEY (`blog_category_id`,`store_id`) ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_category_to_store` ( ";
		$sql .= "`blog_category_id` int(11) NOT NULL, ";
		$sql .= "`store_id` int(11) NOT NULL, ";
		$sql .= "PRIMARY KEY (`blog_category_id`,`store_id`) ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_comment` ( ";
		$sql .= "`blog_comment_id` int(11) NOT NULL AUTO_INCREMENT, ";
		$sql .= "`blog_id` int(11) NOT NULL DEFAULT '0', ";
		$sql .= "`name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`email` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`comment` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`date_added` datetime DEFAULT '0000-00-00 00:00:00', ";
		$sql .= "`status` int(1) NOT NULL DEFAULT '1', ";
		$sql .= "PRIMARY KEY (`blog_comment_id`) ";
		$sql .= ") ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_description` ( ";
		$sql .= "`blog_id` int(11) NOT NULL, ";
		$sql .= "`language_id` int(11) NOT NULL, ";
		$sql .= "`title` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`page_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`meta_keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`meta_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`short_description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, ";
		$sql .= "`tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_related` ( ";
		$sql .= "`parent_blog_id` int(11) NOT NULL DEFAULT '0', ";
		$sql .= "`child_blog_id` int(11) NOT NULL DEFAULT '0' ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_to_category` ( ";
		$sql .= "`blog_id` int(11) NOT NULL, ";
		$sql .= "`blog_category_id` int(11) NOT NULL, ";
		$sql .= "PRIMARY KEY (`blog_id`,`blog_category_id`) ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_to_layout` ( ";
		$sql .= "`blog_id` int(11) NOT NULL, ";
		$sql .= "`store_id` int(11) NOT NULL, ";
		$sql .= "`layout_id` int(11) NOT NULL, ";
		$sql .= "PRIMARY KEY (`blog_id`,`store_id`) ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_related_products` ( ";
		$sql .= "`blog_id` int(11) NOT NULL, ";
		$sql .= "`related_id` int(11) NOT NULL, ";
		$sql .= "PRIMARY KEY (`blog_id`,`related_id`) ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin; ";
		$this->db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "blog_to_store` ( ";
		$sql .= "`blog_id` int(11) NOT NULL, ";
		$sql .= "`store_id` int(11) NOT NULL ";
		$sql .= ") ENGINE=MyISAM DEFAULT CHARSET=latin1; ";
		$this->db->query($sql);
		
		$sql  = "INSERT INTO  `" . DB_PREFIX . "layout` ( `layout_id` , `name` ) VALUES ( NULL , 'Blog' ); ";
		$query = $this->db->query( $sql );
			
		$id = $this->db->getLastId();
			
		$sql = "INSERT INTO `".DB_PREFIX."layout_route` (
					`layout_route_id` ,
					`layout_id` ,
					`store_id` ,
					`route`
					)
					VALUES (
					NULL , '".$id."', '0', 'blog/%');
			";
			$query = $this->db->query( $sql );

		// Newsletter

		$this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "newsletter (
				email varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '', 
				PRIMARY KEY (`email`)
			) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin
		");
		
		$this->load->model('user/user_group');

		$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'extension/newsletter');
		$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'extension/newsletter');

		// Testimonial

		$this->load->model('catalog/testimonial');
		$this->model_catalog_testimonial->createDatabaseTables();
		
		$this->model_user_user_group->addPermission($this->user->getId(), 'access', 'catalog/testimonial');
		$this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'catalog/testimonial');
	}

}
?>