<?php 
class ControllerPaymentInvoicepay extends Controller {
  const MODULE = 'invoicepay';
  const PREFIX = 'invoicepay';
  const MOD_FILE = 'pay_by_invoice';
  
	private $error = array(); 
  private $OC_V2;
  private $token;

  public function __construct($registry) {
		$this->OC_V2 = version_compare(VERSION, '2', '>=');
		parent::__construct($registry);
    $this->token = isset($this->session->data['user_token']) ? 'user_token='.$this->session->data['user_token'] : 'token='.$this->session->data['token'];
	}
  
	public function index() {
    // check tables
    $this->db_tables();
    
    // rights
    $this->load->model('user/user_group');
    
    if ($this->request->get['route'] == 'extension/payment/invoicepay') {
      if (!$this->user->hasPermission('modify', 'payment/invoicepay')) {
        $this->model_user_user_group->addPermission(version_compare(VERSION, '2.0.2', '>=') ? $this->user->getGroupId() : 1, 'access', 'payment/' . self::MODULE);
        $this->model_user_user_group->addPermission(version_compare(VERSION, '2.0.2', '>=') ? $this->user->getGroupId() : 1, 'modify', 'payment/' . self::MODULE);
      }
      
      if (version_compare(VERSION, '2', '>=')) {
        $this->response->redirect($this->url->link('payment/invoicepay', $this->token, 'SSL'));
			} else {
        $this->redirect($this->url->link('extension/payment', $this->token, 'SSL'));
			}
    }
    
    $data['_language'] = &$this->language;
		$data['_config'] = &$this->config;
		$data['_url'] = &$this->url;
		$data['token'] = $this->token;
		$data['OC_V2'] = $this->OC_V2;
    
    $asset_path = 'view/paybyinvoice/';
    
    if (version_compare(VERSION, '2', '<')) {
      //$this->document->addScript($asset_path . 'jquery-migrate.js');
      $this->document->addStyle($asset_path . 'awesome/css/font-awesome.min.css');
      $data['style_scoped'] = file_get_contents($asset_path . 'bootstrap.min.css');
			//$data['style_scoped'] .= str_replace('img/', $asset_path . 'img/', file_get_contents($asset_path . 'gkd-theme.css'));
			$data['style_scoped'] .= str_replace('img/', $asset_path . 'img/', file_get_contents($asset_path . 'style.css'));
			$this->document->addScript($asset_path . 'bootstrap.min.js');
		}
    
    if (version_compare(VERSION, '2.3', '>=')) {
      $this->document->addStyle('view/javascript/summernote/summernote.css');
			$this->document->addScript('view/javascript/summernote/summernote.js');
			$this->document->addScript('view/javascript/summernote/opencart.js');
    }
		
		$this->document->addScript('view/paybyinvoice/itoggle.js');
		$this->document->addStyle('view/paybyinvoice/awesome/css/font-awesome.min.css');
		$this->document->addStyle('view/paybyinvoice/style.css');
		
    if (version_compare(VERSION, '3', '>=')) {
      $this->load->language('extension/payment/invoicepay');
    } else {
      $this->load->language('payment/invoicepay');
    }

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('payment_invoicepay', array('payment_invoicepay_status' => $this->request->post['invoicepay_status']));				
			$this->model_setting_setting->editSetting('invoicepay', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

      if (version_compare(VERSION, '3', '>=')) {
        $this->response->redirect($this->url->link('marketplace/extension', 'type=payment&' . $this->token, 'SSL'));
      } else if (version_compare(VERSION, '2.3', '>=')) {
        $this->response->redirect($this->url->link('extension/extension', 'type=payment&' . $this->token, 'SSL'));
      } else if ($this->OC_V2) {
        $this->response->redirect($this->url->link('extension/payment', $this->token, 'SSL'));
			} else {
        $this->redirect($this->url->link('extension/payment', $this->token, 'SSL'));
			}
		}

		$this->load->model('localisation/language');
		$data['languages'] = $languages = $this->model_localisation_language->getLanguages();
    
    foreach ($data['languages'] as &$language) {
      if (version_compare(VERSION, '2.2', '>=')) {
        $language['image'] = 'language/'.$language['code'].'/'.$language['code'].'.png';
      } else {
        $language['image'] = 'view/image/flags/'. $language['image'];
      }
    }
    
    // customer groups
    if (version_compare(VERSION, '2.1', '>=')) {
      $this->load->model('customer/customer_group');
      $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();
      array_unshift($data['customer_groups'], array('customer_group_id' => '0', 'name' => $this->language->get('text_guest')));
    } else {
      $this->load->model('sale/customer_group');
      $data['customer_groups'] = $this->model_sale_customer_group->getCustomerGroups();
      array_unshift($data['customer_groups'], array('customer_group_id' => '0', 'name' => $this->language->get('text_guest')));
    }
		
    // pdf invoice installed ?
    if (version_compare(VERSION, '3', '>=')) {
      $this->load->model('setting/extension');
      $extensions = $this->model_setting_extension->getInstalled('module');
    } else if (version_compare(VERSION, '2', '>=')) {
     $this->load->model('extension/extension');
     $extensions = $this->model_extension_extension->getInstalled('module');
    } else {
      $this->load->model('setting/extension');
      $extensions = $this->model_setting_extension->getInstalled('module');
    }
    
		$data['pdf_invoice_installed'] = in_array('pdf_invoice', $extensions) || in_array('pdf_invoice_lite', $extensions);
    
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_all_zones'] = $this->language->get('text_all_zones');
		
		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_order_status'] = $this->language->get('entry_order_status');		
		$data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['breadcrumbs'] = array();

    $data['breadcrumbs'][] = array(
      'text'      => $this->language->get('text_home'),
      'href'      => $this->url->link('common/home', $this->token, 'SSL'),
      'separator' => false
    );
    
    if (version_compare(VERSION, '3', '>=')) {
      $extension_link = 'marketplace/extension';
    } else if (version_compare(VERSION, '2.3', '>=')) {
      $extension_link = 'extension/extension';
    } else {
      $extension_link = 'extension/payment';
    }
    
    $data['breadcrumbs'][] = array(
      'text'      => $this->language->get('text_payment'),
      'href'      => $this->url->link($extension_link, $this->token, 'SSL'),
      'separator' => ' :: '
    );

    $data['breadcrumbs'][] = array(
      'text'      => $this->language->get('heading_title'),
      'href'      => $this->url->link('payment/invoicepay', $this->token, 'SSL'),
      'separator' => ' :: '
    );
		
    $data['action'] = $this->url->link('payment/invoicepay', $this->token, true);
		
		$data['cancel'] = $extension_link;
		
		if (isset($this->request->post['invoicepay_customergroup'])) {
			$data['invoicepay_customergroup'] = $this->request->post['invoicepay_customergroup'];
		} else {
			$data['invoicepay_customergroup'] = $this->config->get('invoicepay_customergroup');
		}
		
		if (isset($this->request->post['invoicepay_order_notify'])) {
			$data['invoicepay_order_notify'] = (isset($this->request->post['invoicepay_order_notify'])) ? true : false;
		} else {
			$data['invoicepay_order_notify'] = $this->config->get('invoicepay_order_notify');
		}
		
		foreach ($languages as $lang) { 
			if (isset( $this->request->post[ 'invoicepay_notify_message_'.$lang['language_id'] ])) {
				$data[ 'invoicepay_notify_message_'.$lang['language_id'] ] = $this->request->post[ 'invoicepay_notify_message_'.$lang['language_id'] ];
			} else {
				$data[ 'invoicepay_notify_message_'.$lang['language_id'] ] = $this->config->get('invoicepay_notify_message_'.$lang['language_id']);
			}
			
			if (isset( $this->request->post[ 'invoicepay_desc_'.$language['language_id'] ])) {
				$data[ 'invoicepay_desc_'.$lang['language_id'] ] = $this->request->post[ 'invoicepay_desc_'.$lang['language_id'] ];
			} else {
				$data[ 'invoicepay_desc_'.$lang['language_id'] ] = $this->config->get('invoicepay_desc_'.$lang['language_id']);
			}
		}
		
		/*
		if (isset($this->request->post['invoicepay_order_status_id'])) {
			$data['invoicepay_order_status_id'] = $this->request->post['invoicepay_order_status_id'];
		} else {
			$data['invoicepay_order_status_id'] = $this->config->get('invoicepay_order_status_id'); 
		}
		*/
		
		if (isset($this->request->post['invoicepay_paid_status_id'])) {
			$data['invoicepay_paid_status_id'] = $this->request->post['invoicepay_paid_status_id'];
		} else {
			$data['invoicepay_paid_status_id'] = $this->config->get('invoicepay_paid_status_id'); 
		} 

		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		if (isset($this->request->post['invoicepay_geo_zone_id'])) {
			$data['invoicepay_geo_zone_id'] = $this->request->post['invoicepay_geo_zone_id'];
		} else {
			$data['invoicepay_geo_zone_id'] = $this->config->get('invoicepay_geo_zone_id'); 
		} 
		
		$this->load->model('localisation/geo_zone');
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		
		if (isset($this->request->post['invoicepay_status'])) {
			$data['invoicepay_status'] = $this->request->post['invoicepay_status'];
		} else {
			$data['invoicepay_status'] = $this->config->get('invoicepay_status');
		}
    
    if (isset($this->request->post['invoicepay_forcepdf'])) {
			$data['invoicepay_forcepdf'] = $this->request->post['invoicepay_forcepdf'];
		} else {
			$data['invoicepay_forcepdf'] = $this->config->get('invoicepay_forcepdf');
		}
		
		if (isset($this->request->post['invoicepay_total'])) {
			$data['invoicepay_total'] = $this->request->post['invoicepay_total'];
		} else {
			$data['invoicepay_total'] = $this->config->get('invoicepay_total');
		}
    
    if (isset($this->request->post['invoicepay_po'])) {
			$data['invoicepay_po'] = $this->request->post['invoicepay_po'];
		} else {
			$data['invoicepay_po'] = $this->config->get('invoicepay_po');
		}
		
		if (isset($this->request->post['invoicepay_sort_order'])) {
			$data['invoicepay_sort_order'] = $this->request->post['invoicepay_sort_order'];
		} else {
			$data['invoicepay_sort_order'] = $this->config->get('invoicepay_sort_order');
		}
    
    if (version_compare(VERSION, '2', '>=')) {
			$data['header'] = $this->load->controller('common/header');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['footer'] = $this->load->controller('common/footer');
			
      if (version_compare(VERSION, '3', '>=')) {
        $this->config->set('template_engine', 'template');
        $this->response->setOutput($this->load->view('payment/invoicepay', $data));
      } else {
        $this->response->setOutput($this->load->view('payment/invoicepay.tpl', $data));
      }
		} else {
			$data['column_left'] = '';
			$this->data = &$data;
			$this->template = 'payment/invoicepay.tpl';
			$this->children = array(
				'common/header',
				'common/footer'
			);
      
			$this->response->setOutput($this->render());
		}
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/invoicepay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
	
  public function db_tables() {
    if (!$this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "order` LIKE 'po_number'")->row)
      $this->db->query("ALTER TABLE `" . DB_PREFIX . "order` ADD `po_number` VARCHAR(64) NULL");
  }
  
	public function install($redir = false) {
    // rights
    $this->load->model('user/user_group');

    $this->model_user_user_group->addPermission(version_compare(VERSION, '2.0.2', '>=') ? $this->user->getGroupId() : 1, 'access', 'payment/' . self::MODULE);
    $this->model_user_user_group->addPermission(version_compare(VERSION, '2.0.2', '>=') ? $this->user->getGroupId() : 1, 'modify', 'payment/' . self::MODULE);
    
		$this->load->model('setting/setting');
		
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();
		
		$img_link = (defined('_JEXEC')) ? HTTP_CATALOG . 'components/com_mijoshop/opencart/image/data/invoicepay_sips.png' : HTTP_CATALOG.'image/data/invoicepay_sips.png';
		
		$ml_settings = array();
		foreach($languages as $language)
		{
			//$ml_settings['invoicepay_desc_'.$language['language_id']] = '';
		}
		
		$this->model_setting_setting->editSetting('invoicepay', array(
			'invoicepay_mode' => 'test',
		) + $ml_settings);
		
		// generate bug on 1.5.1
		//$this->redirect($this->url->link('module/pdf_invoice', $this->token, 'SSL'));
    
    if ($redir) {
      if (version_compare(VERSION, '2', '>=')) {
				$this->response->redirect($this->url->link('payment/'.self::MODULE, $this->token, 'SSL'));
			} else {
				$this->redirect($this->url->link('payment/'.self::MODULE, $this->token, 'SSL'));
			}
    }
	}
}