<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
<?php if(!empty($style_scoped)) { ?><style scoped><?php echo $style_scoped; ?></style><?php } ?>
<ul class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
  <?php } ?>
</ul>
<div class="<?php if (version_compare(VERSION, '2', '>=')) echo 'container-fluid' ?>">
	<?php if (isset($success) && $success) { ?><div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><script type="text/javascript">setTimeout("$('.alert-success').slideUp();",5000);</script><?php } ?>
	<?php if (isset($info) && $info) { ?><div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $info; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><?php } ?>
	<?php if (isset($error) && $error) { ?><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><?php } ?>
    <?php if (isset($error_warning) && $error_warning) { ?><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><?php } ?>
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="pull-right">
      <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
    </div>
    <h3 class="panel-title"><i class="fa fa-file-text-o"></i> <?php echo $heading_title; ?></h3>
  </div>
    <div class="content panel-body">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab-0" data-toggle="tab"><i class="fa fa-cog"></i><?php echo $_language->get('tab_1'); ?></a></li>
        <li><a href="#tab-1" data-toggle="tab"><i class="fa fa-sliders"></i><?php echo $_language->get('tab_2'); ?></a></li>
        <li><a href="#tab-about" data-toggle="tab"><i class="fa fa-info"></i><?php echo $_language->get('tab_about'); ?></a></li>
      </ul>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <div class="tab-content">
    <div class="tab-pane active" id="tab-0">
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><input class="switch" type="checkbox" name="invoicepay_status" id="invoicepay_status" value="1" <?php if($invoicepay_status) echo 'checked="checked"' ?> /></td>
          </tr>
          <tr>
            <td><?php echo $_language->get('entry_paid_status'); ?></td>
            <td>
              <select name="invoicepay_paid_status_id" class="form-control">
                <?php foreach ($order_statuses as $order_status) { ?>
                <?php if ($order_status['order_status_id'] == $invoicepay_paid_status_id) { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </td>
          </tr>
          <tr>
            <td><?php echo $entry_geo_zone; ?></td>
            <td><select name="invoicepay_geo_zone_id" class="form-control">
                <option value="0"><?php echo $text_all_zones; ?></option>
                <?php foreach ($geo_zones as $geo_zone) { ?>
                <?php if ($geo_zone['geo_zone_id'] == $invoicepay_geo_zone_id) { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td><?php echo $_language->get('entry_customer_group'); ?></td>
            <td>
              <?php $i=0; foreach ($customer_groups as $group) { ?>
                <span style="color:#555"><?php echo $group['name']; ?></span>
                <input class="switch" type="checkbox"  id="customergroup<?php echo $i++; ?>" name="invoicepay_customergroup[<?php echo $group['customer_group_id']; ?>]" value="<?php echo $group['customer_group_id']; ?>" <?php if(in_array($group['customer_group_id'], (array) $invoicepay_customergroup)) echo 'checked="checked"'; ?> />
							<?php } ?>
            </td>
          </tr>
          <tr>
            <td><?php echo $_language->get('entry_total'); ?></td>
            <td><input type="text" name="invoicepay_total" value="<?php echo $invoicepay_total; ?>"  class="form-control"/></td>
          </tr>
          <tr>
            <td><?php echo $_language->get('entry_purchase_order'); ?></td>
            <td>
              <select name="invoicepay_po" class="form-control">
                <option value=""><?php echo $_language->get('purchase_order_disabled'); ?></option>
                <option value="on" <?php echo ($invoicepay_po == 'on') ? 'selected="selected"' : ''; ?>><?php echo $_language->get('purchase_order_on'); ?></option>
                <option value="required" <?php echo ($invoicepay_po == 'required') ? 'selected="selected"' : ''; ?>><?php echo $_language->get('purchase_order_required'); ?></option>
              </select>
            </td>
          </tr>
          <tr>
            <td><?php echo $_language->get('entry_pdf_invoice'); ?></td>
            <td>
              <?php if ($pdf_invoice_installed) { ?>
                <input class="switch" type="checkbox" name="invoicepay_forcepdf" id="invoicepay_forcepdf" value="1" <?php if($invoicepay_forcepdf) echo 'checked="checked"' ?> />
              <?php } else { ?>
                <span style="color:#555"><?php echo $_language->get('entry_pdf_invoice_required'); ?></span>
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td><?php echo $entry_sort_order; ?></td>
            <td><input type="text" name="invoicepay_sort_order" value="<?php echo $invoicepay_sort_order; ?>" class="form-control"/></td>
          </tr>
        </table>
      </div>
       <div class="tab-pane" id="tab-1">
        <table class="form">
          <tr>
            <td><?php echo $_language->get('entry_order_notify'); ?></td>
            <td><input class="switch" type="checkbox" name="invoicepay_order_notify" id="invoicepay_order_notify" value="1" <?php if($invoicepay_order_notify) echo 'checked="checked"' ?> /></td>
          </tr>
        </table>
        <ul class="nav nav-tabs">
          <?php $f=1; foreach ($languages as $language) { ?>
          <li <?php if($f) echo 'class="active"'; $f=0; ?>><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="<?php echo $language['image']; ?>" alt=""/> <?php echo $language['name']; ?></a></li>
          <?php } ?>
        </ul>
        <div class="tab-content">
          <?php $f=1; foreach ($languages as $language) { ?>
          <div id="language<?php echo $language['language_id']; ?>" class="tab-pane <?php if($f) echo ' active'; $f=0; ?>">
            <table class="form">
              <tr>
                <td><?php echo $_language->get('entry_notify_message'); ?></td>
                <td>
                  <?php if(defined('_JEXEC')) {
                    //echo AceShop::get('base')->editor()->display("invoicepay_notify_message_".$language['language_id'], ${'invoicepay_notify_message_'.$language['language_id']}, '97%', '320', '50', '11'); 
                    $desc = isset(${'invoicepay_notify_message_'.$language['language_id']}) ? ${'invoicepay_notify_message_'.$language['language_id']} : '';
                    echo MijoShop::get('base')->editor()->display("invoicepay_notify_message_".$language['language_id'], $desc, '97% !important', '320', '50', '11');
                   } else { ?>
                    <textarea name="invoicepay_notify_message_<?php echo $language['language_id']; ?>" id="notify<?php echo $language['language_id']; ?>"><?php echo ${'invoicepay_notify_message_'.$language['language_id']}; ?></textarea>
                  <?php } ?>
                </td>
              </tr>
              <tr>
                  <td>
            <?php echo $_language->get('entry_desc'); ?><br>
          </td>
                <td>
                  <?php if(defined('_JEXEC')) {
                    //echo AceShop::get('base')->editor()->display("invoicepay_desc_".$language['language_id'], ${'invoicepay_desc_'.$language['language_id']}, '97%', '320', '50', '11'); 
                    $desc = isset(${'invoicepay_desc_'.$language['language_id']}) ? ${'invoicepay_desc_'.$language['language_id']} : '';
                    echo MijoShop::get('base')->editor()->display("invoicepay_desc_".$language['language_id'], $desc, '97% !important', '320', '50', '11');
                   } else { ?>
                    <textarea name="invoicepay_desc_<?php echo $language['language_id']; ?>" id="desc<?php echo $language['language_id']; ?>"><?php echo ${'invoicepay_desc_'.$language['language_id']}; ?></textarea>
                  <?php } ?>
                </td>
              </tr>
            </table>
          </div>
          <?php } ?>
        </div>
      </div>
      <div class="tab-pane" id="tab-about">
        <table class="form about">
          <tr>
            <td colspan="2" style="text-align:center;padding:30px 0 50px"><!--img src="view/image/payment/paybyinvoice.png" alt=""/-->Pay by Invoice</td>
          </tr>
          <tr>
            <td>Version</td>
            <td>1.2.1</td>
          </tr>
          <tr>
            <td>Free support</td>
            <td>I take care of maintaining my modules at top quality and affordable price.<br/>In case of bug, incompatibility, or if you want a new feature, just contact me on my mail.</td>
          </tr>
          <tr>
            <td>Contact</td>
            <td><a href="mailto:support@geekodev.com">support@geekodev.com</a></td>
          </tr>
          <tr>
            <td>Links</td>
            <td>
              If you like this module, please consider to make a star rating <span style="position:relative;top:3px;width:80px;height:17px;display:inline-block;background:url(data:data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNy8wNy8xMrG4sToAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAACr0lEQVQ4jX1US0+TURA98/Xri0KBYqG8BDYItBoIBhFBBdRNTTQx0Q0gujBiAkEXxoXxD6iJbRcaY1iQEDXqTgwQWkWDIBU3VqWQoEgECzUU+n5910VbHhacZHLvzD05c+fMzaVhgxYJIwIYi+8B8FJ5bzjob9ucB4DmLttGMGyoAGMsyc1G7bEvA91roz2NL7Y7TziHHSxFmWsorbuUFgn79BaTLnMn3LYEZqPukCKruFAUGEd54w1ekqK69x8CSkoqMnJv72noTmN+O9Q5KlE44GqxmHTS7Qho5MH+X8SJUuMhAIbM/CrS1tSnCYsmkOoUnO7SiP3dHV8Mw5AoKkRCfTwR96ei+ZZGVVDDJQhIWAVbfhjDe8eQnd/Aq8+/VAIsAcGbR8ejQiR8jcwGbYZEkTFVd7I9B4IXcL+GEPwdK4SN0XJSDaCoAvHZsA4/93hWHNVNnbZpjoG5gl7XvpFnxggxAZRaA0rokliIAIkaxMnwdWLE7XW77jd12qYBgCMiNHfZlhgTCkZfPfUDBAYGItoiL0lK8N0+51txzD1u7Ji8njTGpk6bg/iUhSiU4GT5YOtPL940AOfiDyHod9/dMsYEzmLS5bBoKE/ES8ECCyACSF4IFledAdhd2SIFUdtmAp7i92QM+uKqVg6RJXDKakCcjyjSwcldMUDgG7I0h8WKdI0ewM2kFuTpmlb1bp2UMYBJyjBjm/FYh57MjA/1+1wuESNZOfjoLPwe516zUSdLIgi6l+sl3CIW5leD7/v7HPNTE+cOtr8tDXhWy+zWAcvnDx/XoiEPiirPBomgXxd32KAFEWp3FR0YdP60pop4sfHI5cmr+MfMRl2tXKnqzS5pyFuaHRusu2A5EyeoAEAQS2Q94VDg4pY/YUOf9ZgxnBaJJSeOdny6AgB/AYEpKtpaTusRAAAAAElFTkSuQmCC)"></span> on the module page :]<br/><br/>
              <!-- links -->
              <!--b>Module page :</b> <a target="new" href="https://www.opencart.com/index.php?route=extension/extension/info&extension_id=4794">Pay by Invoice</a><br/-->
              <b>Other modules :</b> <a target="new" href="https://www.opencart.com/index.php?route=marketplace/extension&filter_member=GeekoDev">My modules on opencart</a><br/>
            </td>
          </tr>
        </table>
      </div>
       </div>
       </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$('input.switch').iToggle({easing: 'swing',speed: 200});

$('select[name=invoicepay_mode]').change(function(){
	if(this.value == 'test') $('.test_disable').attr('disabled', 'disabled');
	else $('.test_disable').removeAttr('disabled');
});
$('select[name=invoicepay_mode]').trigger('change');
--></script>
<?php if (!$OC_V2) { ?>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<?php } ?>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
	<?php foreach (array('desc', 'notify') as $type) { ?>
  <?php if (version_compare(VERSION, '2', '>=')) { ?>
			$('#<?php echo $type . $language['language_id']; ?>').summernote({
				height: 300
			});
  <?php } else { ?>
    CKEDITOR.replace('<?php echo $type . $language['language_id']; ?>', {
      filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
      filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
    });
    <?php } ?>
	<?php } ?>
<?php } ?>
//--></script> 
<?php echo $footer; ?>