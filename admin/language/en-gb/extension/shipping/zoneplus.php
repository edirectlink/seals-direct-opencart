<?php
// Heading
$_['heading_title']    		  = 'Zone Shipping Plus (Q)';

// Text
$_['text_shipping']    		  = 'Shipping';
$_['text_success']     		  = 'Success: You have modified the extension!';
$_['text_incremental']    	  = 'incremental';
$_['text_decremental']    	  = 'decremental';
$_['text_method_subtotal'] 	  = 'subtotal-based';
$_['text_method_total'] 	  = 'taxed total-based';
$_['text_method_weight']   	  = 'weight-based';
$_['text_method_itemcount']   = 'itemcount-based';
$_['text_edit']    			  = 'Edit Shipping';

// Tab
$_['tab_support']        	  = 'Support';

// Entry
$_['entry_title']       	  = 'Title:';
$_['entry_cost']       		  = 'Cost:';
$_['entry_tax_class']         = 'Tax Class:';
$_['entry_geo_zone']   		  = 'Geo Zone:';
$_['entry_status']     		  = 'Status:';
$_['entry_sort_order'] 		  = 'Sort Order:';
$_['entry_method']       	  = 'Method:';
$_['entry_calc']          	  = 'Calculation:';
$_['entry_filter']         	  = 'Advanced Filter Mode:';
$_['entry_include']        	  = 'Filter Include:';
$_['entry_exclude']        	  = 'Filter Exclude:';

// Tooltip
$_['tooltip_title']        	  = 'The shipping title shown on the checkout shipping step';
$_['tooltip_cost']       	  = 'The cost per each unit of measurement based on the method selected. Comma delimited.';
$_['tooltip_tax_class']   	  = 'The tax class to tax the shipping cost';
$_['tooltip_geo_zone']   	  = 'The geo zone the customer must be in to see this rate';
$_['tooltip_status']     	  = 'Enable/Disable';
$_['tooltip_sort_order']      = 'The sort order on the checkout/shipping page';
$_['tooltip_method']       	  = 'Trigger by Subtotal, Weight, or Total items in cart';
$_['tooltip_calc']            = 'incremental - cost increases if cart value increases. decremental - cost decreases if cart value increases';
$_['tooltip_filter']          = 'Determines which address field should be filtered for advanced filtering.';
$_['tooltip_include']         = 'Only used when using Advanced Filter Mode. Will limit the rate to only these included values.';
$_['tooltip_exclude']         = 'Only used when using Advanced Filter Mode. Will limit the rate to only these excluded values.';

// Help
$_['help_cost']        		  = 'Comma delimited. (Value:Cost, Value:Cost, etc.) <br />Example: 5:10.00, 10:12.00, 999:15.00 means 5 or less is 10, 6 through 10 is 12, 13 through 999 is 15<br/><span style="color:red;">BE SURE YOU HAVE A HIGH CEILING. FOR YOUR LAST RATE, USE SOMETHING LIKE 999999:50.00 TO ENSURE ALL RANGES ARE COVERED.</span>';
$_['help_filter']      		  = 'Zone Plus can use parts of the customer address for more enhanced filtering. See the readme for more help on how to use the Advanced Filtering. For example, if you choose "postcode" here, it will compare the customer shipping postcode with the data you enter in the include/exclude fields, and allow or deny based on that information. If you do not want advanced filtering by address details, leave this disabled.';
$_['help_include']      	  = 'When using Advanced Filter Mode, enter partial matches (starting at the first character) that you want to <b>include</b>. For example, if you want to match postcodes from 90200 to 90300, you would enter "902,903". No longer needs the "~" symbol.';
$_['help_exclude']      	  = 'When using Advanced Filter Mode, enter partial matches (starting at the first character) that you want to <b>exclude</b>. For example, if you want to ignore postcodes from 90250 to 90260, you would enter "9025,9026". No longer needs the "!" symbol.';

// Error
$_['error_permission'] 		  = 'Warning: You do not have permission to modify this extension!';
?>