<?php
/**
 * UK Postcode Lookup by Crafty Clicks
 *
 * @author      Crafty Clicks Limited
 * @link        https://craftyclicks.co.uk
 * @copyright  	Copyright (c) 2016, Crafty Clicks Limited
 * @license    	Licensed under the terms of the MIT license.
 * @version		3.0.0
**/

// Heading
$_['heading_title']       = 'Crafty Clicks UK Postcode Lookup';

// Text
$_['text_module']			= 'Modules';
$_['text_edit']				= 'Settings';
$_['text_success']			= 'Modifications Complete';
$_['text_extensions']		= 'Extensions';

// Entry
$_['entry_status']		= 'Status:';

// Error
$_['error_permission']		= 'Warning: You do not have permission to modify the module';

//Crafty Clicks labels
$_['label_access_token']		= 'Access Token: ';
