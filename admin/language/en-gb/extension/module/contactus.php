<?php
// Heading
$_['heading_title']              = 'Contact Us Ultimate 3';

// Root
$_['root']                       = $_SERVER['DOCUMENT_ROOT'];

// Title
$_['title_welcome']              = 'Welcome to our \'Contact Us\' Extension';
$_['title_options']              = 'Set Dropdown Options';
$_['title_upload']               = 'Set Upload Options';
$_['title_folder']               = 'Upload Folder';
$_['title_current']              = 'Current Uploads';
$_['title_email']                = 'Email Settings';
$_['title_tutorial']             = '<font color="red"><b>Tutorial</b></font>';

// Text
$_['text_module']                = 'Modules';
$_['text_settings']              = 'Edit Contact Us settings';
$_['text_tabwelcome']            = 'Welcome';
$_['text_taboptions']            = 'Reason Dropdown Settings';
$_['text_tabupload']             = 'File Upload Settings';
$_['text_tabemail']              = 'Email Settings';
$_['text_welcome']               = 'Thank you,<br /><br />Contact Us is now installed<br /><br /><font color="green">Poplarman</font><br />poplarman@poplarweb.co.uk';
$_['text_options']               = 'In this section you can set the dropdown options which your Customer uses to let you know the subject of their contact.';
$_['text_addcomment']            = 'If you need to add more options, click \'Submit\' and return here.';
$_['text_upload']                = 'Upload Settings';
$_['text_folder']                = 'The current Uploads Folder is:';
$_['text_current']               = 'The Following Customer Uploads are in the Upload Folder:';
$_['text_noupload']              = 'There are on files in the Upload Folder:';
$_['text_email']                 = 'Set your preferred email settings here:';
$_['text_acknemail']             = 'Send Acknowledgement email?:';

// Tip
$_['tip_number']                 = '<b>Tip:</b> We recommend a low setting (1). You can always return here if you have a need for a larger number.';
$_['tip_size']                   = '<b>Tip:</b> Convention is for a max file size of 2 Megabyte. You can always return here if you have a need for a larger number.';
$_['tip_folder']                 = '<b>Tip:</b> Enter relative path. Folder will be created if not already existing.';
$_['tip_adminemail']             = '<b>Tip:</b> For more than one email, seperate with comma: e.g. admin@mysite.com, sales@mysite.com';

//Entry
$_['entry_options']              = 'Edit, delete, or add to your dropdown options:';
$_['entry_upload']               = 'Are Uploads allowed?';
$_['entry_number']               = 'Number of Uploads allowed:';
$_['entry_size']                 = 'Maximum Size of uploaded file (Megabyte):';
$_['entry_folder']               = 'Change Folder?';
$_['entry_adminemail']           = 'Enter Admin Email';
$_['entry_custemail']            = 'Send Customer email';
$_['entry_copyemail']            = 'Copy to Admin';

// Table
$_['table_date']                 = 'Date Uploaded';
$_['table_file']                 = 'File name';
$_['table_type']                 = 'Type of file';
$_['table_action']               = 'Action';
$_['table_delete']               = 'Delete';

// Tab
$_['tab_welcome']                = 'welcome';
$_['tab_upload']                 = 'upload';
$_['tab_email']                  = 'email';

// Button
$_['button_submit']              = 'Submit';
$_['button_refresh']             = 'Refresh';
$_['button_delete']              = 'Delete!';
$_['button_download']            = 'Download';

// Success
$_['success_options']            = 'Success - You\'ve modified the Option Settings';
$_['success_upload']             = 'Success - You\'ve modified the Upload Settings';
$_['success_refresh']            = 'Success - You\'ve refreshed the Uploads List';
$_['success_email']              = 'Success - You\'ve modified the Email Settings';
$_['success_delete']             = 'Success - You\'ve removed the file(s)';

// Error
$_['error_badfolder']            = 'The folder name can only contain "A-Z,a-z", "0-9" and "-" !';
$_['error_permission']           = 'Warning: You do not have permission to modify account module!';
$_['error_noemail']              = 'You haven\'t entered an email address!';
$_['error_bademail']             = 'Email address appears to be invalid!';
$_['error_nofolder']             = 'You can\'t turn on Uploads until you\'ve set the <b>Upload Folder</b> (see below)!';

// Tutorial
$_['text_tutorial']              = '
There are three stages to setting up <b>Contact Us Ultimate</b>:
<br /><br />
<b>1. Reason Dropdown Settings</b>
<br />
Here you set the reasons a visitor to your shop might choose to inform you of their reason for contacting you. They can be tailored to match your own shop usage.
<br /><br />
@ To <b>modify</b> an existing reason - just overtype it;
<br />@ To <b>add</b> a reason - give it an order number (they will be displayed to the customer in ascending order) and add the text. If you need more reasons once you\'ve completed all of the available rows just click submit.
<br />@ To <b>delete</b> a reason - tick the box to the right of the text.
<br /><br />
<b>2. File Upload Settings</b>
<br />
Here you decide whether to allow your visitors to upload files for you to view.
<br /><br />
@ <b>Are Uploads Allowed</b> A simple \'No\' or \'Yes\' answer. If set to \'No\' all of the following entries will be ignored.
<br />
If \'Yes\' you can also set the number of files a visitor might upload, and their maximum size (in Mb).
<br />
@ <b>Upload Folder</b> Note: <font color="red"><b>This must be set before you can set Uploads to \'Yes\'</b></font>
<br />
Enter a path to the folder into which you want the customer files uploaded.
<br />
The <b>base is your website root</b> in which case you might enter \'<b>/ContactUsFiles</b>\'.
<br />If your online shop is not at your website root you might want to enter \'<b>/<i>myshop</i>/ContactUsFiles</b>\'.
<br />Or, to place files into the image folder \'<b>/<i>myshop</i>/image/ContactUsFiles</b>\'.
<br />Always include the leading slash \'/\'.
<br /><br />
@ <b>Current Uploads</b> This will display all files currently within your nominated Upload Folder (or display \'None\').
<br />
Click \'<b>View</b>\' to view a file (opens in a new window).
<br />
Or, select the tickbox to delete a file.
<br /><br />
<b>3. Email Settings</b>
@ Enter the email address(es) of where you would like Contact Us submissions to be sent to.
<br />
@ The two select boxas are for a) <b>Send an email to your visitor</b> to say that you have received their <b>Contact us</b>, and b) To <b>send a copy of that email</b> to your Admin address above.
<br /><br />
Note: <b>The Logo displayed in your Contact Us messages is the one you set on the image tab of store Settings.</b>
<br /><br /><br />
Any problems contact us: <b>poplarman@poplarweb.co.uk</b>';