<?php
// Heading
$_['heading_title']		= 'Pay by Invoice';

// Text 
$_['text_payment']		= 'Payment';
$_['text_success']		= 'Success: You have modified atos account details!';
$_['text_invoicepay']			= '';
$_['text_default_desc']		= 'Restore default';

// Tabs
$_['tab_1']		= 'General';
$_['tab_2']		= 'Advanced';
$_['tab_about']	= 'About';

// Entry
$_['entry_customer_group']       = 'Customer group:<span class="help">This payment method will be available only for selected customer groups</span>';
$_['entry_order_notify'] 		    = 'Custom message:<span class="help">Add some message in the confirmation mail</span>';
$_['entry_notify_message'] 		  = 'Message included in email:';
$_['entry_desc'] 		    		    = 'Payment description:<span class="help">Choose what to display to user before order confirmation</span>';
$_['entry_total']               = 'Total:<span class="help">The checkout total the order must reach before this payment method becomes active.</span>';
$_['entry_purchase_order']      = 'Purchase order:<span class="help">Display the purchase order entry.</span>';
$_['purchase_order_disabled']   = 'Disabled';
$_['purchase_order_on']         = 'Enabled, not required';
$_['purchase_order_required']   = 'Enabled and required';
$_['entry_order_status']        = 'Order Status:<span class="help">Order status when order is confirmed by user</span>';
$_['entry_paid_status']         = 'Order Status:<span class="help">Order status when customer confirms the checkout</span>';
$_['entry_pdf_invoice']         = 'Attach pdf invoice:<span class="help">Add pdf invoice to the email independently from settings in pdf invoice module</span>';
$_['entry_pdf_invoice_required']= 'This option is available only if <a  target="blank" href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=16487">PDF Invoice Pro</a> or <a target="blank" href="http://www.opencart.com/index.php?route=extension/extension/info&extension_id=16487">PDF Invoice Lite</a> module is installed.';
$_['entry_geo_zone']            = 'Geo Zone:'; 
$_['text_all_zones']            = 'All zones';
$_['text_enabled']              = 'enabled';
$_['text_disabled']             = 'disabled';
$_['entry_status']              = 'Status:';
$_['entry_sort_order']          = 'Sort Order:';
$_['text_guest']                = 'Guest';

// Error 
$_['error_permission']          = 'Warning: You do not have permission to modify this payment!';
$_['error_required']            = 'This field must be specified!';