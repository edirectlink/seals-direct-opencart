<?php
$_['text_pubdown'] = 'Public Downloads';
$_['text_pubdown_info'] = 'Select the files you want to make directly available to download on product page. You can click or drag and drop files in the dropzone to the right to upload new files.';
$_['text_pubdown_dropbox'] = 'Upload files here';
$_['text_pubdown_delete'] = 'File unassigned to product, do you want to also delete this file from your server?';

$_['text_pubdown_error'] = 'Error:';
$_['text_pubdown_error_session'] = 'Error: session terminated?';
$_['text_pubdown_error_rights'] = 'Warning: no rights to public downloads, in order to use this feature please give read/write rights to product/public_download in <a href="%s">System > Users > User Groups</a>';
?>