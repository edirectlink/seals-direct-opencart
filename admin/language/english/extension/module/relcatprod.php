<?php
// Heading
$_['heading_title']    = 'Related Categories At Product';

$_['text_module']      = 'Modules';
$_['text_extension']   = 'Extensions';
$_['text_extension_doc']   = '<a class="btn btn-info" href="http://demo24.opencarttools.net/extimg/relcatprod/index.html" target="_blank"> Extension Documentation</a>';
$_['text_success']     = 'Success: You have modified Related Categories At Product Module!';
$_['text_edit']        = 'Edit Related Categories At Product Module';

// Entry 
$_['entry_status']     = 'Status';
$_['entry_img_w']     = 'Image Width';
$_['entry_img_h']     = 'Image Height';
$_['entry_desc_len']     = 'Description Text Limit';
$_['entry_newwin']     = 'Open In New Window ?';


$_['entry_relcatprod_title']    = 'Related Categories Title';
$_['entry_relcatprod_category_id']    = 'Choose Related Categories';
 
// Error 
$_['error_permission'] = 'Warning: You do not have permission to modify Related Categories At Product Module!';
$_['error_selectcheckbox'] = 'Select atleast 1 checkbox!';