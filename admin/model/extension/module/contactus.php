<?php
class ModelExtensionModuleContactUs extends Model {
    public function deleteSchema() {
      $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "contactus_select`");
	  $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "contactus_params`");
    }
   
	public function createSchema() {
        $this->db->query("
        CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "contactus_select` (
        `order` int(11) NOT NULL primary key,
	    `option` VARCHAR(256) DEFAULT NULL
        ) DEFAULT CHARSET=utf8");
	  
	    $this->db->query("TRUNCATE TABLE `" . DB_PREFIX . "contactus_select`");
	  
	    $this->db->query("
	    INSERT INTO `" . DB_PREFIX . "contactus_select` (`order`, `option`) VALUES
          (0,  'Please tell us why you are contacting us:'),
          (1,  'Details about a Product'),
		  (2,  'Details about Ordering'),
		  (3,  'Details about Payment'),
		  (4,  'Details about Shipping'),
          (5,  'Other')");

	    $this->db->query("
	    CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "contactus_params` (
	    `upload` boolean NOT NULL,
	    `number` INT(1) DEFAULT 1,
	    `size`   INT(1) DEFAULT 2097152,
		`folder` VARCHAR(256) DEFAULT '',
		`adminemail` TEXT(512),
	    `acknemail` VARCHAR(1) DEFAULT 0,
	    `copyemail` VARCHAR(1) DEFAULT 0
        ) DEFAULT CHARSET=utf8");
		
		$email = $this->config->get('config_email');
		
		$this->db->query("
	    INSERT INTO `" . DB_PREFIX . "contactus_params` (
		  `upload`, `number`, `size`, `folder`,`adminemail`,`acknemail`,`copyemail`) VALUES
          (0,1,2097152,'','$email',0,0)");
    }
   
    public function getOptions() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "contactus_select` ORDER BY `order` ASC");

		return $query->rows;
    }
   
    public function delOptions($order) {
	    // Temporarily set to 9a9b9c9d9e9f9 for avoidance of duplication, and later removal
		$this->db->query("UPDATE `" . DB_PREFIX . "contactus_select` SET `option` = '9a9b9c9d9e9f9' WHERE `order` = '$order'");
    }
   
    public function setOptions($order, $option) {
        $option  = preg_replace('/\r?\n/', '<br>', $option);
        $option  = addslashes($option);
		
	    // Check if not deleted (set to 9a9b9c9d9e9f9 as above)
		$this->db->query("SELECT * FROM `" . DB_PREFIX . "contactus_select` WHERE `order` = '$order' and `option` = '9a9b9c9d9e9f9'");
		if ($this->db->countAffected() != 0) {
		    $this->db->query("DELETE FROM `" . DB_PREFIX . "contactus_select` WHERE `order` = '$order'");
		} else {
		    $this->db->query("SELECT * FROM `" . DB_PREFIX . "contactus_select` WHERE `order` = '$order'");
	        // Found in table
			if ($this->db->countAffected() != 0) {
	            $this->db->query("UPDATE `" . DB_PREFIX . "contactus_select` SET `option` = '$option' WHERE `order` = '$order'");
            } else {
			// Not found in table
		        if ($option != '') {
				    $this->db->query("INSERT INTO `" . DB_PREFIX . "contactus_select` (`order`, `option`) VALUES ('$order', '$option')");
		        }
			}
		}
	}
		
    public function clearOptions() {
	    // Clears 9a9b9c9d9e9f9 if any left behind
		$this->db->query("DELETE FROM `" . DB_PREFIX . "contactus_select` WHERE `option` = '9a9b9c9d9e9f9' or `option` = ''");
    }
	
	public function setUpload($upload, $number, $size) {
    	$this->db->query("UPDATE `" . DB_PREFIX . "contactus_params` SET `upload` = '$upload', `number` = '$number', `size` = '$size'");
	}
	
	public function setFolder($root,$folder) {
		$this->db->query("UPDATE `" . DB_PREFIX . "contactus_params` SET `folder` = '$folder'");
		
		$folder = $root.$folder;
		if (!is_dir($folder)) {
            if (!mkdir($folder, 0755, true)) {
	            die('Failed to create folder...'.$folder);
	        }
        }
    }
	
	public function getParams() {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "contactus_params` LIMIT 1");

		return $query->rows;
    }
	
	public function setEmail($email,$ackn,$copy) {
		$this->db->query("UPDATE `" . DB_PREFIX . "contactus_params` SET `adminemail` = '$email',`acknemail` = '$ackn',`copyemail` = '$copy'");
    }
}

