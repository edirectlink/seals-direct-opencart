<?php
class ModelExtensionrelcatprod extends Controller { 
	public function save($product_id, $data) {
		if($product_id && isset($data['relcatprod_category_str_id']) && isset($data['relcatprod_title'])) { 
			if($data['relcatprod_category_str_id'] && $data['relcatprod_title']) {
				$category_str_id = array_unique($data['relcatprod_category_str_id']);
				$category_str_id = implode(",", $category_str_id);
				
				$this->db->query("DELETE FROM " . DB_PREFIX . "relcatprod WHERE product_id = '" . (int)$product_id . "'");
				
				$this->db->query("INSERT INTO " . DB_PREFIX . "relcatprod SET product_id = '" . (int)$product_id . "', `relcatprod_title` = '" . $this->db->escape(json_encode($data['relcatprod_title'], true)) . "', `relcatprod_category_str_id` = '" . $this->db->escape($category_str_id) . "' ");
			}
		} 
	}
	
	public function getdata($product_id) { 
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "relcatprod WHERE product_id = '" . (int)$product_id . "' limit 1");
		if($query->num_rows){
			return $query->row;
		} 
	} 
}