<?php 
class ModelPaymentinvoicepay extends Model {
  	public function getMethod($address, $total) {
		$this->load->language('payment/invoicepay');
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('invoicepay_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
		
    // check customer group
    $customer_group_id = 0;
    
    if ($this->customer->isLogged()) {
      $this->load->model('account/customer');
      $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
      
      $customer_group_id = $customer_info['customer_group_id'];
    } elseif (isset($this->session->data['guest'])) {
      $customer_group_id = $this->session->data['guest']['customer_group_id'];
    }
      
		if (!in_array($customer_group_id, ((array)$this->config->get('invoicepay_customergroup')))) {
      return array();
    }
    
		if ($this->config->get('invoicepay_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('invoicepay_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}	
		
		$method_data = array();
	
      if ($status) {  
        $method_data = array( 
          'code'       => 'invoicepay',
          'title'      => $this->language->get('text_title'),
          'terms'      => '',
          'sort_order' => $this->config->get('invoicepay_sort_order')
        );
      }
   
    	return $method_data;
  	}
}